<?php

return array(

    'appNameIOS' => array(
        'environment' =>'development',
        'certificate' => storage_path() . '/CertificatesPush.pem',
        'passPhrase'  =>'',
        'service'     =>'apns'
    ),
    'appNameAndroid' => array(
        'environment' =>'development',
        'apiKey'      =>'AIzaSyA_tWXpWxIvMCST2dN_XR7mPMcTxJr81yc',
        'service'     =>'gcm'
    )

);