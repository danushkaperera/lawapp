-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 06, 2016 at 04:23 PM
-- Server version: 5.6.30
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fr46wear_law`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_requests`
--

CREATE TABLE IF NOT EXISTS `account_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('pending','approved','declined') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_requests_email_unique` (`email`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=139 ;

--
-- Dumping data for table `account_requests`
--

INSERT INTO `account_requests` (`id`, `name`, `company`, `email`, `designation`, `phone`, `status`, `created_at`, `updated_at`) VALUES
(40, 'Sudesh Peter', 'Ceylon Tobacco Company PLC', 'sudesh_peter@bat.com', 'Regulatory Affairs Counsel', '0777704978', 'approved', '2016-05-14 08:51:20', '2016-06-27 04:11:07'),
(97, 'Mafaz Ifharm', 'we are designers ', 'mafaz@wearedesigner.net', 'vp business development ', '0777123456', 'approved', '2016-06-06 01:24:53', '2016-06-06 01:28:52'),
(98, 'shani hettiarachchi ', 'wad', 'hettiarachchism123@gmail.com', 'lawyer', '0718400116', 'declined', '2016-06-09 10:15:14', '2016-06-09 10:31:19'),
(99, 'sameera', 'wad', 'sameerafit@gmail.com', 'Se', '(940) 112 1568', 'declined', '2016-06-16 09:45:34', '2016-06-17 04:22:05'),
(100, 'Mafazjjj', 'Hdhd', 'Magaz13@gmajl.com', 'Hhebd', '0773667788', 'declined', '2016-06-16 09:47:15', '2016-06-17 04:22:20'),
(101, 'sameera', 'wad', 'sameerafit2@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 09:50:40', '2016-06-17 04:22:15'),
(102, 'sameera', 'wad', 'sameerafit2d@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 10:00:07', '2016-06-17 04:22:11'),
(103, 'sameera', 'wad', 'sameerafit2dd@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 10:01:55', '2016-06-17 04:22:00'),
(104, 'sameera', 'wad', 'sameerafitsdd@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 10:11:14', '2016-06-17 04:21:56'),
(105, 'sameera', 'wad', 'sameerafitsdds@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 10:11:43', '2016-06-17 04:21:52'),
(106, 'sameera', 'wad', 'ssasa@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 10:18:22', '2016-06-17 04:21:47'),
(107, 'sameera', 'wad', 'rrrrrrr1@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 11:32:14', '2016-06-17 04:21:43'),
(108, 'sameera', 'wad', 'rrrrrrrd1@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 11:32:25', '2016-06-17 04:21:38'),
(109, 'sameera', 'wad', 'rrrrrriird1@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 11:36:35', '2016-06-17 04:21:33'),
(110, 'sameera', 'wad', 'rrrrrrddiird1@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 11:36:51', '2016-06-17 04:21:29'),
(111, 'sameera', 'wad', 'sameerafitee@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 11:37:24', '2016-06-17 04:16:38'),
(112, 'sameera', 'wad', 'rrrrrrr@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 11:37:49', '2016-06-17 04:16:33'),
(113, 'sameera', 'wad', 'rrrrrdsrr@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 11:40:26', '2016-06-17 04:21:25'),
(114, 'sameera', 'wad', 'rrrrerdsrr@gmail.com', 'Se', '0713219718', 'declined', '2016-06-16 11:40:48', '2016-06-17 04:21:21'),
(115, 'sameera', 'wad', 'rrrrerdsrr@gmail.coms', 'Se', '0713219718', 'declined', '2016-06-16 11:41:29', '2016-06-17 04:21:14'),
(116, 'sameera', 'wad', 'rrrrerddsrr@gmail.coms', 'Se', '0713219718', 'declined', '2016-06-16 11:43:11', '2016-06-17 04:21:10'),
(117, 'sameera', 'wad', 'sameerafit33@gmail.com', 'wad', '0713219718', 'declined', '2016-06-16 11:44:15', '2016-06-17 04:21:06'),
(118, 'maheesha', 'wad', 'maheesha_d@hotmail.com', 'intern', '0716159148', 'declined', '2016-06-16 11:53:34', '2016-06-17 04:21:01'),
(119, 'maheesha', 'wad', 'maheesha_d@gmail.com', 'intern', '0716159148', 'declined', '2016-06-16 11:53:46', '2016-06-17 04:20:56'),
(120, 'maheesha', 'wad', 'mashds92@gmail.com', 'intern', '94716159148', 'declined', '2016-06-16 11:57:23', '2016-06-17 04:20:51'),
(121, 'majdn', 'bdjsj', 'mafazaaa@agna.com', 'hahsh', '0773666999', 'declined', '2016-06-16 11:58:50', '2016-06-17 04:20:39'),
(122, 'Brian Kealey ', 'Microsoft Sri Lanka', 'Bkealey@microsoft.com', 'Managing Director ', '0765765500', 'approved', '2016-06-16 14:28:14', '2016-06-17 04:16:12'),
(123, 'maheesha', 'test', 'mashds@live.com', 'test', '94716159148', 'declined', '2016-06-16 14:30:42', '2016-06-17 04:20:47'),
(124, 'maheesha', 'test', 'maheesha_d@yahoo.com', 'test', '94716159148', 'declined', '2016-06-16 14:32:41', '2016-06-17 04:20:43'),
(125, 'maheesha', 'test', 'mashds@mash.com', 'test', '94716159148', 'declined', '2016-06-16 14:51:12', '2016-06-17 04:16:24'),
(126, 'Chandanika', 'Hayleys Advantis Limited ', 'chandanika.jayawickrema@hayleysadvantis.com', 'Head of Group Legal', '0773945713', 'approved', '2016-06-16 17:37:39', '2016-06-17 04:17:47'),
(127, 'Ravi Abeysuriya', 'Candor ', 'ravidila@gmail.com', 'CEO', '0777599799', 'approved', '2016-06-16 17:38:13', '2016-06-17 04:18:35'),
(128, 'charmaine', 'PwC', 'charmaine.tillekeratne@lk.pwc.com', 'Director Tax ', '0770208016', 'approved', '2016-06-17 00:23:25', '2016-06-17 04:19:40'),
(129, 'trinesh fernando ', 'Dialog Axiata PLC ', 'trinesh.fernando@dialog.lk', 'GM Legal and Regulatory ', '0094777333306', 'approved', '2016-06-17 03:12:34', '2016-06-17 04:13:36'),
(130, 'sameera', 'wad', 'sameeraabcv@gmail.com', 'Wad', '0713219718', 'declined', '2016-06-17 05:48:37', '2016-06-18 03:19:49'),
(131, 'Kumar Nadesan ', 'Sri Lanka Press Institute ', 'Md@expressnewspapers.lk', 'Chairman ', '0777270744', 'approved', '2016-06-17 07:12:55', '2016-06-20 05:11:45'),
(132, 'mafaz', 'req to confirm this admin email to mafaz', 'mafaz@test.com', 'test email', '0713219714', 'declined', '2016-06-17 07:59:03', '2016-06-18 03:19:54'),
(133, 'Sanjeewa Anthony', 'Jetwing Hotels Ltd', 'sanjeewa@jetwinghotels.com', 'Executive Director', '0773052264', 'approved', '2016-06-17 09:35:09', '2016-06-20 05:10:41'),
(134, 'sampath', 'was international', 'wgsampath@gmail.com', 'developer', '0716686299', 'declined', '2016-06-20 05:53:32', '2016-06-21 15:51:34'),
(135, 'Shihani', 'Acuity Partners (Pvt) Ltd', 'shihani@acuitystockbrokers.com', 'Manager Legal ', '0777231288', 'approved', '2016-06-21 07:52:40', '2016-06-22 04:47:05'),
(136, 'shazil ismail', 'candor', 'shazil.ismail@gmail.com', 'coo', '0755841352', 'approved', '2016-06-21 15:19:15', '2016-06-22 04:46:13'),
(137, 'Priyanthi Guneratne ', 'Brandix Lanka Limited ', 'priyanthig@brandix.com', 'Group Head Legal ', '0114727272', 'approved', '2016-06-23 16:41:52', '2016-06-27 04:10:25'),
(138, 'Pereira ', 'AMT', 'Ceo@amt.lk', 'Ceo ', '0773444567', 'approved', '2016-06-30 01:13:50', '2016-06-30 04:55:16');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_type_unique` (`type`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `type`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Track your matter - incorporation of a company', 'admin@fjgdirect.com', '$2y$10$HxvIIWkAeLgH6WV1FZPSnOXHZ/GbM/Jte.xVN6vCl6bUe9O135exi', '2016-04-17 09:51:26', '2016-04-17 09:51:26'),
(2, 'Track your matter - Litigation', 'admin2@gmail.com', '$2y$10$mF8IVxebJXD96On9PfDE4O/VQTkoZ70qlqyrwiuAYEgMG3p71Ck8q', '2016-04-17 09:51:27', '2016-04-17 09:51:27'),
(3, 'Track your matter - IP registration', 'admin3@gmail.com', '$2y$10$ezE/d45wXgf09UUem/kK5.Q4OGMK1i.ytyF0rXY2l.H3Z4P7XpB4e', '2016-04-17 09:51:27', '2016-04-17 09:51:27'),
(4, 'General IT administrator', 'admin4@gmail.com', '$2y$10$UXfpHe6UtZHWkqw8vdPSOeTX.RZcvLy4pC1WFmz0b4rSIiB8oOzky', '2016-04-17 09:51:28', '2016-04-17 09:51:28');

-- --------------------------------------------------------

--
-- Table structure for table `appointment_requests`
--

CREATE TABLE IF NOT EXISTS `appointment_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=210 ;

--
-- Dumping data for table `appointment_requests`
--

INSERT INTO `appointment_requests` (`id`, `datetime`, `subject`, `email`, `phone`, `company`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-17 16:54:55', '2016-04-17 16:54:55'),
(2, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-17 16:56:47', '2016-04-17 16:56:47'),
(3, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-17 19:03:13', '2016-04-17 19:03:13'),
(4, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-18 10:31:14', '2016-04-18 10:31:14'),
(5, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-18 10:31:25', '2016-04-18 10:31:25'),
(6, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-22 11:30:46', '2016-04-22 11:30:46'),
(7, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-22 11:59:08', '2016-04-22 11:59:08'),
(8, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-22 12:01:33', '2016-04-22 12:01:33'),
(9, '0000-00-00 00:00:00', 'Optional("sass@gmail.com")', 'sass@gmail.com', '0713219718', 'Optional("sass@gmail.com")', NULL, '2016-04-22 12:05:46', '2016-04-22 12:05:46'),
(10, '0000-00-00 00:00:00', 'Optional("jd@gmail.com")', 'jd@gmail.com', '0771231234', 'Optional("jd@gmail.com")', NULL, '2016-04-22 14:59:41', '2016-04-22 14:59:41'),
(11, '0000-00-00 00:00:00', 'Optional("john@gmail.com")', 'john@gmail.com', '0771231234', 'Optional("john@gmail.com")', NULL, '2016-04-22 15:06:59', '2016-04-22 15:06:59'),
(12, '0000-00-00 00:00:00', 'Optional("Anjali.fernando@gmail.com")', 'Anjali.fernando@gmail.com', '0777356253', 'Optional("Anjali.fernando@gmail.com")', NULL, '2016-04-22 15:09:48', '2016-04-22 15:09:48'),
(13, '0000-00-00 00:00:00', 'Optional("Anjali.fernando@gmail.com")', 'Anjali.fernando@gmail.com', '0777356253', 'Optional("Anjali.fernando@gmail.com")', NULL, '2016-04-22 15:09:58', '2016-04-22 15:09:58'),
(14, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-22 15:11:00', '2016-04-22 15:11:00'),
(15, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-22 15:20:34', '2016-04-22 15:20:34'),
(16, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-22 15:31:54', '2016-04-22 15:31:54'),
(17, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-22 15:32:28', '2016-04-22 15:32:28'),
(18, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-22 15:32:43', '2016-04-22 15:32:43'),
(19, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-22 15:35:09', '2016-04-22 15:35:09'),
(20, '0000-00-00 00:00:00', 'Optional("Anjali.fernando@gmail.com")', 'Anjali.fernando@gmail.com', '0777356253', 'Optional("Anjali.fernando@gmail.com")', NULL, '2016-04-22 15:44:28', '2016-04-22 15:44:28'),
(21, '0000-00-00 00:00:00', 'Optional("Anjali.fernando@gmail.com")', 'Anjali.fernando@gmail.com', '0777356253', 'Optional("Anjali.fernando@gmail.com")', NULL, '2016-04-22 15:49:33', '2016-04-22 15:49:33'),
(22, '2016-02-05 05:50:21', 'dufi', 'maheesha.desilva@yahoo.com', '0716', 'bssns', NULL, '2016-04-22 15:56:03', '2016-04-22 15:56:03'),
(23, '0000-00-00 00:00:00', 'Optional("ggg@mafaz.com")', 'ggg@mafaz.com', '0773539888', 'Optional("ggg@mafaz.com")', NULL, '2016-04-22 16:38:19', '2016-04-22 16:38:19'),
(24, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-22 19:41:27', '2016-04-22 19:41:27'),
(25, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-25 08:33:30', '2016-04-25 08:33:30'),
(26, '0000-00-00 00:00:00', 'Optional("spammers.@you.chj")', 'spammers.@you.chj', '0713219718', 'Optional("spammers.@you.chj")', NULL, '2016-04-25 08:40:14', '2016-04-25 08:40:14'),
(27, '0000-00-00 00:00:00', 'Optional("hettiarachchism@gmail.com")', 'hettiarachchism@gmail.com', '0718400116', 'Optional("hettiarachchism@gmail.com")', NULL, '2016-04-25 11:20:23', '2016-04-25 11:20:23'),
(28, '2016-02-05 05:50:21', 'hghjg', 'k.a.c.dulanjala@gmail.com', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-25 12:03:34', '2016-04-25 12:03:34'),
(29, '2016-02-05 05:50:21', 'hghjg', 'k.a.c.dulanjala@gmail.com', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-25 12:04:55', '2016-04-25 12:04:55'),
(30, '2016-02-05 05:50:21', 'hghjg', 'k.a.c.dulanjala@gmail.com', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-25 12:05:52', '2016-04-25 12:05:52'),
(31, '0000-00-00 00:00:00', 'Optional("hettiarachchism@gmail.com")', 'hettiarachchism@gmail.com', '0718400116', 'Optional("hettiarachchism@gmail.com")', NULL, '2016-04-25 14:18:19', '2016-04-25 14:18:19'),
(32, '0000-00-00 00:00:00', 'Optional("test@gmail.com")', 'test@gmail.com', '0718674982', 'Optional("test@gmail.com")', NULL, '2016-04-25 14:26:41', '2016-04-25 14:26:41'),
(33, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-25 15:07:06', '2016-04-25 15:07:06'),
(34, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-25 15:07:16', '2016-04-25 15:07:16'),
(35, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-25 15:07:38', '2016-04-25 15:07:38'),
(36, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0718400116', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-25 15:08:21', '2016-04-25 15:08:21'),
(37, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-25 15:09:26', '2016-04-25 15:09:26'),
(38, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-26 12:50:33', '2016-04-26 12:50:33'),
(39, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-04-26 13:21:47', '2016-04-26 13:21:47'),
(40, '0000-00-00 00:00:00', 'Optional("sameera@g.com")', 'sameera@g.com', '0713219718', 'Optional("sameera@g.com")', NULL, '2016-04-26 13:25:09', '2016-04-26 13:25:09'),
(41, '0000-00-00 00:00:00', 'shshh', 'maheesha@yahoo.com', '546', 'mash', NULL, '2016-04-26 13:40:09', '2016-04-26 13:40:09'),
(42, '0000-00-00 00:00:00', 'dufu', 'maheesha.desil@yahoo.com', '6259361598', 'mash', NULL, '2016-04-26 13:43:21', '2016-04-26 13:43:21'),
(43, '0000-00-00 00:00:00', 'Optional("hettiarachchism@gmail.com")', 'hettiarachchism@gmail.com', '0718400116', 'Optional("hettiarachchism@gmail.com")', NULL, '2016-04-27 15:33:14', '2016-04-27 15:33:14'),
(44, '0000-00-00 00:00:00', 'duf', 'maheesha.desilva@yahoo.com', '0716159148', 'bsbs', NULL, '2016-04-27 15:51:14', '2016-04-27 15:51:14'),
(45, '0000-00-00 00:00:00', 'Optional("sfghafdf@gaff.ff")', 'sfghafdf@gaff.ff', '0713217442', 'Optional("sfghafdf@gaff.ff")', NULL, '2016-04-27 15:53:44', '2016-04-27 15:53:44'),
(46, '0000-00-00 00:00:00', 'bsns', 'maheesha.desilva@yahoo.com', '0716159148', 'ggh', NULL, '2016-04-27 15:59:25', '2016-04-27 15:59:25'),
(47, '0000-00-00 00:00:00', 'fufof', 'maheesha.desilva@yahoo.com', '0716159148', 'gshsh', NULL, '2016-04-27 16:20:38', '2016-04-27 16:20:38'),
(48, '0000-00-00 00:00:00', 'this is test', 'saneerafit@gmail.com', '0713219718', 'wad', NULL, '2016-04-28 11:51:56', '2016-04-28 11:51:56'),
(49, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-28 11:54:07', '2016-04-28 11:54:07'),
(50, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-28 11:55:34', '2016-04-28 11:55:34'),
(51, '0000-00-00 00:00:00', 'this is test', 'shani@gmail.com', '0718400116', 'wad', NULL, '2016-04-28 11:56:29', '2016-04-28 11:56:29'),
(52, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-28 11:57:32', '2016-04-28 11:57:32'),
(53, '0000-00-00 00:00:00', 'Optional("sameerafit@gmail.com")', 'sameerafit@gmail.com', '0713219718', 'Optional("sameerafit@gmail.com")', NULL, '2016-04-28 12:02:18', '2016-04-28 12:02:18'),
(54, '0000-00-00 00:00:00', 'hshsb', 'maheesha.desilva@yahoo.com', '0716159148', 'yshs', NULL, '2016-04-28 12:36:25', '2016-04-28 12:36:25'),
(55, '0000-00-00 00:00:00', 'test', 'maheesha.desilva@yahoo.com', '0713219718', 'wad', NULL, '2016-04-28 12:43:26', '2016-04-28 12:43:26'),
(56, '0000-00-00 00:00:00', 'yfufuf', 'maheesha@gmail.com', '0716159123', 'jcjdjd', NULL, '2016-04-28 14:32:47', '2016-04-28 14:32:47'),
(57, '0000-00-00 00:00:00', 'Optional("Anjali.fernando@gmail.com")', 'Anjali.fernando@gmail.com', '0777356253', 'Optional("Anjali.fernando@gmail.com")', NULL, '2016-04-28 15:37:26', '2016-04-28 15:37:26'),
(58, '0000-00-00 00:00:00', 'test', 'tiliannie707@gmail.com', '0776121880', 'test', NULL, '2016-04-28 16:45:32', '2016-04-28 16:45:32'),
(59, '0000-00-00 00:00:00', 'masjdbwbsh', 'maheesha.desilva@yahoo.com', '0712345696', 'Connelly Ltd', NULL, '2016-05-01 22:51:49', '2016-05-01 22:51:49'),
(60, '0000-00-00 00:00:00', 'Nnnnnnnnnnnnnncvccvv', 'hhjdjdjd@gmail.com', '0715978789', 'bhjhdfdc', NULL, '2016-05-03 11:04:23', '2016-05-03 11:04:23'),
(61, '0000-00-00 00:00:00', 'incorporation ', 'tiliannie707@gmail.com', '0776121880', 'taf', NULL, '2016-05-06 07:32:42', '2016-05-06 07:32:42'),
(62, '0000-00-00 00:00:00', 'case ', 'shamilsm@gmail.com', '0773385544', 'sss', NULL, '2016-05-06 09:20:42', '2016-05-06 09:20:42'),
(63, '0000-00-00 00:00:00', 'test', 'tiliannie707@gmail.com', '0776121880', 'fjg', NULL, '2016-05-06 10:58:01', '2016-05-06 10:58:01'),
(64, '0000-00-00 00:00:00', 'matter ', 'shamilsm@gmail.com', '0773385544', 'abc limited ', NULL, '2016-05-07 17:57:41', '2016-05-07 17:57:41'),
(65, '0000-00-00 00:00:00', 'sbshjsjjs', 'dsjd@gmail.com', '0715194614', 'cjjwjw', NULL, '2016-05-07 22:09:10', '2016-05-07 22:09:10'),
(66, '0000-00-00 00:00:00', 'winding up ', 'shamilsm@gmail.com', '0773385544', 'abx ', NULL, '2016-05-09 10:57:54', '2016-05-09 10:57:54'),
(67, '0000-00-00 00:00:00', 'to chat', 'ayomi.aluwihare@gmail.com', '0114605115', 'screen line', NULL, '2016-05-09 11:35:33', '2016-05-09 11:35:33'),
(68, '0000-00-00 00:00:00', 'Optional("praveeni.algama@fjgdesaram.com")', 'praveeni.algama@fjgdesaram.com', '0773020919', 'Optional("praveeni.algama@fjgdesaram.com")', NULL, '2016-05-18 11:04:08', '2016-05-18 11:04:08'),
(69, '2016-02-05 05:50:21', 'hghjg', 'mashds@gmail.com', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-05-24 15:48:02', '2016-05-24 15:48:02'),
(70, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-05-25 09:21:04', '2016-05-25 09:21:04'),
(71, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-05-25 09:25:48', '2016-05-25 09:25:48'),
(72, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159123', 'Test', NULL, '2016-05-25 09:57:07', '2016-05-25 09:57:07'),
(73, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716239563', 'We are Designers', NULL, '2016-05-31 09:58:21', '2016-05-31 09:58:21'),
(74, '0000-00-00 00:00:00', 'testing the mobile app', 'mashds@gmail.com', '0716325963', 'We are Designers', NULL, '2016-05-31 10:01:44', '2016-05-31 10:01:44'),
(75, '0000-00-00 00:00:00', 'testing the mobile app', 'mashds@gmail.com', '0716325963', 'We are Designers', NULL, '2016-05-31 10:02:18', '2016-05-31 10:02:18'),
(76, '0000-00-00 00:00:00', 'testing the app ', 'mashds@gmail.com', '0716236148', 'We are Designers', NULL, '2016-05-31 10:20:16', '2016-05-31 10:20:16'),
(77, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716123456', 'We are Designers', NULL, '2016-05-31 10:22:29', '2016-05-31 10:22:29'),
(78, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0715123456', 'We are Designers', NULL, '2016-05-31 10:46:46', '2016-05-31 10:46:46'),
(79, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0713263456', 'We are Designers', NULL, '2016-05-31 10:47:56', '2016-05-31 10:47:56'),
(80, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716159148', 'We are Designers', NULL, '2016-05-31 15:30:23', '2016-05-31 15:30:23'),
(81, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716159148', 'We are Designers', NULL, '2016-05-31 15:31:59', '2016-05-31 15:31:59'),
(82, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716159148', 'We Are Designers', NULL, '2016-05-31 15:34:43', '2016-05-31 15:34:43'),
(83, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716159148', 'We are Designers', NULL, '2016-05-31 15:36:46', '2016-05-31 15:36:46'),
(84, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716159148', 'We are Designers', NULL, '2016-05-31 15:37:04', '2016-05-31 15:37:04'),
(85, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716159148', 'We are Designers', NULL, '2016-06-01 14:13:02', '2016-06-01 14:13:02'),
(86, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716159148', 'We are Designers', NULL, '2016-06-01 14:14:26', '2016-06-01 14:14:26'),
(87, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716159148', 'We are Designers', NULL, '2016-06-01 14:14:41', '2016-06-01 14:14:41'),
(88, '2016-02-05 05:50:21', 'hghjg', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-06-02 15:28:45', '2016-06-02 15:28:45'),
(89, '0000-00-00 00:00:00', 'Test IOS ', '0713219718', 'sameerafit@gmail.com', 'wad ', NULL, '2016-06-02 16:12:12', '2016-06-02 16:12:12'),
(90, '0000-00-00 00:00:00', 'Testing the app', 'mashds@gmail.com', '0716159148', 'We are Designers', NULL, '2016-06-02 16:18:23', '2016-06-02 16:18:23'),
(91, '0000-00-00 00:00:00', 'App testing. ', '0713219718', 'sameerafit@gmail.com', 'wad', NULL, '2016-06-02 16:24:59', '2016-06-02 16:24:59'),
(92, '0000-00-00 00:00:00', 'App testing. ', '0713219718', 'sameerafit@gmail.com', 'wad', NULL, '2016-06-02 16:25:24', '2016-06-02 16:25:24'),
(93, '0000-00-00 00:00:00', 'App testing ', '0713219718', 'sameerafit@gmail.com', 'wad', NULL, '2016-06-02 16:30:30', '2016-06-02 16:30:30'),
(94, '0000-00-00 00:00:00', 'test by wad', 'Sameerafit@gmail.com', '0713219718', 'qqqq', NULL, '2016-06-02 16:34:48', '2016-06-02 16:34:48'),
(95, '0000-00-00 00:00:00', 'App testing', '0713219718', 'sameerafit@gmail.com', 'wad', NULL, '2016-06-02 16:36:12', '2016-06-02 16:36:12'),
(96, '0000-00-00 00:00:00', 'I want to meet you ', 'sameerafit@gmail.com', '0713219718', 'home', NULL, '2016-06-02 16:42:36', '2016-06-02 16:42:36'),
(97, '0000-00-00 00:00:00', 'test ', 'Sameerafit@gmail.com', '0718400116', 'wad', NULL, '2016-06-02 17:11:23', '2016-06-02 17:11:23'),
(98, '0000-00-00 00:00:00', 'Hello', 'sameerafit@gmail.com', '0713219718', 'wad', NULL, '2016-06-02 17:19:26', '2016-06-02 17:19:26'),
(99, '0000-00-00 00:00:00', 'Helli', 'sameerafit@gmail.com', '0713219718', 'qasd', NULL, '2016-06-02 17:20:40', '2016-06-02 17:20:40'),
(100, '0000-00-00 00:00:00', 'Hello\n', 'sameerafit@gmail.com', '0713219718', 'bbbbz', NULL, '2016-06-02 17:24:21', '2016-06-02 17:24:21'),
(101, '0000-00-00 00:00:00', 'Hello\n', 'sameerafit@gmail.com', '0713219718', 'bbbbz', NULL, '2016-06-02 17:25:57', '2016-06-02 17:25:57'),
(102, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716159148', 'We are Designers', NULL, '2016-06-02 17:26:10', '2016-06-02 17:26:10'),
(103, '0000-00-00 00:00:00', 'Hello\n', 'sameerafit@gmail.com', '0713219718', 'bbbbz', NULL, '2016-06-02 17:26:33', '2016-06-02 17:26:33'),
(104, '0000-00-00 00:00:00', 'testing the app', 'mashds@gmail.com', '0716159148', 'We are Designers', NULL, '2016-06-02 17:26:41', '2016-06-02 17:26:41'),
(105, '0000-00-00 00:00:00', 'App testing', 'sameerafit@gmail.com', '0713219718', 'wad', NULL, '2016-06-02 17:27:28', '2016-06-02 17:27:28'),
(106, '0000-00-00 00:00:00', 'App testing', 'sameerafit@gmail.com', '0713219718', 'cvv', NULL, '2016-06-02 17:28:57', '2016-06-02 17:28:57'),
(107, '0000-00-00 00:00:00', 'App testing', 'sameerafit@gmail.com', '0713219718', 'cvv', NULL, '2016-06-02 17:30:15', '2016-06-02 17:30:15'),
(108, '0000-00-00 00:00:00', 'App testing', 'sameerafit@gmail.com', '0713219718', 'cvv', NULL, '2016-06-02 17:30:43', '2016-06-02 17:30:43'),
(109, '0000-00-00 00:00:00', 'two ', 'mafaz@wearedesigner.net', '0712365987', 'test', NULL, '2016-06-03 10:15:06', '2016-06-03 10:15:06'),
(110, '0000-00-00 00:00:00', 'two ', 'mafaz@wearedesigner.net', '0776333666', 'test', NULL, '2016-06-03 10:15:35', '2016-06-03 10:15:35'),
(111, '0000-00-00 00:00:00', 'test', 'Sameerafit@gmail.com', '0713219718', 'wad', NULL, '2016-06-03 10:18:59', '2016-06-03 10:18:59'),
(112, '0000-00-00 00:00:00', 'this is test', 'hettiarachchism@gmail.com', '0718400116', 'wad', NULL, '2016-06-05 23:54:57', '2016-06-05 23:54:57'),
(113, '0000-00-00 00:00:00', 'this is test', 'hettiarachchism@gmail.com', '0718400116', 'wad', NULL, '2016-06-05 23:55:05', '2016-06-05 23:55:05'),
(114, '0000-00-00 00:00:00', 'this is test', 'hettiarachchism@gmail.com', '0718400116', 'wad', NULL, '2016-06-05 23:55:18', '2016-06-05 23:55:18'),
(115, '0000-00-00 00:00:00', 'this is test', 'hettiarachchism@gmail.com', '0718400116', 'wad', NULL, '2016-06-05 23:55:24', '2016-06-05 23:55:24'),
(116, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'wad', NULL, '2016-06-05 23:56:05', '2016-06-05 23:56:05'),
(117, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'wad', NULL, '2016-06-05 23:56:08', '2016-06-05 23:56:08'),
(118, '2016-02-05 05:50:21', 'test', 'mashds@gmail.com', '0716159148', 'wad', NULL, '2016-06-05 23:57:32', '2016-06-05 23:57:32'),
(119, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'wad', NULL, '2016-06-05 23:58:41', '2016-06-05 23:58:41'),
(120, '0000-00-00 00:00:00', 'this is test from wad', 'madushika1@gmail.com', '0718400116', 'wad', NULL, '2016-06-05 23:59:43', '2016-06-05 23:59:43'),
(121, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'wad', NULL, '2016-06-06 00:00:35', '2016-06-06 00:00:35'),
(122, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'wad', NULL, '2016-06-06 00:03:23', '2016-06-06 00:03:23'),
(123, '0000-00-00 00:00:00', 'Test', 'sameerafit@gmail.com', '0713219718', 'wad', NULL, '2016-06-06 00:03:29', '2016-06-06 00:03:29'),
(124, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'wad', NULL, '2016-06-06 00:03:59', '2016-06-06 00:03:59'),
(125, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:09:43', '2016-06-06 00:09:43'),
(126, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:13:47', '2016-06-06 00:13:47'),
(127, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:14:26', '2016-06-06 00:14:26'),
(128, '0000-00-00 00:00:00', 'test', 'maheesha_d@yahoo.com', '0716159148', 'test', NULL, '2016-06-06 00:14:54', '2016-06-06 00:14:54'),
(129, '0000-00-00 00:00:00', 'test', 'maheesha_d@yahoo.com', '0716159148', 'test', NULL, '2016-06-06 00:17:02', '2016-06-06 00:17:02'),
(130, '2016-02-05 05:50:21', 'Test', 'hgkhgkj', 'jhkklughuihiuh', 'jkhjkhkjh', NULL, '2016-06-06 00:17:28', '2016-06-06 00:17:28'),
(131, '0000-00-00 00:00:00', 'test', 'maheesha_d@yahoo.com', '0716159148', 'test', NULL, '2016-06-06 00:17:51', '2016-06-06 00:17:51'),
(132, '0000-00-00 00:00:00', 'Wad test', 'sameerafit@gmail.com', '0713219718', 'wad', NULL, '2016-06-06 00:18:28', '2016-06-06 00:18:28'),
(133, '0000-00-00 00:00:00', 'test', 'maheesha_d@yahoo.com', '0716159148', 'test', NULL, '2016-06-06 00:20:25', '2016-06-06 00:20:25'),
(134, '0000-00-00 00:00:00', 'Test app iOS', 'sameerafit@gmail.com', '0713219718', 'wad', NULL, '2016-06-06 00:21:24', '2016-06-06 00:21:24'),
(135, '0000-00-00 00:00:00', 'test', 'maheesha_d@yahoo.com', '0716159148', 'test', NULL, '2016-06-06 00:24:26', '2016-06-06 00:24:26'),
(136, '0000-00-00 00:00:00', 'test', 'maheesha_d@yahoo.com', '0716159148', 'test', NULL, '2016-06-06 00:24:46', '2016-06-06 00:24:46'),
(137, '0000-00-00 00:00:00', 'test', 'maheesha_d@yahoo.com', '0716159148', 'test', NULL, '2016-06-06 00:26:38', '2016-06-06 00:26:38'),
(138, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:28:28', '2016-06-06 00:28:28'),
(139, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:29:13', '2016-06-06 00:29:13'),
(140, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:29:31', '2016-06-06 00:29:31'),
(141, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:30:51', '2016-06-06 00:30:51'),
(142, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:31:10', '2016-06-06 00:31:10'),
(143, '0000-00-00 00:00:00', 'test', 'maheesha_d@yahoo.com', '0716159148', 'test', NULL, '2016-06-06 00:32:01', '2016-06-06 00:32:01'),
(144, '0000-00-00 00:00:00', 'Testing the app', 'mashds@gmail.com', '0716159148', 'wad', NULL, '2016-06-06 00:34:12', '2016-06-06 00:34:12'),
(145, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'wad', NULL, '2016-06-06 00:35:20', '2016-06-06 00:35:20'),
(146, '0000-00-00 00:00:00', 'test', 'maheesha_d@yahoo.com', '0716159148', 'test', NULL, '2016-06-06 00:38:13', '2016-06-06 00:38:13'),
(147, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:38:16', '2016-06-06 00:38:16'),
(148, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:38:25', '2016-06-06 00:38:25'),
(149, '0000-00-00 00:00:00', 'test', 'maheesha_d@yahoo.com', '0716159148', 'test', NULL, '2016-06-06 00:38:51', '2016-06-06 00:38:51'),
(150, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:39:28', '2016-06-06 00:39:28'),
(151, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:39:30', '2016-06-06 00:39:30'),
(152, '0000-00-00 00:00:00', 'testing the android version', 'mashds@gmail.com', '0716159148', 'wad', NULL, '2016-06-06 00:41:39', '2016-06-06 00:41:39'),
(153, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'wad', NULL, '2016-06-06 00:44:59', '2016-06-06 00:44:59'),
(154, '0000-00-00 00:00:00', 'this is test ', 'hettiarachchism@gmail.com', '0718400116', 'wad', NULL, '2016-06-06 00:46:48', '2016-06-06 00:46:48'),
(155, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:57:18', '2016-06-06 00:57:18'),
(156, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:58:00', '2016-06-06 00:58:00'),
(157, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:58:23', '2016-06-06 00:58:23'),
(158, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:58:53', '2016-06-06 00:58:53'),
(159, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:59:18', '2016-06-06 00:59:18'),
(160, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 00:59:42', '2016-06-06 00:59:42'),
(161, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:00:00', '2016-06-06 01:00:00'),
(162, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:00:16', '2016-06-06 01:00:16'),
(163, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:00:29', '2016-06-06 01:00:29'),
(164, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:05:35', '2016-06-06 01:05:35'),
(165, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:06:19', '2016-06-06 01:06:19'),
(166, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:06:44', '2016-06-06 01:06:44'),
(167, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:06:52', '2016-06-06 01:06:52'),
(168, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:06:59', '2016-06-06 01:06:59'),
(169, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:07:37', '2016-06-06 01:07:37'),
(170, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:08:09', '2016-06-06 01:08:09'),
(171, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:09:13', '2016-06-06 01:09:13'),
(172, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:11:12', '2016-06-06 01:11:12'),
(173, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:11:36', '2016-06-06 01:11:36'),
(174, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:12:18', '2016-06-06 01:12:18'),
(175, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:12:59', '2016-06-06 01:12:59'),
(176, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:13:32', '2016-06-06 01:13:32'),
(177, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:14:39', '2016-06-06 01:14:39'),
(178, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:15:20', '2016-06-06 01:15:20'),
(179, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:15:43', '2016-06-06 01:15:43'),
(180, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:16:01', '2016-06-06 01:16:01'),
(181, '0000-00-00 00:00:00', 'test', 'mashds@gmail.com', '0716159148', 'test', NULL, '2016-06-06 01:16:39', '2016-06-06 01:16:39'),
(182, '0000-00-00 00:00:00', 'I want to make an appoinmet\n\n', 'mafaz@wearedesigner.net', '0777123456', 'wad', NULL, '2016-06-06 01:58:59', '2016-06-06 01:58:59'),
(183, '0000-00-00 00:00:00', 'I want to make an appointment on mentioned date\n', 'mafaz@weareseaigner.net', '0777123456', 'we are designer', NULL, '2016-06-06 02:18:48', '2016-06-06 02:18:48'),
(184, '0000-00-00 00:00:00', 'This is a test msg', 'mafaz@wearedesigners.net', '0773527358', 'wad', NULL, '2016-06-06 03:13:15', '2016-06-06 03:13:15'),
(185, '0000-00-00 00:00:00', 'Dhdhdhdhfhhfh djdhfhfh djdhfhfh. \n\n\n\n\n\n\n\n\n\n', 'anjali.fernando@gmail.com', '0777356253', 'ahshdhf', NULL, '2016-06-06 05:53:20', '2016-06-06 05:53:20'),
(186, '0000-00-00 00:00:00', 'Test Ios ', 'sameerafit@gmail.com', '0713219718', 'Wad ', NULL, '2016-06-08 03:07:59', '2016-06-08 03:07:59'),
(187, '0000-00-00 00:00:00', 'test', 'anjali.fernando@gmail.com', '0777356253', 'Sbshse.):):):', NULL, '2016-06-08 07:19:21', '2016-06-08 07:19:21'),
(188, '0000-00-00 00:00:00', 'this is test ', 'hettiarachchism@gmail.com', '0718400116', 'wad', NULL, '2016-06-09 08:24:55', '2016-06-09 08:24:55'),
(189, '0000-00-00 00:00:00', 'bn', 'anjali.fernando@gmail.com', '0777356253', 'edhdhhedhdhd', NULL, '2016-06-15 06:39:50', '2016-06-15 06:39:50'),
(190, '0000-00-00 00:00:00', 'test', 'anjali.fernando@gmail.com', '0777356253', 'Test', NULL, '2016-06-15 17:54:29', '2016-06-15 17:54:29'),
(191, '0000-00-00 00:00:00', 'This is a test message', 'mafaz@wearedesigners.net', '0112558847', 'test', NULL, '2016-06-15 18:19:05', '2016-06-15 18:19:05'),
(192, '0000-00-00 00:00:00', 'test', 'anjali.fernando@fjgdesaram.com', '0777356253', 'Test', NULL, '2016-06-15 18:27:38', '2016-06-15 18:27:38'),
(193, '0000-00-00 00:00:00', 'Test', 'mafaz13@gmail.com', '0773527358', 'test', NULL, '2016-06-15 18:34:37', '2016-06-15 18:34:37'),
(194, '0000-00-00 00:00:00', 'Please let me know if you receive this message. Mafaz ', 'mafaz@wearedesigners.net', '0776997744', 'test', NULL, '2016-06-15 19:04:08', '2016-06-15 19:04:08'),
(195, '0000-00-00 00:00:00', 'let me know if you get this', 'anjali.fernando@gmail.com', '0777356253', 'Test', NULL, '2016-06-15 19:17:32', '2016-06-15 19:17:32'),
(196, '0000-00-00 00:00:00', 'let me know if you get this ', 'anjali.fernando@fjgdesaram.com', '0777356253', 'Test', NULL, '2016-06-15 19:18:49', '2016-06-15 19:18:49'),
(197, '0000-00-00 00:00:00', 'testing ', 'shamilsm@gmail.com', '0773385544', 'abc (Private) limited ', NULL, '2016-06-16 00:57:44', '2016-06-16 00:57:44'),
(198, '0000-00-00 00:00:00', 'launch test', 'anjali.feenando@gmail.com', '0777356253', 'Test Launch', NULL, '2016-06-16 04:19:13', '2016-06-16 04:19:13'),
(199, '0000-00-00 00:00:00', 'let me know if you get this request ', 'anjali.fernando@gmail.com', '0777356253', 'Test', NULL, '2016-06-16 04:25:40', '2016-06-16 04:25:40'),
(200, '0000-00-00 00:00:00', ' test assignment', 'pavithranavarathne@gmail.com', '0773685029', 'Pavithra', NULL, '2016-06-16 04:35:23', '2016-06-16 04:35:23'),
(201, '0000-00-00 00:00:00', 'This is a test message. Please acknowledge receipt. ', 'mafaz@wearedesigners.net', '0772555888', 'test ', NULL, '2016-06-16 05:22:21', '2016-06-16 05:22:21'),
(202, '0000-00-00 00:00:00', 'IT contract', 'nkdecosta@sltnet.lk', '0777720966', 'Abc', NULL, '2016-06-16 05:57:21', '2016-06-16 05:57:21'),
(203, '0000-00-00 00:00:00', 'test', 'gunasekara.hsr@gmail.com', '0773741097', 'Test ', NULL, '2016-06-16 09:05:11', '2016-06-16 09:05:11'),
(204, '0000-00-00 00:00:00', 'just', 'shehara.gunasekera@fjgdesaram.com', '0777122443', 'shehara gunasekera', NULL, '2016-06-16 09:07:08', '2016-06-16 09:07:08'),
(205, '0000-00-00 00:00:00', 'test 2 ', 'anjali.fernando@gmail.com', '0777356253', 'Test 2', NULL, '2016-06-16 09:07:17', '2016-06-16 09:07:17'),
(206, '0000-00-00 00:00:00', 'testing', 'sankhakaru@icloud.com', '0777720056', 'Sankha And Co', NULL, '2016-06-16 09:47:40', '2016-06-16 09:47:40'),
(207, '0000-00-00 00:00:00', 'thank you for the great evening. and congratulations on 175 years', 'ajith@cal.lk', '0777712347', 'CAL', NULL, '2016-06-17 03:28:51', '2016-06-17 03:28:51'),
(208, '0000-00-00 00:00:00', 'test ', 'shamilsm@gmail.com', '0773385544', 'abc (private) limited ', NULL, '2016-06-17 16:53:18', '2016-06-17 16:53:18'),
(209, '0000-00-00 00:00:00', 'test', 'shehara.varia@gmail.com', '0777342163', 'abc', NULL, '2016-07-02 07:37:05', '2016-07-02 07:37:05');

-- --------------------------------------------------------

--
-- Table structure for table `appointment_request_users`
--

CREATE TABLE IF NOT EXISTS `appointment_request_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `appointment_request_id` int(10) unsigned NOT NULL,
  `lawyer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `appointment_request_users_appointment_request_id_foreign` (`appointment_request_id`),
  KEY `appointment_request_users_lawyer_id_foreign` (`lawyer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=341 ;

--
-- Dumping data for table `appointment_request_users`
--

INSERT INTO `appointment_request_users` (`id`, `appointment_request_id`, `lawyer_id`, `created_at`, `updated_at`) VALUES
(2, 1, 2, '2016-04-17 16:54:55', '2016-04-17 16:54:55'),
(3, 1, 3, '2016-04-17 16:54:55', '2016-04-17 16:54:55'),
(5, 2, 2, '2016-04-17 16:56:47', '2016-04-17 16:56:47'),
(6, 2, 3, '2016-04-17 16:56:47', '2016-04-17 16:56:47'),
(8, 3, 2, '2016-04-17 19:03:13', '2016-04-17 19:03:13'),
(9, 3, 3, '2016-04-17 19:03:13', '2016-04-17 19:03:13'),
(11, 4, 2, '2016-04-18 10:31:15', '2016-04-18 10:31:15'),
(12, 4, 3, '2016-04-18 10:31:15', '2016-04-18 10:31:15'),
(14, 5, 2, '2016-04-18 10:31:25', '2016-04-18 10:31:25'),
(15, 5, 3, '2016-04-18 10:31:25', '2016-04-18 10:31:25'),
(17, 6, 2, '2016-04-22 11:30:46', '2016-04-22 11:30:46'),
(18, 6, 3, '2016-04-22 11:30:46', '2016-04-22 11:30:46'),
(19, 7, 35, '2016-04-22 11:59:08', '2016-04-22 11:59:08'),
(20, 7, 35, '2016-04-22 11:59:08', '2016-04-22 11:59:08'),
(21, 7, 35, '2016-04-22 11:59:08', '2016-04-22 11:59:08'),
(22, 8, 42, '2016-04-22 12:01:33', '2016-04-22 12:01:33'),
(23, 8, 42, '2016-04-22 12:01:33', '2016-04-22 12:01:33'),
(24, 8, 42, '2016-04-22 12:01:33', '2016-04-22 12:01:33'),
(28, 10, 26, '2016-04-22 14:59:41', '2016-04-22 14:59:41'),
(29, 10, 26, '2016-04-22 14:59:41', '2016-04-22 14:59:41'),
(30, 10, 26, '2016-04-22 14:59:41', '2016-04-22 14:59:41'),
(31, 11, 11, '2016-04-22 15:06:59', '2016-04-22 15:06:59'),
(32, 11, 11, '2016-04-22 15:06:59', '2016-04-22 15:06:59'),
(33, 11, 11, '2016-04-22 15:06:59', '2016-04-22 15:06:59'),
(34, 12, 11, '2016-04-22 15:09:48', '2016-04-22 15:09:48'),
(35, 12, 11, '2016-04-22 15:09:48', '2016-04-22 15:09:48'),
(36, 12, 11, '2016-04-22 15:09:48', '2016-04-22 15:09:48'),
(37, 13, 11, '2016-04-22 15:09:58', '2016-04-22 15:09:58'),
(38, 13, 11, '2016-04-22 15:09:58', '2016-04-22 15:09:58'),
(39, 13, 11, '2016-04-22 15:09:58', '2016-04-22 15:09:58'),
(41, 14, 2, '2016-04-22 15:11:00', '2016-04-22 15:11:00'),
(42, 14, 3, '2016-04-22 15:11:00', '2016-04-22 15:11:00'),
(44, 15, 2, '2016-04-22 15:20:34', '2016-04-22 15:20:34'),
(45, 15, 3, '2016-04-22 15:20:34', '2016-04-22 15:20:34'),
(47, 16, 2, '2016-04-22 15:31:54', '2016-04-22 15:31:54'),
(48, 16, 3, '2016-04-22 15:31:54', '2016-04-22 15:31:54'),
(50, 17, 2, '2016-04-22 15:32:28', '2016-04-22 15:32:28'),
(51, 17, 3, '2016-04-22 15:32:28', '2016-04-22 15:32:28'),
(53, 18, 2, '2016-04-22 15:32:43', '2016-04-22 15:32:43'),
(54, 18, 3, '2016-04-22 15:32:43', '2016-04-22 15:32:43'),
(56, 19, 2, '2016-04-22 15:35:09', '2016-04-22 15:35:09'),
(57, 19, 3, '2016-04-22 15:35:09', '2016-04-22 15:35:09'),
(58, 20, 2, '2016-04-22 15:44:28', '2016-04-22 15:44:28'),
(59, 20, 2, '2016-04-22 15:44:28', '2016-04-22 15:44:28'),
(60, 20, 2, '2016-04-22 15:44:28', '2016-04-22 15:44:28'),
(61, 21, 11, '2016-04-22 15:49:33', '2016-04-22 15:49:33'),
(62, 21, 11, '2016-04-22 15:49:33', '2016-04-22 15:49:33'),
(63, 21, 11, '2016-04-22 15:49:33', '2016-04-22 15:49:33'),
(64, 22, 37, '2016-04-22 15:56:03', '2016-04-22 15:56:03'),
(65, 22, 36, '2016-04-22 15:56:03', '2016-04-22 15:56:03'),
(66, 22, 2, '2016-04-22 15:56:03', '2016-04-22 15:56:03'),
(67, 23, 11, '2016-04-22 16:38:19', '2016-04-22 16:38:19'),
(68, 23, 11, '2016-04-22 16:38:19', '2016-04-22 16:38:19'),
(69, 23, 11, '2016-04-22 16:38:19', '2016-04-22 16:38:19'),
(71, 24, 2, '2016-04-22 19:41:27', '2016-04-22 19:41:27'),
(72, 24, 3, '2016-04-22 19:41:27', '2016-04-22 19:41:27'),
(73, 25, 2, '2016-04-25 08:33:31', '2016-04-25 08:33:31'),
(74, 25, 2, '2016-04-25 08:33:31', '2016-04-25 08:33:31'),
(75, 25, 2, '2016-04-25 08:33:31', '2016-04-25 08:33:31'),
(76, 26, 36, '2016-04-25 08:40:14', '2016-04-25 08:40:14'),
(77, 26, 36, '2016-04-25 08:40:14', '2016-04-25 08:40:14'),
(78, 26, 36, '2016-04-25 08:40:14', '2016-04-25 08:40:14'),
(79, 27, 11, '2016-04-25 11:20:23', '2016-04-25 11:20:23'),
(80, 27, 11, '2016-04-25 11:20:23', '2016-04-25 11:20:23'),
(81, 27, 11, '2016-04-25 11:20:23', '2016-04-25 11:20:23'),
(85, 31, 11, '2016-04-25 14:18:19', '2016-04-25 14:18:19'),
(86, 31, 11, '2016-04-25 14:18:19', '2016-04-25 14:18:19'),
(87, 31, 11, '2016-04-25 14:18:19', '2016-04-25 14:18:19'),
(88, 32, 4, '2016-04-25 14:26:41', '2016-04-25 14:26:41'),
(89, 32, 4, '2016-04-25 14:26:41', '2016-04-25 14:26:41'),
(90, 32, 4, '2016-04-25 14:26:41', '2016-04-25 14:26:41'),
(91, 33, 11, '2016-04-25 15:07:07', '2016-04-25 15:07:07'),
(92, 33, 11, '2016-04-25 15:07:07', '2016-04-25 15:07:07'),
(93, 33, 11, '2016-04-25 15:07:07', '2016-04-25 15:07:07'),
(94, 34, 11, '2016-04-25 15:07:16', '2016-04-25 15:07:16'),
(95, 34, 11, '2016-04-25 15:07:16', '2016-04-25 15:07:16'),
(96, 34, 11, '2016-04-25 15:07:16', '2016-04-25 15:07:16'),
(97, 35, 11, '2016-04-25 15:07:38', '2016-04-25 15:07:38'),
(98, 35, 11, '2016-04-25 15:07:38', '2016-04-25 15:07:38'),
(99, 35, 11, '2016-04-25 15:07:38', '2016-04-25 15:07:38'),
(100, 36, 11, '2016-04-25 15:08:21', '2016-04-25 15:08:21'),
(101, 36, 11, '2016-04-25 15:08:21', '2016-04-25 15:08:21'),
(102, 36, 11, '2016-04-25 15:08:21', '2016-04-25 15:08:21'),
(103, 37, 11, '2016-04-25 15:09:26', '2016-04-25 15:09:26'),
(104, 37, 11, '2016-04-25 15:09:26', '2016-04-25 15:09:26'),
(105, 37, 11, '2016-04-25 15:09:26', '2016-04-25 15:09:26'),
(106, 38, 35, '2016-04-26 12:50:33', '2016-04-26 12:50:33'),
(107, 38, 35, '2016-04-26 12:50:33', '2016-04-26 12:50:33'),
(108, 38, 35, '2016-04-26 12:50:33', '2016-04-26 12:50:33'),
(110, 39, 2, '2016-04-26 13:21:48', '2016-04-26 13:21:48'),
(111, 39, 3, '2016-04-26 13:21:48', '2016-04-26 13:21:48'),
(112, 40, 11, '2016-04-26 13:25:09', '2016-04-26 13:25:09'),
(113, 40, 11, '2016-04-26 13:25:09', '2016-04-26 13:25:09'),
(114, 40, 11, '2016-04-26 13:25:09', '2016-04-26 13:25:09'),
(115, 41, 11, '2016-04-26 13:40:09', '2016-04-26 13:40:09'),
(116, 42, 37, '2016-04-26 13:43:21', '2016-04-26 13:43:21'),
(117, 43, 11, '2016-04-27 15:33:14', '2016-04-27 15:33:14'),
(118, 43, 11, '2016-04-27 15:33:14', '2016-04-27 15:33:14'),
(119, 43, 11, '2016-04-27 15:33:14', '2016-04-27 15:33:14'),
(121, 45, 11, '2016-04-27 15:53:44', '2016-04-27 15:53:44'),
(122, 45, 11, '2016-04-27 15:53:44', '2016-04-27 15:53:44'),
(123, 45, 11, '2016-04-27 15:53:44', '2016-04-27 15:53:44'),
(124, 46, 35, '2016-04-27 15:59:25', '2016-04-27 15:59:25'),
(125, 47, 42, '2016-04-27 16:20:38', '2016-04-27 16:20:38'),
(126, 48, 42, '2016-04-28 11:51:56', '2016-04-28 11:51:56'),
(127, 49, 11, '2016-04-28 11:54:07', '2016-04-28 11:54:07'),
(128, 49, 11, '2016-04-28 11:54:07', '2016-04-28 11:54:07'),
(129, 49, 11, '2016-04-28 11:54:07', '2016-04-28 11:54:07'),
(130, 50, 11, '2016-04-28 11:55:34', '2016-04-28 11:55:34'),
(131, 50, 11, '2016-04-28 11:55:34', '2016-04-28 11:55:34'),
(132, 50, 11, '2016-04-28 11:55:34', '2016-04-28 11:55:34'),
(133, 51, 35, '2016-04-28 11:56:29', '2016-04-28 11:56:29'),
(134, 52, 11, '2016-04-28 11:57:32', '2016-04-28 11:57:32'),
(135, 52, 11, '2016-04-28 11:57:32', '2016-04-28 11:57:32'),
(136, 52, 11, '2016-04-28 11:57:32', '2016-04-28 11:57:32'),
(137, 53, 11, '2016-04-28 12:02:18', '2016-04-28 12:02:18'),
(138, 53, 11, '2016-04-28 12:02:18', '2016-04-28 12:02:18'),
(139, 53, 11, '2016-04-28 12:02:18', '2016-04-28 12:02:18'),
(140, 54, 35, '2016-04-28 12:36:25', '2016-04-28 12:36:25'),
(141, 55, 11, '2016-04-28 12:43:26', '2016-04-28 12:43:26'),
(142, 56, 35, '2016-04-28 14:32:47', '2016-04-28 14:32:47'),
(143, 57, 2, '2016-04-28 15:37:26', '2016-04-28 15:37:26'),
(144, 57, 2, '2016-04-28 15:37:26', '2016-04-28 15:37:26'),
(145, 57, 2, '2016-04-28 15:37:26', '2016-04-28 15:37:26'),
(146, 58, 14, '2016-04-28 16:45:32', '2016-04-28 16:45:32'),
(147, 59, 14, '2016-05-01 22:51:49', '2016-05-01 22:51:49'),
(148, 60, 35, '2016-05-03 11:04:23', '2016-05-03 11:04:23'),
(149, 61, 14, '2016-05-06 07:32:42', '2016-05-06 07:32:42'),
(150, 62, 29, '2016-05-06 09:20:42', '2016-05-06 09:20:42'),
(151, 63, 14, '2016-05-06 10:58:01', '2016-05-06 10:58:01'),
(152, 64, 14, '2016-05-07 17:57:41', '2016-05-07 17:57:41'),
(153, 65, 42, '2016-05-07 22:09:10', '2016-05-07 22:09:10'),
(154, 66, 29, '2016-05-09 10:57:54', '2016-05-09 10:57:54'),
(155, 67, 23, '2016-05-09 11:35:33', '2016-05-09 11:35:33'),
(156, 68, 4, '2016-05-18 11:04:08', '2016-05-18 11:04:08'),
(157, 68, 4, '2016-05-18 11:04:08', '2016-05-18 11:04:08'),
(158, 68, 4, '2016-05-18 11:04:08', '2016-05-18 11:04:08'),
(160, 69, 2, '2016-05-24 15:48:02', '2016-05-24 15:48:02'),
(161, 69, 3, '2016-05-24 15:48:02', '2016-05-24 15:48:02'),
(162, 70, 11, '2016-05-25 09:21:04', '2016-05-25 09:21:04'),
(163, 70, 36, '2016-05-25 09:21:04', '2016-05-25 09:21:04'),
(164, 71, 42, '2016-05-25 09:25:48', '2016-05-25 09:25:48'),
(165, 71, 5, '2016-05-25 09:25:48', '2016-05-25 09:25:48'),
(166, 72, 29, '2016-05-25 09:57:07', '2016-05-25 09:57:07'),
(168, 74, 42, '2016-05-31 10:01:44', '2016-05-31 10:01:44'),
(169, 75, 37, '2016-05-31 10:02:18', '2016-05-31 10:02:18'),
(170, 76, 35, '2016-05-31 10:20:16', '2016-05-31 10:20:16'),
(171, 76, 37, '2016-05-31 10:20:16', '2016-05-31 10:20:16'),
(172, 77, 42, '2016-05-31 10:22:29', '2016-05-31 10:22:29'),
(173, 77, 4, '2016-05-31 10:22:29', '2016-05-31 10:22:29'),
(174, 78, 35, '2016-05-31 10:46:46', '2016-05-31 10:46:46'),
(175, 78, 4, '2016-05-31 10:46:46', '2016-05-31 10:46:46'),
(176, 79, 35, '2016-05-31 10:47:56', '2016-05-31 10:47:56'),
(177, 80, 35, '2016-05-31 15:30:23', '2016-05-31 15:30:23'),
(178, 81, 36, '2016-05-31 15:31:59', '2016-05-31 15:31:59'),
(179, 81, 11, '2016-05-31 15:31:59', '2016-05-31 15:31:59'),
(180, 82, 35, '2016-05-31 15:34:43', '2016-05-31 15:34:43'),
(181, 83, 11, '2016-05-31 15:36:46', '2016-05-31 15:36:46'),
(182, 84, 11, '2016-05-31 15:37:04', '2016-05-31 15:37:04'),
(183, 84, 45, '2016-05-31 15:37:04', '2016-05-31 15:37:04'),
(184, 85, 35, '2016-06-01 14:13:02', '2016-06-01 14:13:02'),
(185, 86, 42, '2016-06-01 14:14:26', '2016-06-01 14:14:26'),
(186, 87, 42, '2016-06-01 14:14:41', '2016-06-01 14:14:41'),
(187, 87, 45, '2016-06-01 14:14:41', '2016-06-01 14:14:41'),
(189, 89, 11, '2016-06-02 16:12:12', '2016-06-02 16:12:12'),
(190, 89, 29, '2016-06-02 16:12:12', '2016-06-02 16:12:12'),
(191, 90, 35, '2016-06-02 16:18:23', '2016-06-02 16:18:23'),
(192, 90, 37, '2016-06-02 16:18:23', '2016-06-02 16:18:23'),
(193, 91, 11, '2016-06-02 16:24:59', '2016-06-02 16:24:59'),
(194, 91, 29, '2016-06-02 16:24:59', '2016-06-02 16:24:59'),
(195, 92, 11, '2016-06-02 16:25:24', '2016-06-02 16:25:24'),
(196, 92, 29, '2016-06-02 16:25:24', '2016-06-02 16:25:24'),
(197, 93, 11, '2016-06-02 16:30:30', '2016-06-02 16:30:30'),
(199, 95, 11, '2016-06-02 16:36:12', '2016-06-02 16:36:12'),
(203, 98, 11, '2016-06-02 17:19:26', '2016-06-02 17:19:26'),
(211, 105, 29, '2016-06-02 17:27:28', '2016-06-02 17:27:28'),
(225, 119, 42, '2016-06-05 23:58:41', '2016-06-05 23:58:41'),
(227, 121, 36, '2016-06-06 00:00:35', '2016-06-06 00:00:35'),
(239, 129, 3, '2016-06-06 00:17:02', '2016-06-06 00:17:02'),
(240, 129, 6, '2016-06-06 00:17:02', '2016-06-06 00:17:02'),
(242, 130, 2, '2016-06-06 00:17:28', '2016-06-06 00:17:28'),
(243, 130, 3, '2016-06-06 00:17:28', '2016-06-06 00:17:28'),
(256, 138, 42, '2016-06-06 00:28:28', '2016-06-06 00:28:28'),
(257, 139, 3, '2016-06-06 00:29:13', '2016-06-06 00:29:13'),
(267, 147, 3, '2016-06-06 00:38:16', '2016-06-06 00:38:16'),
(304, 182, 52, '2016-06-06 01:58:59', '2016-06-06 01:58:59'),
(305, 183, 52, '2016-06-06 02:18:48', '2016-06-06 02:18:48'),
(306, 184, 52, '2016-06-06 03:13:15', '2016-06-06 03:13:15'),
(307, 185, 2, '2016-06-06 05:53:20', '2016-06-06 05:53:20'),
(308, 185, 52, '2016-06-06 05:53:20', '2016-06-06 05:53:20'),
(309, 186, 11, '2016-06-08 03:07:59', '2016-06-08 03:07:59'),
(310, 187, 2, '2016-06-08 07:19:21', '2016-06-08 07:19:21'),
(311, 187, 11, '2016-06-08 07:19:21', '2016-06-08 07:19:21'),
(312, 188, 35, '2016-06-09 08:24:55', '2016-06-09 08:24:55'),
(313, 189, 11, '2016-06-15 06:39:51', '2016-06-15 06:39:51'),
(314, 189, 29, '2016-06-15 06:39:51', '2016-06-15 06:39:51'),
(315, 190, 11, '2016-06-15 17:54:29', '2016-06-15 17:54:29'),
(316, 190, 2, '2016-06-15 17:54:29', '2016-06-15 17:54:29'),
(317, 191, 11, '2016-06-15 18:19:05', '2016-06-15 18:19:05'),
(318, 192, 11, '2016-06-15 18:27:38', '2016-06-15 18:27:38'),
(319, 193, 29, '2016-06-15 18:34:37', '2016-06-15 18:34:37'),
(320, 194, 29, '2016-06-15 19:04:08', '2016-06-15 19:04:08'),
(321, 195, 29, '2016-06-15 19:17:32', '2016-06-15 19:17:32'),
(322, 196, 29, '2016-06-15 19:18:49', '2016-06-15 19:18:49'),
(323, 197, 11, '2016-06-16 00:57:44', '2016-06-16 00:57:44'),
(324, 198, 11, '2016-06-16 04:19:13', '2016-06-16 04:19:13'),
(325, 199, 23, '2016-06-16 04:25:40', '2016-06-16 04:25:40'),
(326, 199, 30, '2016-06-16 04:25:40', '2016-06-16 04:25:40'),
(327, 200, 11, '2016-06-16 04:35:23', '2016-06-16 04:35:23'),
(328, 200, 55, '2016-06-16 04:35:23', '2016-06-16 04:35:23'),
(329, 200, 4, '2016-06-16 04:35:23', '2016-06-16 04:35:23'),
(330, 201, 11, '2016-06-16 05:22:21', '2016-06-16 05:22:21'),
(331, 202, 23, '2016-06-16 05:57:21', '2016-06-16 05:57:21'),
(332, 203, 7, '2016-06-16 09:05:11', '2016-06-16 09:05:11'),
(333, 204, 55, '2016-06-16 09:07:08', '2016-06-16 09:07:08'),
(334, 205, 30, '2016-06-16 09:07:17', '2016-06-16 09:07:17'),
(335, 206, 26, '2016-06-16 09:47:40', '2016-06-16 09:47:40'),
(336, 207, 2, '2016-06-17 03:28:51', '2016-06-17 03:28:51'),
(337, 207, 36, '2016-06-17 03:28:51', '2016-06-17 03:28:51'),
(338, 207, 43, '2016-06-17 03:28:51', '2016-06-17 03:28:51'),
(339, 208, 29, '2016-06-17 16:53:18', '2016-06-17 16:53:18'),
(340, 209, 43, '2016-07-02 07:37:05', '2016-07-02 07:37:05');

-- --------------------------------------------------------

--
-- Table structure for table `company_setups`
--

CREATE TABLE IF NOT EXISTS `company_setups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_setup_directors`
--

CREATE TABLE IF NOT EXISTS `company_setup_directors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_setup_id` int(10) unsigned NOT NULL,
  `nic_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nic_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `residentail_address_local` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `residentail_address_foregin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `current_occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_setup_directors_company_setup_id_foreign` (`company_setup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_setup_shareholders`
--

CREATE TABLE IF NOT EXISTS `company_setup_shareholders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_setup_id` int(10) unsigned NOT NULL,
  `nic_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nic_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `residentail_address_local` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `residentail_address_foregin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `current_occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_setup_shareholders_company_setup_id_foreign` (`company_setup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `corporate_users`
--

CREATE TABLE IF NOT EXISTS `corporate_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

--
-- Dumping data for table `corporate_users`
--

INSERT INTO `corporate_users` (`id`, `name`, `company`, `department`, `designation`, `phone`, `status`, `created_at`, `updated_at`) VALUES
(38, 'Mafaz Ifharm', 'WAD', 'IT', 'vp business development ', '0777123456', 'active', '2016-06-06 01:28:52', '2016-06-14 03:28:07'),
(39, 'FJGDirect', 'Acme Company (Private) Limited', 'Management', 'Manager', '0112123456', 'deactive', '2016-06-15 11:02:44', '2016-06-17 03:38:04'),
(40, 'Shani Hettiarachchi', 'wad', 'IT', 'Lawyer', '+94718400116', 'active', '2016-06-15 11:45:30', '2016-06-15 11:45:30'),
(41, 'Trinesh Fernando ', 'Dialog Axiata PLC ', 'Legal and Regulatory ', 'GM Legal and Regulatory ', '0777333306', 'active', '2016-06-17 04:13:35', '2016-06-17 04:13:35'),
(42, 'Brian Kealey ', 'Microsoft Sri Lanka', 'Management', 'Managing Director ', '0765765500', 'active', '2016-06-17 04:16:12', '2016-06-17 04:16:12'),
(43, 'Chandanika', 'Hayleys Advantis Limited ', 'Legal', 'Head of Group Legal', '0773945713', 'active', '2016-06-17 04:17:47', '2016-06-17 04:17:47'),
(44, 'Ravi Abeysuriya', 'Candor ', 'Management', 'CEO', '0777599799', 'active', '2016-06-17 04:18:35', '2016-06-17 04:18:35'),
(45, 'Charmaine Tillekeratne', 'PwC', 'Tax', 'Director Tax ', '0770208016', 'active', '2016-06-17 04:19:40', '2016-06-17 04:19:40'),
(46, 'Sanjeewa Anthony', 'Jetwing Hotels Ltd', 'Management', 'Executive Director', '0773052264', 'active', '2016-06-20 05:10:41', '2016-06-20 05:10:41'),
(47, 'Kumar Nadesan ', 'Sri Lanka Press Institute ', 'Management', 'Chairman ', '0777270744', 'active', '2016-06-20 05:11:45', '2016-06-20 05:11:45'),
(48, 'Shazil Ismail', 'Candor', 'Management', 'COO', '755841352', 'active', '2016-06-22 04:46:13', '2016-06-22 04:46:13'),
(49, 'Shihani', 'Acuity Partners (Pvt) Ltd', 'Legal', 'Manager Legal ', '0777231288', 'active', '2016-06-22 04:47:05', '2016-06-22 04:47:05'),
(50, 'Priyanthi Guneratne ', 'Brandix Lanka Limited ', 'Legal', 'Group Head Legal ', '0114727272', 'active', '2016-06-27 04:10:24', '2016-06-27 05:14:39');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE IF NOT EXISTS `designations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client` tinyint(1) NOT NULL,
  `lawyer` tinyint(1) NOT NULL,
  `firm` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `firm_users`
--

CREATE TABLE IF NOT EXISTS `firm_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lawyer_id` int(100) NOT NULL,
  `division_id` int(10) unsigned NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FJ&G de Saram ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=165 ;

--
-- Dumping data for table `firm_users`
--

INSERT INTO `firm_users` (`id`, `name`, `lawyer_id`, `division_id`, `designation`, `phone`, `status`, `created_at`, `updated_at`, `company`) VALUES
(115, 'Ayomi Aluwihare-Gunawardena', 2, 4, 'Partner  ', '0114605115', 'active', '2016-06-15 07:54:56', '2016-06-16 06:51:33', 'FJ&G de Saram '),
(116, 'Praveeni Algama', 3, 4, 'Senior Associate', '0114605171', 'active', '2016-06-15 07:54:56', '2016-06-16 06:29:53', 'FJ&G de Saram '),
(117, 'Buwaneka Basnayake', 4, 4, 'Senior Legal Counsel', '0114605181', 'active', '2016-06-15 07:54:57', '2016-06-16 06:29:45', 'FJ&G de Saram '),
(118, 'Chamari De Silva', 5, 5, 'Partner  ', '0114605150', 'active', '2016-06-15 07:54:57', '2016-06-16 06:52:02', 'FJ&G de Saram '),
(119, 'Nimesha De Silva', 6, 2, 'Litigator', '0114605160', 'active', '2016-06-15 07:54:58', '2016-06-16 06:29:30', 'FJ&G de Saram '),
(120, 'Raneesha De Alwis', 7, 2, 'Senior Litigator', '0114605183', 'active', '2016-06-15 07:54:58', '2016-06-16 06:29:14', 'FJ&G de Saram '),
(121, 'Harshana De Alwis', 8, 2, 'Litigator', '0114605162', 'active', '2016-06-15 07:54:59', '2016-06-16 06:29:08', 'FJ&G de Saram '),
(122, 'Kolitha Dissanayake', 9, 4, 'Senior Associate', '0114605175', 'active', '2016-06-15 07:54:59', '2016-06-16 06:29:02', 'FJ&G de Saram '),
(123, 'Manjula Ellepola', 10, 5, 'Partner  ', '0114605147', 'active', '2016-06-15 07:54:59', '2016-06-16 06:50:44', 'FJ&G de Saram '),
(124, 'Anjali Fernando', 11, 4, 'Partner  ', '0114605123', 'active', '2016-06-15 07:55:00', '2016-06-16 06:50:21', 'FJ&G de Saram '),
(125, 'Rozali Fernando', 12, 2, 'Litigator', '0114605132', 'active', '2016-06-15 07:55:00', '2016-06-16 06:28:42', 'FJ&G de Saram '),
(126, 'Tania Fernando', 13, 3, 'Associate', '0114605186', 'active', '2016-06-15 07:55:01', '2016-06-16 06:28:37', 'FJ&G de Saram '),
(127, 'Tilani Ford', 14, 4, 'Senior Associate', '0114605134', 'active', '2016-06-15 07:55:02', '2016-06-16 06:28:31', 'FJ&G de Saram '),
(128, 'Shanaka Gunasekara', 15, 2, 'Senior Litigator', '0114605112', 'active', '2016-06-15 07:55:02', '2016-06-16 06:28:24', 'FJ&G de Saram '),
(129, 'Natalie Gnaniah', 17, 1, 'Executive-Legal', '0114605119', 'active', '2016-06-15 07:55:03', '2016-06-16 06:28:17', 'FJ&G de Saram '),
(130, 'Christina Hettiarachchi', 18, 2, 'Legal Assistant', '0114605131', 'active', '2016-06-15 07:55:03', '2016-06-16 06:28:11', 'FJ&G de Saram '),
(131, 'Ashiq Hassim', 19, 2, 'Litigator', '0114605157', 'active', '2016-06-15 07:55:04', '2016-06-16 06:28:04', 'FJ&G de Saram '),
(132, 'Kasuni Jayaweera', 20, 2, 'Litigator', '0114605144', 'active', '2016-06-15 07:55:04', '2016-06-16 06:27:58', 'FJ&G de Saram '),
(133, 'Suganthie Wijayasuriya-Kadirgamar', 21, 3, 'Senior Partner', '0114605141', 'active', '2016-06-15 07:55:05', '2016-06-16 06:27:48', 'FJ&G de Saram '),
(134, 'U.L. Kadurugamuwa', 22, 4, 'Consultant', '0114605114', 'active', '2016-06-15 07:55:05', '2016-06-16 06:27:39', 'FJ&G de Saram '),
(135, 'Roshani Kobbekaduwa', 23, 4, 'Partner ', '0114605122', 'active', '2016-06-15 07:55:06', '2016-06-16 06:46:53', 'FJ&G de Saram '),
(136, 'Jayathri Kulatilaka', 24, 3, 'Partner  ', '0114605152', 'active', '2016-06-15 07:55:06', '2016-06-16 06:49:15', 'FJ&G de Saram '),
(137, 'Roshika Kulatunga', 25, 1, 'Manager', '0114605127', 'active', '2016-06-15 07:55:07', '2016-06-16 06:27:04', 'FJ&G de Saram '),
(138, 'Sankha Karunaratne', 26, 4, 'Senior Associate', '0114605121', 'active', '2016-06-15 07:55:07', '2016-06-16 06:26:59', 'FJ&G de Saram '),
(139, 'Charana Kanankegamage', 27, 4, 'Associate', '0114605156', 'active', '2016-06-15 07:55:08', '2016-06-16 06:26:52', 'FJ&G de Saram '),
(140, 'Himali Mudadeniya', 28, 4, 'Partner  ', '0114605120', 'active', '2016-06-15 07:55:08', '2016-06-16 06:26:47', 'FJ&G de Saram '),
(141, 'Shamil Mueen', 29, 4, 'Associate', '0114605136', 'active', '2016-06-15 07:55:09', '2016-06-16 06:26:41', 'FJ&G de Saram '),
(142, 'Pavithra Navarathne', 30, 4, 'Senior Associate', '0114605179', 'active', '2016-06-15 07:55:09', '2016-06-16 06:26:35', 'FJ&G de Saram '),
(143, 'Shehani Nayagam', 31, 1, 'Executive-Legal', '0114605170', 'active', '2016-06-15 07:55:10', '2016-06-16 06:26:29', 'FJ&G de Saram '),
(144, 'Shevanthi Seresinhe-Perera', 32, 4, 'Senior Associate', '0114605177', 'active', '2016-06-15 07:55:10', '2016-06-16 06:26:14', 'FJ&G de Saram '),
(145, 'Udeesha Perera', 33, 1, 'Executive-Legal', '0114605192', 'active', '2016-06-15 07:55:11', '2016-06-16 06:26:08', 'FJ&G de Saram '),
(146, 'Amali Peiris', 35, 1, 'Executive-Legal', '0114605193', 'active', '2016-06-15 07:55:11', '2016-06-16 06:26:02', 'FJ&G de Saram '),
(147, 'Avindra Rodrigo', 36, 2, 'Counsel', '0114605137', 'active', '2016-06-15 07:55:12', '2016-06-16 06:25:55', 'FJ&G de Saram '),
(148, 'Aruna De Silva', 37, 2, 'Litigator', '0114605151', 'active', '2016-06-15 07:55:12', '2016-06-16 06:25:49', 'FJ&G de Saram '),
(149, 'Medhya Samarasinghe', 38, 2, 'Litigator', '0114605133', 'active', '2016-06-15 07:55:13', '2016-06-16 06:25:39', 'FJ&G de Saram '),
(150, 'Upekha Senaratne', 39, 3, 'Partner  ', '0114605154', 'active', '2016-06-15 07:55:13', '2016-06-16 06:48:54', 'FJ&G de Saram '),
(151, 'Patali Thrimanne', 40, 2, 'Litigator', '0114605100', 'active', '2016-06-15 07:55:14', '2016-06-16 06:25:02', 'FJ&G de Saram '),
(152, 'Dinukshi Thalgahagoda', 41, 1, 'Partner  ', '0114605180', 'active', '2016-06-15 07:55:14', '2016-06-16 06:48:36', 'FJ&G de Saram '),
(153, 'Ananda Tikirirathne', 42, 2, 'Senior Litigator', '0114605159', 'active', '2016-06-15 07:55:15', '2016-06-16 06:24:49', 'FJ&G de Saram '),
(154, 'Shehara Varia', 43, 4, 'Partner  ', '0114605128', 'active', '2016-06-15 07:55:15', '2016-06-16 06:47:47', 'FJ&G de Saram '),
(155, 'Rosy Vanlangenberg', 44, 3, 'Associate', '0114605187', 'active', '2016-06-15 07:55:16', '2016-06-16 06:24:37', 'FJ&G de Saram '),
(156, 'Anuradha Wijesooriya', 45, 2, 'Litigator', '0114605139', 'active', '2016-06-15 07:55:16', '2016-06-16 06:24:32', 'FJ&G de Saram '),
(157, 'Ramesh Perera', 52, 4, 'Legal Assistant', '0114605158', 'active', '2016-06-15 07:55:17', '2016-06-16 06:24:25', 'FJ&G de Saram '),
(158, 'Deepthi Abeysena', 53, 5, 'Director', '0114605132', 'active', '2016-06-15 07:55:17', '2016-06-16 06:24:17', 'FJ&G de Saram '),
(159, 'Shehara Gunasekara', 54, 5, 'Associate', '0114605151', 'active', '2016-06-15 07:55:17', '2016-06-16 06:24:05', 'FJ&G de Saram '),
(160, 'Anushika Abeywickrama', 55, 5, 'Associate', '0114605146', 'active', '2016-06-15 07:55:18', '2016-06-16 06:23:45', 'FJ&G de Saram '),
(161, 'Oshani Wijewardena', 56, 5, 'Associate', '0114605195', 'active', '2016-06-15 07:55:18', '2016-06-16 06:23:27', 'FJ&G de Saram '),
(162, 'Harshini Serasinghe', 57, 1, 'Executive Legal', '0777373980', 'active', '2016-06-15 07:55:19', '2016-06-16 06:22:56', 'FJ&G de Saram '),
(163, 'Thilini Samarawickrama', 58, 1, 'Legal Assistant', '0114605134', 'active', '2016-06-15 07:55:19', '2016-06-16 09:32:25', 'FJ&G de Saram '),
(164, 'Tharushi Perera', 59, 3, 'Legal Assistant', '+94114605100', 'active', '2016-06-15 07:55:20', '2016-06-16 06:36:05', 'FJ&G de Saram ');

-- --------------------------------------------------------

--
-- Table structure for table `getupdates`
--

CREATE TABLE IF NOT EXISTS `getupdates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `legal_alert` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `quick_reference` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `track_matter` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `getupdates`
--

INSERT INTO `getupdates` (`id`, `user_id`, `legal_alert`, `quick_reference`, `track_matter`) VALUES
(6, 65, '2016-06-20 05:50:37', '2016-06-20 05:50:37', '2016-06-20 05:50:37'),
(7, 405, '2016-06-30 00:31:04', '2016-06-30 00:30:51', '2016-07-03 13:19:02'),
(8, 365, '2016-06-25 18:30:00', '2016-06-25 18:30:00', '2016-06-28 18:30:00'),
(9, 396, '2016-06-29 18:22:57', '2016-06-26 08:22:14', '2016-06-27 17:51:34'),
(10, 351, '2016-06-25 10:08:05', '2016-06-28 04:56:54', '2016-06-29 18:22:47'),
(11, 361, '2016-06-29 18:30:44', '2016-06-29 18:31:16', '2016-06-29 18:31:38'),
(12, 376, '2016-06-28 22:00:00', '2016-06-29 20:57:50', '2016-06-29 20:58:10'),
(13, 391, '2016-06-19 06:45:43', '2016-06-19 06:43:58', '2016-06-19 06:43:58'),
(14, 377, '2016-06-22 18:30:00', '2016-06-22 18:30:00', '2016-06-29 18:30:00'),
(15, 358, '2016-06-30 02:22:47', '2016-07-01 04:52:52', '2016-07-01 04:53:50'),
(16, 386, '2016-06-29 18:30:00', '2016-06-18 18:30:00', '2016-07-05 18:30:00'),
(17, 395, '2016-06-22 08:43:13', '2016-06-20 07:10:26', '2016-06-22 08:43:28'),
(18, 356, '2016-06-22 09:03:05', '2016-06-21 02:58:02', '2016-06-22 11:15:49'),
(19, 406, '2016-06-30 15:53:23', '2016-07-02 10:30:47', '2016-07-02 10:30:53'),
(20, 388, '2016-07-02 07:34:17', '2016-06-21 06:04:03', '2016-06-23 15:34:16'),
(21, 362, '2016-06-23 10:06:43', '2016-06-20 06:56:35', '2016-07-02 11:41:56'),
(22, 409, '2016-07-03 10:07:52', '2016-06-28 02:46:37', '2016-06-27 10:32:48'),
(23, 372, '2016-06-22 10:31:36', '2016-07-04 09:02:10', '2016-07-04 09:02:13'),
(24, 371, '2016-07-04 10:12:19', '2016-07-05 06:46:47', '2016-07-05 06:47:03'),
(25, 393, '2016-07-04 15:07:28', '2016-06-21 12:28:39', '2016-06-21 12:28:20'),
(26, 394, '2016-06-25 14:44:14', '2016-06-17 14:05:24', '2016-06-25 14:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `lawyers`
--

CREATE TABLE IF NOT EXISTS `lawyers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `division_id` int(10) unsigned NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `direct_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `head` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lawyers_division_id_foreign` (`division_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=60 ;

--
-- Dumping data for table `lawyers`
--

INSERT INTO `lawyers` (`id`, `full_name`, `designation`, `division_id`, `email`, `direct_phone`, `image`, `created_at`, `updated_at`, `head`) VALUES
(2, 'Ayomi Aluwihare-Gunawardena', 'Partner  ', 4, 'ayomi.aluwihare@fjgdesaram.com', '0114605115', 'images/lawyers/49459.jpg', '2016-04-17 09:51:09', '2016-06-29 19:26:41', 1),
(3, 'Praveeni Algama', 'Senior Associate', 4, 'praveeni.algama@fjgdesaram.com', '0114605171', 'images/lawyers/46809.jpg', '2016-04-17 09:51:09', '2016-06-29 19:26:21', 0),
(4, 'Buwaneka Basnayake', 'Senior Legal Counsel', 4, 'buwaneka.basnayake@fjgdesaram.com', '0114605181', 'images/lawyers/57988.jpg', '2016-04-17 09:51:09', '2016-06-29 19:25:57', 0),
(5, 'Chamari De Silva', 'Partner  ', 5, 'chamari.desilva@fjgdesaram.com', '0114605150', 'images/lawyers/45825.jpg', '2016-04-17 09:51:09', '2016-06-29 19:25:33', 0),
(6, 'Nimesha De Silva', 'Litigator', 2, 'nimesha.desilva@fjgdesaram.com', '0114605160', 'images/lawyers/89700.jpg', '2016-04-17 09:51:09', '2016-06-29 19:24:03', 0),
(7, 'Raneesha De Alwis', 'Senior Litigator', 2, 'raneesha.dealwis@fjgdesaram.com', '0114605183', 'images/lawyers/78057.jpg', '2016-04-17 09:51:09', '2016-06-29 19:23:40', 0),
(8, 'Harshana De Alwis', 'Litigator', 2, 'harshana.dealwis@fjgdesaram.com', '0114605162', 'images/lawyers/31998.jpg', '2016-04-17 09:51:09', '2016-06-29 19:23:23', 0),
(9, 'Kolitha Dissanayake', 'Senior Associate', 4, 'kolitha.dissanayake@fjgdesaram.com', '0114605175', 'images/lawyers/26901.jpg', '2016-04-17 09:51:09', '2016-06-29 19:22:55', 0),
(10, 'Manjula Ellepola', 'Partner  ', 5, 'manjula.ellepola@fjgdesaram.com', '0114605147', 'images/lawyers/92149.jpg', '2016-04-17 09:51:09', '2016-06-29 19:22:29', 1),
(11, 'Anjali Fernando', 'Partner  ', 4, 'anjali.fernando@fjgdesaram.com', '0114605123', 'images/lawyers/30563.jpg', '2016-04-17 09:51:09', '2016-06-29 19:21:44', 0),
(12, 'Rozali Fernando', 'Litigator', 2, 'rozali.fernando@fjgdesaram.com', '0114605132', 'images/lawyers/16319.jpg', '2016-04-17 09:51:10', '2016-06-29 19:21:13', 0),
(13, 'Tania Fernando', 'Associate', 3, 'tania.fernando@fjgdesaram.com', '0114605186', 'images/lawyers/21083.jpg', '2016-04-17 09:51:10', '2016-06-29 19:20:48', 0),
(14, 'Tilani Ford', 'Senior Associate', 4, 'tilani.ford@fjgdesaram.com', '0114605134', 'images/lawyers/75832.jpg', '2016-04-17 09:51:10', '2016-06-29 19:19:23', 0),
(15, 'Shanaka Gunasekara', 'Senior Litigator', 2, 'shanaka.gunasekara@fjgdesaram.com', '0114605112', 'images/lawyers/92618.jpg', '2016-04-17 09:51:10', '2016-06-29 19:18:31', 0),
(17, 'Natalie Gnaniah', 'Executive-Legal', 1, 'natalie.gnaniah@fjgdesaram.com', '0114605119', 'images/lawyers/30327.jpg', '2016-04-17 09:51:10', '2016-06-29 19:18:02', 0),
(18, 'Christina Hettiarachchi', 'Legal Assistant', 2, 'christina.hettiarachchi@fjgdesaram.com', '0114605131', 'images/lawyers/80300.jpg', '2016-04-17 09:51:10', '2016-06-29 19:17:35', 0),
(19, 'Ashiq Hassim', 'Litigator', 2, 'ashiq.hassim@fjgdesaram.com', '0114605157', 'images/lawyers/Ashiq_Hassim.jpg', '2016-04-17 09:51:10', '2016-06-29 19:16:50', 0),
(20, 'Kasuni Jayaweera', 'Litigator', 2, 'kasuni.jayaweera@fjgdesaram.com', '0114605144', 'images/lawyers/78095.jpg', '2016-04-17 09:51:10', '2016-06-29 19:16:32', 0),
(21, 'Suganthie Wijayasuriya-Kadirgamar', 'Senior Partner', 3, 'suganthie.wijayasuriya@fjgdesaram.com', '0114605141', 'images/lawyers/lawyer.png', '2016-04-17 09:51:10', '2016-06-29 19:16:05', 1),
(22, 'U.L. Kadurugamuwa', 'Consultant', 4, 'ul.kadurugamuwa@fjgdesaram.com', '0114605114', 'images/lawyers/lawyer.png', '2016-04-17 09:51:10', '2016-06-29 19:15:19', 0),
(23, 'Roshani Kobbekaduwa', 'Partner ', 4, 'roshani.kobbekaduwa@fjgdesaram.com', '0114605122', 'images/lawyers/83360.jpg', '2016-04-17 09:51:10', '2016-06-29 19:14:40', 0),
(24, 'Jayathri Kulatilaka', 'Partner  ', 3, 'jayathri.kulatilaka@fjgdesaram.com', '0114605152', 'images/lawyers/73311.jpg', '2016-04-17 09:51:10', '2016-06-29 19:14:07', 0),
(25, 'Roshika Kulatunga', 'Manager', 1, 'roshika.kulatunga@fjgdesaram.com', '0114605127', 'images/lawyers/79631.jpg', '2016-04-17 09:51:10', '2016-06-29 19:13:24', 0),
(26, 'Sankha Karunaratne', 'Senior Associate', 4, 'sankha.karunaratne@fjgdesaram.com', '0114605121', 'images/lawyers/17000.jpg', '2016-04-17 09:51:10', '2016-06-29 19:12:45', 0),
(27, 'Charana Kanankegamage', 'Associate', 4, 'charana.kanankegamage@fjgdesaram.com', '0114605156', 'images/lawyers/60134.jpg', '2016-04-17 09:51:10', '2016-06-29 19:12:23', 0),
(28, 'Himali Mudadeniya', 'Partner  ', 4, 'himali.mudadeniya@fjgdesaram.com', '0114605120', 'images/lawyers/21486.jpg', '2016-04-17 09:51:10', '2016-06-29 19:12:06', 0),
(29, 'Shamil Mueen', 'Associate', 4, 'shamil.mueen@fjgdesaram.com', '0114605136', 'images/lawyers/96413.jpg', '2016-04-17 09:51:10', '2016-06-29 19:11:43', 0),
(30, 'Pavithra Navarathne', 'Senior Associate', 4, 'pavithra.navarathne@fjgdesaram.com', '0114605179', 'images/lawyers/45374.jpg', '2016-04-17 09:51:10', '2016-06-29 19:10:25', 0),
(31, 'Shehani Nayagam', 'Executive-Legal', 1, 'shehani.nayagam@fjgdesaram.com', '0114605170', 'images/lawyers/47406.jpg', '2016-04-17 09:51:10', '2016-06-29 19:09:37', 0),
(32, 'Shevanthi Seresinhe-Perera', 'Senior Associate', 4, 'shevanthi.perera@fjgdesaram.com', '0114605177', 'images/lawyers/13161.jpg', '2016-04-17 09:51:10', '2016-06-29 19:09:00', 0),
(33, 'Udeesha Perera', 'Executive-Legal', 1, 'udeesha.perera@fjgdesaram.com', '0114605192', 'images/lawyers/39705.jpg', '2016-04-17 09:51:10', '2016-06-29 19:07:10', 0),
(35, 'Amali Peiris', 'Executive-Legal', 1, 'amali.peiris@fjgdesaram.com', '0114605193', 'images/lawyers/75477.jpg', '2016-04-17 09:51:10', '2016-06-29 19:06:40', 0),
(36, 'Avindra Rodrigo', 'Counsel', 2, 'avindra.rodrigo@fjgdesaram.com', '0114605137', 'images/lawyers/lawyer.png', '2016-04-17 09:51:10', '2016-06-29 19:06:12', 1),
(37, 'Aruna De Silva', 'Litigator', 2, 'aruna.desilva@fjgdesaram.com', '0114605151', 'images/lawyers/24255.jpg', '2016-04-17 09:51:10', '2016-06-29 19:04:23', 0),
(38, 'Medhya Samarasinghe', 'Litigator', 2, 'medhya.samarasinghe@fjgdesaram.com', '0114605133', 'images/lawyers/67144.jpg', '2016-04-17 09:51:10', '2016-06-29 19:03:49', 0),
(39, 'Upekha Senaratne', 'Partner  ', 3, 'upekha.senaratne@fjgdesaram.com', '0114605154', 'images/lawyers/46167.jpg', '2016-04-17 09:51:10', '2016-06-29 19:03:13', 0),
(40, 'Patali Thrimanne', 'Litigator', 2, 'patali.thrimanne@fjgdesaram.com', '0114605100', 'images/lawyers/27162.jpg', '2016-04-17 09:51:10', '2016-06-29 19:02:49', 0),
(41, 'Dinukshi Thalgahagoda', 'Partner  ', 1, 'dinukshi.thalgahagoda@fjgdesaram.com', '0114605180', 'images/lawyers/33624.jpg', '2016-04-17 09:51:10', '2016-06-30 05:07:02', 0),
(42, 'Ananda Tikirirathne', 'Senior Litigator', 2, 'ananda.tikiriratne@fjgdesaram.com', '0114605159', 'images/lawyers/Ananda_Tikirirathne.png', '2016-04-17 09:51:10', '2016-06-29 19:01:57', 0),
(43, 'Shehara Varia', 'Partner  ', 4, 'shehara.varia@fjgdesaram.com', '0114605128', 'images/lawyers/20538.jpg', '2016-04-17 09:51:10', '2016-06-29 19:00:17', 0),
(44, 'Rosy Vanlangenberg', 'Associate', 3, 'rosy.vanlangenberg@fjgdesaram.com', '0114605187', 'images/lawyers/37651.jpg', '2016-04-17 09:51:10', '2016-06-29 18:58:15', 0),
(45, 'Anuradha Wijesooriya', 'Litigator', 2, 'anuradha.wijesooriya@fjgdesaram.com', '0114605139', 'images/lawyers/61013.jpg', '2016-04-17 09:51:10', '2016-06-29 18:57:07', 0),
(52, 'Ramesh Perera', 'Legal Assistant', 4, 'ramesh.perera@fjgdesaram.com', '0114605158', 'images/lawyers/31698.jpg', '2016-06-06 01:13:02', '2016-06-29 18:55:59', 0),
(53, 'Deepthi Abeysena', 'Director', 5, 'deepthi.fernando@fjgdesaram.com', '0114605132', 'images/lawyers/83989.jpg', '2016-06-07 10:58:16', '2016-06-29 18:54:45', 0),
(54, 'Shehara Gunasekara', 'Associate', 5, 'shehara.gunasekara@fjgdesaram.com', '0114605151', 'images/lawyers/79134.jpg', '2016-06-07 11:02:38', '2016-06-29 18:55:09', 0),
(55, 'Anushika Abeywickrama', 'Associate', 5, 'anushika.abeywickrama@fjgdesaram.com', '0114605146', 'images/lawyers/61387.jpg', '2016-06-07 11:04:44', '2016-06-29 18:55:27', 0),
(56, 'Oshani Wijewardena', 'Associate', 5, 'oshani.wijewardena@fjgdesaram.com', '0114605195', 'images/lawyers/64183.jpg', '2016-06-07 11:06:20', '2016-06-29 18:53:50', 0),
(57, 'Harshini Serasinghe', 'Executive Legal', 1, 'harshini.serasinghe@fjgdesaram.com', '0777373980', 'images/lawyers/27503.jpg', '2016-06-07 11:08:10', '2016-06-29 18:52:15', 0),
(58, 'Thilini Samarawickrama', 'Legal Assistant', 1, 'thilini.samarawickrama@fjgdesaram.com', '0114605134', 'images/lawyers/93552.jpg', '2016-06-07 11:09:41', '2016-06-29 18:51:36', 0),
(59, 'Tharushi Perera', 'Legal Assistant', 3, 'tharushi.perera@fjgdesaram.com', '+94114605100', 'images/lawyers/44962.jpg', '2016-06-07 11:10:53', '2016-06-24 16:19:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lawyer_divisions`
--

CREATE TABLE IF NOT EXISTS `lawyer_divisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `division` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `lawyer_divisions`
--

INSERT INTO `lawyer_divisions` (`id`, `division`) VALUES
(1, 'Corporate Services (Private) Limited'),
(2, 'Litigation'),
(3, 'Intellectual Property'),
(4, 'Corporate Law'),
(5, 'Real Property');

-- --------------------------------------------------------

--
-- Table structure for table `legal_alerts`
--

CREATE TABLE IF NOT EXISTS `legal_alerts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `legal_alerts_type_id_foreign` (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `legal_alerts`
--

INSERT INTO `legal_alerts` (`id`, `title`, `message`, `type_id`, `created_at`, `updated_at`) VALUES
(26, 'Requirement to repatriate export proceeds from the export of goods', 'Payments received from the export of goods from Sri Lanka must be repatriated to Sri Lanka within 90 days from the date of export. \r\n\r\nBy virtue of Extraordinary Gazette Notification No.759/15 dated 26th March 1993 repatriation of payments received for goods exported from Sri Lanka was exempted.\r\n\r\nThis exemption has been repealed and as a result,\r\n1. any such payment received on or after 01st April 2016 has to be repatriated to Sri Lanka within 90 days from the date of exportation of goods;\r\n\r\n2. any such payment retained abroad  as at 01st April 2016 has to be repatriated to Sri  Lanka not later than 01st May 2016\r\n\r\nGazette Notification (Extraordinary) No. 1960/66 dated 1st April 2016', 1, '2016-06-15 05:49:45', '2016-06-18 03:14:37'),
(27, 'Change in methodology of calculating price multiples', 'The Colombo Stock Exchange will calculate the P/E (price earning ratio) and DY (dividend yield ratio) taking into account “Rolling Four Quarterly Earnings”.  P/BV (price to book value ratio) will be calculated based on the latest quarterly results.\r\n\r\nThe change is effective from May 03rd, 2016.\r\n\r\nColombo Stock Exchange circular dated 25th April 2016', 5, '2016-06-15 05:58:08', '2016-06-18 03:14:52'),
(28, 'Reduction of threshold for liability to pay nations building tax', 'A person or partnership whose liable turnover from any business is not less than LKR 3 million for any quarter is liable for national building tax.\r\n\r\nBusinesses that were exempted, will be required to pay nations building tax, including businesses that provide telecommunication services, supply electricity (including to the national grid), lubricants.\r\n\r\nInland Revenue department notices dated 29th April 2016 and 02nd May 2016.', 6, '2016-06-15 05:59:07', '2016-06-18 03:15:04'),
(29, 'Increase of value added tax', 'The value added rate is 15% (including on financial services) with effect from May 02nd, 2016 as informed by the Minister of Finance.  Persons or partnerships with VAT liable supplies of more than LKR 3 million a quarter or LKR 12 million per annum must be registered at the Inland Revenue Department.\r\n\r\nInland Revenue department notices dated 29th April and 02nd May 2016', 6, '2016-06-15 05:59:45', '2016-06-18 03:15:27'),
(30, 'Companies allowed to borrow from residents outside Sri Lanka', 'Permission has been granted to companies to borrow from residents outside Sri Lanka under an External Commercial Borrowing Scheme (ECBS) subject to certain conditions.\r\n\r\nThe conditions are that:\r\n1. The borrower mitigate the exchange risk through appropriate hedging instruments with any licensed commercial bank in Sri Lanka, unless the borrower has foreign exchange earnings in the same currency.\r\n\r\n2. The tenure of loans be at least 3 years.\r\n\r\n3. The maximum amount of borrowing be based on the financial soundness of the company to repay the loan based on its audited financial statements for at least the most recent three years and the business plan.\r\n\r\n4. The rate of interest be competitive and favourable when compared with the prevailing market rates in Sri Lanka.\r\n\r\n5. All proceeds and the repayments of the borrowing be routed through an External Commercial Borrowing Account maintained by the borrower in any designated foreign currency in a licensed commercial bank in Sri Lanka.\r\n\r\nThis permission is applicable to all companies incorporated under the Companies Act No.7 of 2007, other than licensed commercial banks, licensed specialized banks, licensed finance companies, specialized leasing companies, companies limited by guarantee and overseas companies.\r\n\r\nGazette (Extraordinary) No.1970/8 dated 07/06/2016\r\n', 1, '2016-06-15 06:01:46', '2016-06-18 03:15:41'),
(31, 'Foreigners allowed to invest in money lending and security services companies', 'The prohibition with regard to non-residents acquiring shares in a company carrying the business of money lending or security services has been removed.\r\n\r\nAccordingly, the general permission by the Controller of Exchange is now available for the issue and transfer of shares in a company carrying money lending business or providing security services up to 100% of the issued capital of such company, to approved country funds, approved regional funds, corporate bodies incorporated outside Sri Lanka and individuals resident outside Sri Lanka.\r\n\r\nSecurity services include security managements, assessment and consulting to individuals or private organisations.\r\n\r\nThe payments for shares must be made through a Securities Investment Account.\r\n\r\nGazette (Extraordinary) No.1970/49 dated 10/06/2016.\r\n\r\nSimilarly, permission has been granted by the Controller of Exchange for a registered overseas company providing security services to carry on commercial, trading or industrial activity through a place of business (commonly referred to as a branch office) in Sri Lanka.\r\n\r\nGazette (Extraordinary) No.1970/50 dated 10/06/2016.', 1, '2016-06-16 11:07:38', '2016-06-18 03:15:57'),
(32, 'A new microfinance law', 'The Microfinance Act was certified by the speaker on the 20th of May 2016, consequent to it being passed in Parliament.  The date of operation of the Act or parts thereof is be appointed by the Minister of Finance by order published by Gazette.\r\n\r\nThe Act provides for the licensing, regulation and supervision of companies carrying on microfinance business.  A public company incorporated under the Companies Act is eligible to apply for a license. \r\nThe nature of business that a licensed microfinance company may carry on is listed in the Act and includes the provision of financial accommodation in cash or kind, to accept time and savings deposits and to provide credit for identified purposes.  Further business that may be carried out may also be specified by notification by the Monetary Board.\r\n\r\nThe licensing authority is the Monetary Board of Sri Lanka.  \r\n\r\nThe Director in the Central Bank of the Monetary Board of Sri Lanka to whom the subject of microfinance is assigned is empowered to examine and supervise licensed microfinance company and to monitor the compliance with directions issued by the Monetary Board from time to time. \r\n\r\nMicrofinance Act No 6 of 2016', 4, '2016-06-18 03:19:06', '2016-06-18 03:19:06'),
(33, 'Change in reporting requirements for money broking companies', 'Money broking companies are required to upload information on each quote/bid provided to them by transacting parties and each transaction intermediated through a money broking company, not later than 30 minutes of such transaction through the on-line interface provided to such money broking companies by the Central Bank of Sri Lanka.\r\n\r\nThe Chief Executive Officer and/or the Officer-in-Charge of the dealing room of the money broking company is responsible for uploading the specified information stated herein within the specified time period.\r\nInformation on bids/quotes and transactions sent by faxes is entertained only in the event of a failure of the on-line bidding system and once the system is revived such information is to be uploaded into the system.\r\n\r\nThe change is effective from June 1st, 2016\r\n\r\nGazette Notification (Extraordinary) No. 1969/17 dated 1st June 2016', 4, '2016-06-22 08:39:13', '2016-06-22 08:39:13');

-- --------------------------------------------------------

--
-- Table structure for table `legal_alert_types`
--

CREATE TABLE IF NOT EXISTS `legal_alert_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `legal_alert_types`
--

INSERT INTO `legal_alert_types` (`id`, `type`, `color`) VALUES
(1, 'Exchange Control', '#BFAD26'),
(2, 'Inland Revenue', '#6babb4'),
(3, 'Colombo Stock Exchange', '#d2a56f'),
(4, 'Banking and Finance', '#FBA51A'),
(5, 'Capital Markets', '#9BCA3C'),
(6, 'Tax', '#71C6A5'),
(7, 'Companies', '#426FB6'),
(8, 'Employment ', '#756BB0'),
(9, 'Investments', '#D1499B'),
(10, 'Property', '#ED1E24'),
(11, 'Insurance', '#70C3ED'),
(12, 'Intellectual Property', '#AAD596'),
(13, 'Utilities', '#FEC679'),
(14, 'Transport and Logistics', '#DF92BE'),
(15, 'Consumer Affairs', '#ABB7DD');

-- --------------------------------------------------------

--
-- Table structure for table `legal_matters`
--

CREATE TABLE IF NOT EXISTS `legal_matters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `lead_lawyer_id` int(10) NOT NULL,
  `represent_company` text COLLATE utf8_unicode_ci NOT NULL,
  `next_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `closed` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `legal_matters_type_id_foreign` (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=116 ;

--
-- Dumping data for table `legal_matters`
--

INSERT INTO `legal_matters` (`id`, `name`, `type_id`, `lead_lawyer_id`, `represent_company`, `next_date`, `updated_date`, `closed`, `created_at`, `updated_at`) VALUES
(106, 'test', 2, 116, 'Ryan and Sons', '0000-00-00 00:00:00', '2016-06-15 13:28:16', 1, '2016-06-15 07:58:16', '2016-06-15 08:02:00'),
(107, 'test', 2, 116, 'O''Hara Inc', '0000-00-00 00:00:00', '2016-06-15 13:30:45', 1, '2016-06-15 08:00:45', '2016-06-15 08:01:45'),
(108, 'test', 2, 115, 'Kuhlman and Sons', '2016-06-23 00:00:00', '2016-06-15 13:31:26', 1, '2016-06-15 08:01:26', '2016-06-29 18:11:08'),
(109, 'Test', 2, 141, 'WAD', '0000-00-00 00:00:00', '2016-06-22 12:09:31', 1, '2016-06-22 06:31:45', '2016-06-29 18:11:23'),
(111, 'matter 001', 1, 0, '', '0000-00-00 00:00:00', '2016-06-27 10:03:56', 1, '2016-06-27 04:33:56', '2016-06-29 18:11:42'),
(113, 'test', 2, 141, 'WAD', '2016-06-14 00:00:00', '2016-06-29 23:51:23', 1, '2016-06-29 18:12:32', '2016-06-29 18:22:33'),
(114, 'Test Matter', 1, 129, 'WAD', '0000-00-00 00:00:00', '2016-06-30 07:31:38', 1, '2016-06-30 02:01:38', '2016-06-30 02:03:02'),
(115, 'Test Matter', 1, 152, 'WAD', '0000-00-00 00:00:00', '2016-06-30 10:39:03', 1, '2016-06-30 05:09:03', '2016-06-30 05:11:37');

-- --------------------------------------------------------

--
-- Table structure for table `legal_matter_notes`
--

CREATE TABLE IF NOT EXISTS `legal_matter_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `legal_matter_id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `legal_matter_notes_legal_matter_id_foreign` (`legal_matter_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=63 ;

--
-- Dumping data for table `legal_matter_notes`
--

INSERT INTO `legal_matter_notes` (`id`, `legal_matter_id`, `date`, `note`, `created_at`, `updated_at`) VALUES
(61, 108, '2016-06-15 00:00:00', 'test sample note', '2016-06-15 08:02:34', '2016-06-15 08:02:34'),
(62, 113, '2016-06-01 00:00:00', 'test', '2016-06-29 18:21:44', '2016-06-29 18:21:44');

-- --------------------------------------------------------

--
-- Table structure for table `legal_matter_types`
--

CREATE TABLE IF NOT EXISTS `legal_matter_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `legal_matter_types`
--

INSERT INTO `legal_matter_types` (`id`, `type`) VALUES
(1, 'Incorporation of a Company'),
(2, 'Litigation'),
(3, 'IP Registration');

-- --------------------------------------------------------

--
-- Table structure for table `legal_matter_users`
--

CREATE TABLE IF NOT EXISTS `legal_matter_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `legal_matter_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `legal_matter_users_legal_matter_id_foreign` (`legal_matter_id`),
  KEY `legal_matter_users_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=333 ;

--
-- Dumping data for table `legal_matter_users`
--

INSERT INTO `legal_matter_users` (`id`, `legal_matter_id`, `user_id`, `created_at`, `updated_at`) VALUES
(318, 108, 5, NULL, NULL),
(319, 108, 364, NULL, NULL),
(320, 108, 361, NULL, NULL),
(321, 109, 60, NULL, NULL),
(322, 109, 65, NULL, NULL),
(323, 109, 400, NULL, NULL),
(324, 109, 375, NULL, NULL),
(327, 113, 65, NULL, NULL),
(328, 113, 375, NULL, NULL),
(329, 114, 65, NULL, NULL),
(330, 114, 363, NULL, NULL),
(331, 115, 65, NULL, NULL),
(332, 115, 375, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_03_23_155608_create_quick_reference_guides_table', 1),
('2016_03_23_160012_create_admins_table', 1),
('2016_03_23_160325_create_legal_alerts_table', 1),
('2016_03_23_160703_create_password_recovery_requests_table', 1),
('2016_03_23_191130_create_appointment_requests_table', 1),
('2016_03_23_191748_create_appointment_request_users_table', 1),
('2016_03_23_192126_create_lawyer_divisions_table', 1),
('2016_03_23_192434_create_lawyers_table', 1),
('2016_03_23_192914_create_designations_table', 1),
('2016_03_23_193258_create_corporate_users_table', 1),
('2016_03_23_194132_create_firm_users_table', 1),
('2016_03_23_194613_create_account_requests_table', 1),
('2016_03_23_194913_create_legal_matter_types_table', 1),
('2016_03_23_195047_create_legal_matters_table', 1),
('2016_03_23_195449_create_legal_matter_notes_table', 1),
('2016_03_23_195919_create_legal_matter_users_table', 1),
('2016_03_23_200148_create_company_setups_table', 1),
('2016_03_23_200424_create_company_setups_shareholders_table', 1),
('2016_03_23_200647_create_company_setups_directors_table', 1),
('2016_03_23_213802_add_foreign_keys_to_appointment_request_users', 1),
('2016_03_23_214448_add_foreign_keys_to_lawyers_table', 1),
('2016_03_23_214921_add_foreign_keys_to_corporate_users_table', 1),
('2016_03_23_215040_add_foreign_keys_to_firm_users_table', 1),
('2016_03_23_215226_add_foreign_keys_to_account_requests_table', 1),
('2016_03_23_215414_add_foreign_keys_to_company_setups_shareholders_table', 1),
('2016_03_23_220154_add_foreign_keys_to_company_setups_directors_table', 1),
('2016_03_23_220327_add_foreign_keys_to_legal_matters_table', 1),
('2016_03_23_220527_add_foreign_keys_to_legal_matter_notes_table', 1),
('2016_03_23_220653_add_foreign_keys_to_legal_matter_users_table', 1),
('2016_03_24_035217_add_foreign_keys_to_password_recovery_requests', 1),
('2016_03_28_062200_modifyLawyerTable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_recovery_requests`
--

CREATE TABLE IF NOT EXISTS `password_recovery_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `password_recovery_requests_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Dumping data for table `password_recovery_requests`
--

INSERT INTO `password_recovery_requests` (`id`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 1, NULL, '2016-04-20 14:09:24', '2016-04-20 14:09:24'),
(3, 1, NULL, '2016-04-20 14:21:58', '2016-04-20 14:21:58'),
(4, 1, NULL, '2016-04-26 15:04:44', '2016-04-26 15:04:44'),
(5, 1, NULL, '2016-04-26 16:13:36', '2016-04-26 16:13:36'),
(6, 1, NULL, '2016-04-26 16:14:44', '2016-04-26 16:14:44'),
(7, 1, NULL, '2016-04-26 16:16:29', '2016-04-26 16:16:29'),
(8, 1, NULL, '2016-04-27 10:23:38', '2016-04-27 10:23:38'),
(9, 1, NULL, '2016-04-27 10:24:11', '2016-04-27 10:24:11'),
(10, 1, NULL, '2016-04-27 10:25:09', '2016-04-27 10:25:09'),
(11, 1, NULL, '2016-04-27 10:27:33', '2016-04-27 10:27:33'),
(12, 1, NULL, '2016-04-27 10:44:21', '2016-04-27 10:44:21'),
(13, 1, NULL, '2016-04-27 10:45:26', '2016-04-27 10:45:26'),
(14, 1, NULL, '2016-04-27 10:45:40', '2016-04-27 10:45:40'),
(15, 1, NULL, '2016-04-27 14:34:25', '2016-04-27 14:34:25'),
(16, 10, NULL, '2016-05-12 18:30:08', '2016-05-12 18:30:08'),
(21, 1, NULL, '2016-05-31 16:36:09', '2016-05-31 16:36:09'),
(22, 1, NULL, '2016-05-31 16:36:35', '2016-05-31 16:36:35'),
(23, 65, NULL, '2016-06-14 03:54:51', '2016-06-14 03:54:51'),
(24, 65, NULL, '2016-06-14 04:40:06', '2016-06-14 04:40:06'),
(25, 400, NULL, '2016-06-15 11:46:40', '2016-06-15 11:46:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quick_reference_guides`
--

CREATE TABLE IF NOT EXISTS `quick_reference_guides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `quick_reference_guides`
--

INSERT INTO `quick_reference_guides` (`id`, `title`, `body`, `publish`, `created_at`, `updated_at`) VALUES
(8, 'PROTECTION OF FOREIGN INVESTMENT', 'Foreign investment is guaranteed protection by the Constitution of Sri Lanka. As per Article 157 of the Constitution, any treaty or agreement between the Government of Sri Lanka and a foreign government for the promotion and protection of foreign investments has the force of law and no executive or administrative action can be taken against such an investment.\r\n\r\nSri Lanka has signed Bilateral Investment Protection Agreements with the 28 countries given below:\r\nAustralia; Belgium -Luxembourg Economic Union; Canada; China; Denmark; Egypt; France; Finland; Malaysia; Germany; India; Indonesia; Iran; Italy; Japan; Korea; Kuwait; The Netherlands; Norway; Pakistan; Romania; Singapore; Sweden; Switzerland; Thailand; United Kingdom; United States of America; Vietnam\r\n\r\nSri Lanka is also a founder-member of the Multilateral Investment Guarantee Agency (MIGA) of the World Bank which provides guarantees against non-commercial risks, such as those arising out of political changes or political instability and insecurity. ', 1, '2016-06-15 13:44:06', '2016-06-16 10:30:23'),
(9, 'FOREIGN INVESTMENT IN SRI LANKA', 'IN SHARES:\r\nThe Exchange Control Act prohibits a person resident outside Sri Lanka from acquiring shares in a company incorporated in Sri Lanka without the general or special approval of the Central Bank.\r\n\r\nGeneral approval has been given by the Central Bank for the issue and transfer of shares in a company upto 100% of the issued capital of such company to, corporate bodies incorporated outside Sri Lanka and individuals resident outside Sri Lanka in all areas of business other than in the specific areas listed below subject to the condition that the payment is remitted through a Securities Investment Account (SIA).\r\n\r\nProhibited Areas: Pawn broking; Retail trade with a capital of less than One Million US Dollars and Coastal fishing\r\n\r\nRestricted Areas (investment up to 40% of the issued share capital is allowed unless a higher percentage is permitted): Production of goods where Sri Lanka’s exports are subject to internationally determined quota restrictions; growing and primary processing of tea, rubber, coconut, cocoa, rice, sugar and spices; mining and primary processing of non-renewable national resources; timber based industries using local timber; fishing (deep sea fishing); mass communications; education; freight forwarding; travel agencies and shipping agencies.\r\n\r\nAreas Requiring Special Approval: Air transportation; coastal shipping; industrial undertaking in the second schedule of the Industrial Promotion Act, no. 46 of 1990 (namely, any industry manufacturing arms, ammunitions, explosives, military vehicles and equipment aircraft and other military hardware; poisons, narcotics, alcohols, dangerous drugs and toxic, hazardous or carcinogenic materials; any industry producing currency, coins or security documents); large scale mechanized mining of gems; lotteries.\r\n\r\nA similar permission has been granted for a company that is incorporated outside Sri Lanka and has established a place of business, commonly referred to as a branch office, in Sri Lanka.\r\n\r\nInvestment Incentives:\r\nSri Lanka offers selected incentive regimes to promote private investments, both domestic and foreign into desired sectors of the economy. These incentives mainly include exemption on Corporate Income Tax, Customs Duty, Value Added Tax, and Ports & Airports Development Levy and exemption from Exchange Control.\r\n\r\nThe Board of Investment of Sri Lanka (BOI) is the authority set up by the Government of Sri Lanka for the promotion and facilitation of foreign investment. The BOI Law No. 4 of 1978 provides for two types of investment approvals.\r\n\r\nUnder Section 17 of the BOI Law: The BOI is empowered to grant special incentives to companies satisfying specific eligibility criteria designed to meet strategic economic objectives of the government.  The mechanism through which such concessions are granted is the Agreement entered into with the BOI which modifies, exempts and waives identified laws in keeping with the BOI Regulations.  These laws include Inland Revenue Act, Customs Ordinance, Exchange Control Act and Import Control Ordinance.\r\n\r\nUnder Section 16 of the BOI Law: Companies approved under section 16 are governed under the normal laws of the country. The only advantage in obtaining approval under this section is that the BOI will, subject to certain conditions, grant recommendation for the issue of resident visas to expatriate employees of the company.\r\n\r\nIN LAND:\r\nIn terms of the Land (Restrictions on Alienation). Act, No. 38 of 2014, a foreigner or foreign company cannot purchase land in Sri Lanka unless such purchase is made through a company incorporated in Sri Lanka in which the foreign shareholding (direct or indirect) is less than 50% of the share capital.\r\n\r\nThis law exempts certain types of purchases of land and entities from the application of the above prohibition. For example, foreigners/ foreign companies are permitted purchase condominium units above the 4th floor subject to the entire value being paid upfront through an inward foreign remittance. \r\n\r\nA foreigner/ foreign company may lease land in Sri Lanka subject to the period of lease not exceeding 99 years. The land lease tax of 15% of the total rental payable which was sought to be imposed by the above Act is no longer levied pursuant to government policies.  ', 1, '2016-06-15 13:46:40', '2016-06-16 10:30:36'),
(10, 'ELECTRONIC TRANSACTIONS ACT', 'The Electronic Transactions Act No. 19 of 2006 provides for the legal recognition of data messages, electronic documents, electronic records and other communications in electronic form in Sri Lanka. The said Act also facilitates the formation of electronic contracts. \r\n\r\nA party may enter into transactions electronically other than for those specific transactions that have been excluded by the Act, namely, wills or other testamentary dispositions, powers-of-attorney, sale or conveyance of immovable property, trusts (excluding constructive, implied and resulting trusts), bills of exchange, telecommunication licenses, and any other acts, transactions or documents that may be specified by the Minister by regulation made under the Act.', 1, '2016-06-15 13:58:16', '2016-06-16 10:30:50'),
(11, 'THE AVOIDANCE OF DOUBLE TAXATION', 'Sri Lanka has entered into and ratified agreements for the avoidance of double taxation with a number of countries.\r\n\r\nThese agreements, broadly, seek to eliminate the double taxation of income arising in one territory earned by residents of another and contains specific provisions on the determination of income tax on profits earned by a resident of one territory who/which is carrying on business in the other territory. These agreements also deal with determination of income tax on other income and profits such as dividends, interest, income from immovable property and royalties.   \r\n\r\nThe list of countries with which Sri Lanka has entered into agreements for the avoidance of double taxation are as follows:\r\n\r\nAustralia; Bangladesh; Belarus; Belgium; Canada; China; Denmark; Finland; France; Germany; Hong Kong;  India;  Indonesia; Iran; Italy; Japan; Korea; Kuwait; Luxembourg; Malaysia;  Mauritius; Nepal; Netherlands; Norway; Oman; Pakistan; Palestine; Philippines; Poland; Qatar; Romania; Russia; Saudi; Arabia; Seychelles; Singapore; Sweden; Switzerland; Thailand; United Arab Emirates; United Kingdom.; United States of America; Vietnam;', 1, '2016-06-15 13:59:14', '2016-06-16 10:31:21'),
(14, 'STAMP DUTY ', 'Stamp duty is payable on certain specified instruments.  The non-payment of stamp duty does not invalidate the document which is required to be stamped.  The effect of non-payment of stamp duty is that the particular document will not be admissible in evidence unless the stamp duty is paid together with any fine imposed by the Department of Inland Revenue. \r\n\r\nStamp duties on certain frequently used instruments are given below;\r\n\r\nTRANSFER OF IMMOVABLE PROPERTY (LAND/BUILDING)\r\nIf the value is Rs.100,000/- or less - 3%\r\nIf the value is over Rs.100,000/-, 3% for the first Rs. 100,000/- and 4% for the balance. \r\n\r\nGIFT OF IMMOVABLE PROPERTY (LAND/BUILDING) \r\nIf the value is Rs.50,000/- or less- 3%\r\nIf the value is over Rs.50,000/- 3% for the first Rs. 50,000/- and 2% for the balance.\r\n\r\nMORTGAGE OF PROPERTY (IMMOVABLE/MOVABLE)\r\n0.1%\r\n(Note: Mortgage to secure a loan of Rs.3,000,00/- or less for the construction of a house or a purchase of a house or of a site for the construction of a house obtained from a bank, a finance company, a registered Co-operative Society, any public corporation, any provident fund approved by the Commissioner General of Inland Revenue or any institution approved by the Minister in charge of housing is exempt from stamp duty)        \r\n\r\nLEASE OF PROPERTY ((IMMOVABLE/MOVABLE)\r\n1%\r\nIn the event the period of the lease is more than 20 years Stamp duty is chargeable only on the aggregate rental for the first 20 years of the said period (Note: Lease of any building where lease rental does not exceed Rs.5,000/- per month is exempt from stamp duty)  \r\n\r\nRECEIPTS\r\nUp to and including Rs.25,000/-    Exempt\r\nAbove Rs.25,000/-    Rs.25.00\r\n\r\nPROMISSORY NOTES\r\n0.1%\r\n\r\nAFFIDAVIT \r\nRs.	50.00 \r\n(Note: Any Affidavit made on the request of any public officer or in compliance with any requirement imposed by any written law is exempt from stamp duty).', 1, '2016-06-15 14:15:47', '2016-06-16 10:32:41'),
(15, 'TYPES OF COMPANIES AND FEES PAYABLE TO THE ROC', 'A company incorporated under the Companies Act can be either;\r\n\r\na) a limited company – a company that issues shares, the holders of which have a liability to contribute to the asset of the company, if any, specified in the articles as attaching to those shares\r\n\r\nb) a company limited by guarantee – a company that does not issue shares and whose members undertake to contribute to the assets of the company in the event of it being put into liquidation in an amount specified in that company’s articles.\r\n \r\n* Unlimited companies whilst referred to in the law are not commonly used in Sri Lanka.\r\n\r\nA company incorporated outside Sri Lanka which establishes a place of business in Sri Lanka is required to register itself as an overseas company within one month of establishing a place of business.\r\n\r\nFees payable for incorporation \r\nFor approval of a name of a company (Name Request) - Rs. 1,150\r\nFor registration of a Limited Company- Form 1 -  Rs. 17,250, Articles- Rs. 1,150, Form 19 - Rs. 1,150, Form 18 - Rs. 1,150 (per director), Total - Rs. 20,700/-\r\nFor registration of a Company Limited by Guarantee - Form 5 -  Rs. 28,750, Articles- Rs. 1,150, Form 19 - Rs. 1,150, Form 18 - Rs. 1,150 (per director), Total - Rs. 32,200/-\r\n\r\nFees payable for the registration of an overseas company \r\n\r\nFor registration of a place of business in Sri Lanka by an Overseas Company - Rs. 57,500\r\n\r\nFees payable for other commonly used forms\r\n\r\nFor registration of an annual return - Rs. 5,750\r\nFor registration of mortgages, charges and debentures - Rs. 5,750\r\nCompany Annual levy - Private companies Rs. 60,000, Listed companies Rs. 500,000, Overseas Companies engaging business in Sri Lanka Rs. 100,000\r\n\r\n(The above fees are inclusive of 15 % VAT).', 1, '2016-06-15 14:17:49', '2016-06-16 10:33:04'),
(16, 'TAXATION ', 'INCOME TAX\r\nIncome tax is imposed under Inland Revenue Act No. 10 of 2006 as amended.  \r\n\r\nAs per the Inland Revenue Act, a person resident in Sri Lanka is liable to income tax in respect of the profits and income, wherever arising.  \r\n\r\nA person not resident in Sri Lanka is liable to pay income tax on profits and income arising in or derived from Sri Lanka.\r\n\r\nSome important income tax rates are given below.\r\nFor companies - 28% on the taxable income\r\n\r\nFor individuals\r\nOn the first Rs. 500,000/- of the taxable income - 4%\r\nOn the next Rs. 500,000/- of the taxable income - 8%\r\nOn the next Rs. 500,000/- of the taxable income - 12%\r\nOn the next Rs. 500,000/- of the taxable income - 16%\r\nOn the next Rs. 500,000/- of the taxable income - 20%\r\nOn the balance of the taxable income - 24%\r\n\r\nOn dividends- 10% as a withholding tax\r\n\r\nVALUE ADDED TAX (VAT) \r\nVAT is imposed in terms of the Value Added Tax Act No. 14 of 2002 as amended.\r\nVAT Rate- 15% \r\nVAT is payable at the time of supply, on every taxable supply of goods or services, made in a taxable period, by a registered person in the course of carrying on, or carrying out, of a taxable activity by such person in Sri Lanka.\r\n\r\nThe requirement to register for the payment of VAT arises if a person’s taxable supplies in Sri Lanka exceed Rs. 3 Million per quarter or Rs. 12 Million per annum.\r\n\r\nNATION BUILDING TAX (NBT)    \r\nNBT is imposed in terms of the Nation Building Tax Act No. 9 of 2009 as amended.\r\nNBT Rate- 2% on the liable turnover as defined in the NBT Act.\r\nNBT is payable if the liable turnover is Rs. 3 Million or above in a quarter.\r\n\r\nECONOMIC SERVICE CHARGE (ESC) \r\nESC is imposed under the provisions of Economic Service Charge No. 13 of 2006 as amended.\r\nESC Rate- 0.25% \r\nESC is payable on relevant turnover i.e. the aggregate turnover for that relevant quarter of every trade, business, profession or vocation carried on or exercised by such person or partnership, as the case may be, in Sri Lanka.\r\nESC is not payable if income tax is payable. \r\nESC is payable if the relevant turnover is Rs. 50 Million or above in a quarter.', 1, '2016-06-15 16:23:44', '2016-06-16 10:31:53'),
(19, 'EMPLOYMENT LAW- PART 1 (SHOP AND OFFICE ACT)', 'The laws relating to employment in Sri Lanka are contained in several laws. Some laws that are required to be referred to regularly by employers are the Shop and Office Employees (Regulation of Employment and Remuneration) Act, the Employees Provident Fund Act and Employees Trust Fund Act, the Payment of Gratuity Act, the Wages Board Ordinance, the Budgetary Relief Allowance of Workers Act, Workman’s Compensation Act and the Termination of Employment of Workmen (Special Provisions) Act.\r\n\r\nThe Shop and Office Employees (Regulation of Employment and Remuneration) Act:\r\nThis Act applies to those employed in most Shops and Offices as defined in the law and extends to residential hotels. Certain types of employment such as caretakers and watchers, salesmen, reporters and press photographers, traveling inspectors etc., are exempted from some provisions of this Act.   \r\n			\r\nThis Act governs terms and conditions of employment of such shops and offices such as working hours, holidays, leave, overtime remuneration, maternity benefits etc. A few aspects are highlighted below. \r\n\r\nWorking hours: On any day not more than 8 hours excluding intervals.  On any week not more than 45 hours.\r\nIntervals: Where work is carried on:\r\n(i)	between 11.00 a.m. to 2.00 p.m. – 1 hour\r\n(ii)	4.00 p.m. to 6.00 p.m. – ½ hour\r\n(iii)	7.00 p.m. to 10.00 p.m. – 1 hour\r\n\r\nLeave\r\n(a) Casual leave\r\n7 days casual leave may be taken within the year itself.\r\n(b) Annual leave\r\nFirst year:If the date of commencement of employment is between,\r\n    January 1st to April 1st - 14 days \r\n    April 1st to July 1st - 10 days\r\n    July 1st to October - 7 days\r\n    October onwards only - 4 days\r\n\r\nSecond year: 14 days and out of it 7 should be consecutive days. These holidays must be taken in the year immediately succeeding.\r\n\r\nOvertime\r\nThe normal period during which any person may be employed in or about the business of an office:-\r\n(a)	on any one day shall not exceed 8 hours and\r\n(b)	In any one week shall not exceed 45 hours.\r\nOvertime in relation to any employment or work, means employment or work in excess of the normal maximum period. Overtime rates should therefore be paid after 8 hours of work a day, or after 45 hours of work a week.\r\n\r\nMaternity Benefits - Female employees are entitled to maternity leave of eighty four (84) working days on full pay after the birth of the first or second child. The 84 days leave referred to must be counted only against the working days and therefore all holidays are excluded. The quantum of leave for the birth of a subsequent child is forty two (42) working days. \r\n\r\nHOLIDAYS\r\nWeekly\r\nA week is defined in the act as the period from Saturday midnight to the midnight of the following Saturday.\r\n1 ½ days paid holidays must be allowed to office workers who work for over 28 hours (exclusive of overtime and intervals for means, etc) in any week.\r\n\r\nStatutory Holidays.\r\nThe statutory holidays are :-\r\nTamil Thai Pongal Day, National Day, Holy Prophet’s Birth Day, Day prior to Sinhala & Tamil new year, Sinhala & Tamil new year day, May Day (1st of May), Day following Vesak Full Moon Poya day, Christmas Day\r\n\r\nAll employees are entitled to a holiday on a full moon poya day that is a working day. ', 1, '2016-06-16 08:33:45', '2016-06-16 10:32:23'),
(20, 'EMPLOYMENT LAW- PART 2 (STATUTORY PAYMENTS ETC)', 'CONTRIBUTION RATES FOR EMPLOYEES PROVIDENT FUND\r\nPractically every employment in Sri Lanka is covered by the Employees Provident Fund Act, irrespective of the number of persons employed by the employer.\r\n\r\nEvery employee in a covered employment must contribute 8%, and the employer must contribute 12%, of the employee’s “Total Earnings” each month to the provident fund of the employee.\r\n\r\nCONTRIBUTION RATE FOR EMPLOYEES TRUST FUND\r\nEvery employer is required by the Employees Trust Fund Act to contribute to the Trust Fund established under the Act, a sum equivalent to 3% of the total earnings of every employee from his employment under such employer.  “Total Earnings” have the same meaning as in the Employees Provident Fund Act.\r\n\r\nCALCULATION OF GRATUITY UNDER THE PAYMENT OF GRATUITY ACT\r\nThe Payment of Gratuity Act applies to an employer employing fifteen or more persons in Sri Lanka.  \r\n\r\nAn employee is entitled to receive as gratuity, a sum equivalent to –\r\n- 1/2 month’s salary for each year of Completed Service ( as defined in the Act) computed at the rate of wage or salary last drawn by the workman, in the case of a monthly rated workman; and\r\n\r\n- in the case of any other workman, 14 days wage or salary for each year of completed service computed at the rate of wage or salary last drawn by that workman.\r\n\r\nPAY AS YOU EARN TAX \r\nAn employer is required by law to deduct from the wages of an employee the tax payable by such employee and remit the sum so deducted to the Department of Inland Revenue. The current threshold for deduction is LKR. 62,500/-. The tax table is available on http://www.ird.gov.lk/en/publications/PAYE%20Tax%20Tables/2015-2016/Table%20%E2%80%93%201/Table_1_-_Calculation.pdf \r\n\r\nWAGES BOARD ORDINANCE  \r\nThere are specific provisions that apply to certain trades due to the setting up Wages Boards for such trades under the Wages Board Ordinance and such Board deciding as they are empowered to do by that ordinance the terms and conditions including the minimum wages payable to employees in a particular trade. Such trades include the biscuit and confectionery manufacturing trade, building trade, garment manufacturing trade, engineering trade, tea export trade, security services trade etc. \r\n\r\nTHE BUDGETARY RELIEF ALLOWANCE OF WORKERS ACT \r\nThis Budgetary Relief Allowance of Workers Act which  was certified on 23rd March 2016 and is operative from 1st May 2015 retrospectively, provides for the payment of a Budgetary Relief Allowance to employees earning a wage or salary below LKR 40,000/-\r\n\r\nThe amount payable is:- \r\nLKR 1500/- (with effect from 1st May 2015 to 31st December 2015) and an additional allowance of LKR 1000/- from 1st January 2016.\r\n\r\nTherefore, the allowance payable from 1st January 2016 to an employee receiving less than LKR 40,000/- is LKR 2500/-\r\n\r\n- If the employee received an increase in the salary of less than LKR 2500/- in January 2016, the allowance is payable only up to LKR 2500/. \r\n\r\n- Different rates apply to daily paid and employees paid on a piece rate basis. Limited exemptions apply to charitable organisations and religious institutions. \r\n\r\nWORKMAN’S COMPENSATION\r\nThe Workmen’s Compensation Ordinance provides for payment of compensation by the employer to a workman who sustains personal injury in an accident that occurs out of and in the course of the workman’s employment which is a question of fact to be determined in each case.\r\n\r\nTherefore, the coverage of this law extends to all employees who have a contract, whether oral or in writing, and extends to apprentices as well. There is no exemption in relation to types of employment or salary levels. An independent contractor is not entitled to claim compensation however, the act makes provision for recovery of compensation from a principal of contractor in certain circumstances. \r\n\r\nThe compensation payable is for the results of the injury and not the injury itself. The maximum amount that can be claimed is LKR. 550,000/-\r\n\r\nIn terms of the law an employer has the option of obtaining an insurance against risks of accidents.\r\n\r\nTERMINATION OF EMPLOYMENT\r\nWhilst employees can be terminated on disciplinary grounds, the Termination of Employment of Workmen (Special Provisions) Act is applicable to non -disciplinary terminations and applies to an employer employing more than 15 employees 6 months prior to the date of termination of the employee concerned. In terms of this Act, an employee can be terminated with the consent of the employee or with the approval of the Commissioner of Labour. When approval for termination is given by the Commissioner of Labour compensation is payable in terms of the formula set out in the regulations issued under the Act.\r\n\r\n\r\n', 1, '2016-06-16 08:36:54', '2016-06-16 10:32:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `device` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `push_id` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_user_id_user_type_index` (`user_id`,`user_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=413 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `device`, `push_id`, `user_id`, `user_type`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'charlene.fisher@hotmail.com', '$2y$10$e64RURiWpp/KuWqu0vsQsOi.hXFXvQqpcwdXtpPUlYqzn/23L/0nS', 'apple', '', 1, 'App\\CorporateUser', NULL, '2016-04-17 09:51:12', '2016-05-31 16:36:35'),
(2, 'lilian84@gmail.com', '$2y$10$2nnJdEAR/MVsN.4dpKVo2OgQugsOn00X2b4YkpSutheCT4FfiTYNm', 'android', '', 2, 'App\\CorporateUser', NULL, '2016-04-17 09:51:12', '2016-04-17 09:51:12'),
(3, 'daija.king@pfannerstill.com', '$2y$10$YqoLJyoyKcmvmxAIrBVuiO45loeuUQ5wM5533qws84f38dxFY/hBm', 'android', '', 3, 'App\\CorporateUser', NULL, '2016-04-17 09:51:13', '2016-04-17 09:51:13'),
(4, 'lenny.hegmann@gmail.com', '$2y$10$82mcsqD9em3TQ893pdEHV.KJrLcXZ2qvwekUrDg6OY00WFpAmlJUO', 'android', '', 4, 'App\\CorporateUser', NULL, '2016-04-17 09:51:13', '2016-04-17 09:51:13'),
(5, 'vweissnat@yahoo.com', '$2y$10$0bSTqDIkobTdFlrynbIV7O/NemlQ61vjWYKP3NrTVg8BlbpT6XX/C', 'android', '', 5, 'App\\CorporateUser', NULL, '2016-04-17 09:51:14', '2016-04-17 09:51:14'),
(6, 'uhilll@gmail.com', '$2y$10$oth0DAjPZ0TDJDQfiqpCJ.3GXesfcDS2VQm427HYkVmRpz9tXWAt.', 'android', '', 6, 'App\\CorporateUser', NULL, '2016-04-17 09:51:14', '2016-04-17 09:51:14'),
(7, 'stacy.cormier@hotmail.com', '$2y$10$2S.FTubSK3pKSm7lcUwmMuyEiP4.2FbLqANAJE/Zr2QxWb/VCGEz2', 'android', '', 7, 'App\\CorporateUser', NULL, '2016-04-17 09:51:14', '2016-04-17 09:51:15'),
(8, 'von.candido@hotmail.com', '$2y$10$8I9sWmootJS.2izn97r1x.MQyBnKbAyWNb4nLuyP7EpCVC3OmhJFa', 'android', '', 8, 'App\\CorporateUser', NULL, '2016-04-17 09:51:15', '2016-04-17 09:51:15'),
(9, 'mraz.claude@gmail.com', '$2y$10$mbCGgJuYpJ7xPlrTXxHsLuc8.NavN5mU83OpHgmVmT7FDLzIG8yaa', 'android', '', 9, 'App\\CorporateUser', NULL, '2016-04-17 09:51:15', '2016-04-17 09:51:16'),
(10, 'demo@gmail.com', '$2y$10$md1WBdY/Jppg3UF.JeP59uJBjVO6Bg7Djlvahm9B/uyO3ipn9gvrS', 'Android', '', 10, 'App\\CorporateUser', NULL, '2016-04-17 09:51:16', '2016-06-04 03:15:24'),
(11, 'towne.reilly@gmail.com', '$2y$10$g9taL8T423u6rYmcBuiHzOSckApsiEd7EK3e1.AZDcdN9cFJEGlzK', 'android', '', 11, 'App\\CorporateUser', NULL, '2016-04-17 09:51:16', '2016-04-17 09:51:16'),
(12, 'kerluke.niko@hudson.com', '$2y$10$mtTSAByM3.Y8bB60H/8.FeZRazunl/VLl/fM5FAmS3vY1f.TOnrtq', 'android', '', 12, 'App\\CorporateUser', NULL, '2016-04-17 09:51:17', '2016-04-17 09:51:17'),
(13, 'porter39@yahoo.com', '$2y$10$UrKU3YgHlpNpGUEVUDbxouv9XKi1.puRL.PcwrNwzA5ihnknYcm0a', 'android', '', 13, 'App\\CorporateUser', NULL, '2016-04-17 09:51:17', '2016-04-17 09:51:17'),
(14, 'paucek.maud@hotmail.com', '$2y$10$TM95yvcB0Bi3kaeUN3238OkDUpHgvYJ.L9QupJM91PVmBd7N/TyC.', 'android', '', 14, 'App\\CorporateUser', NULL, '2016-04-17 09:51:18', '2016-04-17 09:51:18'),
(15, 'miracle.prosacco@gmail.com', '$2y$10$3Yh.2vyPSnGhMphliZIvaev0i6PNUf9rSrX4DkJA3VNf4n4Jovfgu', 'android', '', 15, 'App\\CorporateUser', NULL, '2016-04-17 09:51:18', '2016-04-17 09:51:18'),
(44, 'user1@gmail.com', '$2y$10$KkT8u5AzVzeLsq0fff0/xewoS2b4EFcoz7RXCgOlalmpQ2wXCG.C.', '', '', 29, 'App\\CorporateUser', NULL, '2016-05-30 13:20:14', '2016-05-30 13:20:14'),
(50, 'friendtho2013@gmail.com', '$2y$10$o/SnJvZzsKq0pqu8IYnJquHoRJELLSVxwiRFBPxhIix90MncV0186', '', '', 31, 'App\\CorporateUser', NULL, '2016-05-31 12:55:37', '2016-05-31 12:55:37'),
(60, 'dhanuuom@gmail.com', '$2y$10$EY1kNC1.8bG5EbZ2jjXrVOuOA0oN319EoAkkKOrWnf8UEmqkPEI9.', 'Android', 'APA91bEsYjA9FSfOdFXTFzdCqFKIeikpZfE0vyKwd9h-gWMBTCoTPHU75RfrW4VFz84UzI_Fp9NQV7VnTQS2iTB_K8NAkZ8WdVH9l_Th2RJNGj__hEgtKUF6JAA7zw71tBkSEwgyDlIB', 35, 'App\\CorporateUser', NULL, '2016-06-02 14:08:48', '2016-06-02 14:24:05'),
(61, 'madushika1@gmail.com', '$2y$10$LkFKD.Oz7mzuOW/8dexbU.v11MU.oUdCRrdaw7VfeQrz3bX9Moue6', 'Android', 'APA91bHG7onboo59CBC83SJoBWRpk_ScJNG9-vHOTBwG68sA56u6M_iCo9RMEA-b3NzCtoFBq5y9k857y4-suU4EgGixDTtANtDggkOgI7q18RwfKQLixR_UZavbdmcOJdlUNRor9N2F', 36, 'App\\CorporateUser', NULL, '2016-06-02 14:13:04', '2016-06-05 23:59:02'),
(65, 'mafaz@wearedesigners.net', '$2y$10$rnpKljPG9wfAYEM1Q5O9aupZN/y.rHnKt8xM4U388/S4nIUVAY.SK', 'Android', 'APA91bFUC31aeXEWBsaCtPFHA5iPOda3L3EIyW6x-iI4R7U_YeAOlwciDp15rt93GfNwTig9znJBx14v4PSOYn6Zc8sVlIhBwYTp8qxJEr6HLLTpNJaqqo4cc-uhKZHpVkC1sEDD2jGZ', 38, 'App\\CorporateUser', NULL, '2016-06-06 01:28:52', '2016-06-30 03:21:03'),
(349, 'ayomi.aluwihare@fjgdesaram.com', '$2y$10$ndZklJpH48L/JV0ayAnXMey5ICbiXhpOSVyNE/JH5qAnbjWb76TQG', 'Android', '', 115, 'App\\FirmUser', NULL, '2016-06-15 07:54:56', '2016-06-16 10:10:13'),
(350, 'praveeni.algama@fjgdesaram.com', '$2y$10$.tcYEJMXM6lIOz6iaDNDo.dlBCYJcneltb1k6ZnazmZ7l0cz2bAs.', 'apple', 'f325acad5170fd663bd42e91a6f521c6dd3d7e53475fee931448df8685710c5c', 116, 'App\\FirmUser', NULL, '2016-06-15 07:54:57', '2016-06-22 10:28:56'),
(351, 'buwaneka.basnayake@fjgdesaram.com', '$2y$10$9pMXG4rrvpUY1Gge0o.gIeMZzc5/1POxO7At3bRLJTumVKSSEa4bS', 'Android', 'APA91bGxXp9j5wRt9kow85-kU347U_XZnawZZsGTuVq5ApshZhWwPh3dbyJNUMYYM7BtiZTm6e5j3UtTD8ZtXNAyX848o0RIqTXDLkEYjuIIOFjZYn122AEuSkRA7W6k8zhYWj37Goz6', 117, 'App\\FirmUser', NULL, '2016-06-15 07:54:57', '2016-06-24 06:49:43'),
(352, 'chamari.desilva@fjgdesaram.com', '$2y$10$6d6JUx5eZdaNaDj0ftwLWOIe5kKgDhNbbz6BJLgvcLQJqM5ZTyPaO', '', '', 118, 'App\\FirmUser', NULL, '2016-06-15 07:54:58', '2016-06-15 23:37:27'),
(353, 'nimesha.desilva@fjgdesaram.com', '$2y$10$ZT8eO0nUY5INUKrbd6sfT.rndjKVzXjLCwoEKIOLgf8vR8W/T4o5q', 'Android', 'APA91bF7yovqygJ773kfggM2kx4g5-9b6F3moWumdddq0y9ztznZEIc5605KM12YndIbGsKz9Iuzu4xAT3JEBzFsWe6PZLE5XeOMggvBhwCv5kuwTHmIGUcDnv86XjpuSdobJJ2LaSFJ', 119, 'App\\FirmUser', NULL, '2016-06-15 07:54:58', '2016-06-16 07:36:55'),
(354, 'raneesha.dealwis@fjgdesaram.com', '$2y$10$xIcNRubR1U9NxNJcbzL02OuM48rhN5A11Vhn1McFBqEqdRkdvAPdC', 'Android', 'APA91bEvgUPybaqeFgLMomACyfSlG1eDkKlJROnU8EHyKVXFcsoYCNw3pXtwl7WAaQHVtfdo4ZhlmLqrqGtEERIfyr3kN4768x4tGODUUJhyhPhtE2kZ7Qjuq4As9f6DGNlpB4mcQx9q', 120, 'App\\FirmUser', NULL, '2016-06-15 07:54:58', '2016-06-16 07:36:57'),
(355, 'harshana.dealwis@fjgdesaram.com', '$2y$10$urvrBT/S4xX.03sqAQUQ1OS9.QQ4/gyEbnJCzoNQou3M9sWPP1yVu', '', '', 121, 'App\\FirmUser', NULL, '2016-06-15 07:54:59', '2016-06-15 23:36:46'),
(356, 'kolitha.dissanayake@fjgdesaram.com', '$2y$10$S/tHMgPiEH/w2swzVYe9qOivYdUG.ctFnr8T/x5RkLAXCCS7Yl7PC', 'Android', 'APA91bHig1lYWMo0S6gl6IKyvnICCX3NGZ6TTGqgotF79RROpyHYqJILSMp3g5QLUZAmEJz-zs-RAuoXwqlhW-X2mDxbh_ZzicIW4DlfIRXa6z_01iJjeuPBxCBXmMaVe8kHTaIi5Tve', 122, 'App\\FirmUser', NULL, '2016-06-15 07:54:59', '2016-06-17 04:44:14'),
(357, 'manjula.ellepola@fjgdesaram.com', '$2y$10$zIsvbxtlRYaukf0wWY02Suoz/tCCIbwXc5M8wNgGV4XraVj.H85PK', 'Android', 'APA91bGNA9Psz2EMGOfhnq1-1U43lKT9LsAdu4EY4Y_BoPpr-tDXAOneHhNv1nljEXPn9fcSzci33dW4uVo4lfg8EH9aW_alex_HJxiniRlqP0QpsXnzNO3f2n0fy62tOgdGg48fsJBH', 123, 'App\\FirmUser', NULL, '2016-06-15 07:55:00', '2016-06-16 09:53:13'),
(358, 'anjali.fernando@fjgdesaram.com', '$2y$10$muIzR7lN8rHdNWxuTZB7eerzgqYvXnJmRDk33V711L7YhjdrTNone', 'apple', '4ea5f9d56444a38c790242dd6800f0ef9b0792a0ed1e930b20483bd08a17e89e', 124, 'App\\FirmUser', NULL, '2016-06-15 07:55:00', '2016-06-22 10:40:32'),
(359, 'rozali.fernando@fjgdesaram.com', '$2y$10$rtnEGt.JUrHi.7hQMqp/0u8Vcfmiqgs/H2V6m9LRJ1gXL8REWEyZS', '', '', 125, 'App\\FirmUser', NULL, '2016-06-15 07:55:01', '2016-06-15 23:34:23'),
(360, 'tania.fernando@fjgdesaram.com', '$2y$10$TS0dkfOScx45hOC7A7QFaeSj4KJ5kXzx9qZfGzsZPnpkG4xrNlzRm', 'Android', 'APA91bGSvp-vuAAQbma5WafjqMhnkW3k9eUZyORjUPmXbgYsmeslT-YkcHIY_HboYHzutclNGji07fQAIitZ7tRhqhNJMOVSaTr54TQjcEheMyHCsIJbV85zjSrD3TxRO3I4RrZ08g36', 126, 'App\\FirmUser', NULL, '2016-06-15 07:55:02', '2016-06-16 09:35:18'),
(361, 'tilani.ford@fjgdesaram.com', '$2y$10$6Mq17t9a8jkg.0AAb/NvXObTytRKROpeh8DZR24igIdPD3AdsfcZO', 'Android', 'APA91bEwevK7nul02DdhffYKHkQ6udEOAcu8UUCnoCIw4HcNTlIdIThHBVaBlCt-7ov_55pZT4OcN2u2sv9VbFpN7cvGWSLxt83unZ-l7wmTZSEuKeic14gdbXJeEAw68SCLwuHDkpXe', 127, 'App\\FirmUser', NULL, '2016-06-15 07:55:02', '2016-06-16 08:39:24'),
(362, 'shanaka.gunasekara@fjgdesaram.com', '$2y$10$f5m1CERKnAUrglfGS0wEx.Qhi2ebFOHky7crmXJ0BTg1bvoESTe/.', 'apple', '', 128, 'App\\FirmUser', NULL, '2016-06-15 07:55:03', '2016-06-16 07:03:50'),
(363, 'natalie.gnaniah@fjgdesaram.com', '$2y$10$0o8u66oFcdqUtTW2A2ZOp.sWF5ahhwrxwtVQfeHAXSTy677FY/fZm', '', '', 129, 'App\\FirmUser', NULL, '2016-06-15 07:55:03', '2016-06-15 23:33:09'),
(364, 'christina.hettiarachchi@fjgdesaram.com', '$2y$10$pNthKnQZo6ijGWyrENd8u.K6v7dZqcGATl4HrhQFgkEAgWBYDPQzG', 'apple', '', 130, 'App\\FirmUser', NULL, '2016-06-15 07:55:04', '2016-06-16 09:18:06'),
(365, 'ashiq.hassim@fjgdesaram.com', '$2y$10$J41INnUC0oploP2e7Bzd8u1LaA0dweZGqxcXqzOsjEdY07kQ55SPm', 'Android', 'APA91bGIQxdCl7Cv-_pvqe1mgmiOEKP5fAfUVoA2rmalhKW1ZkeFD7UZHkSuxQUo3OYvb2c5sxJLofo1vvfLLM0zIuTiX2St2_a5oTbYZxgfM1eJzAhg3uM9tUiEOtLqTJ-afB5L-nO7', 131, 'App\\FirmUser', NULL, '2016-06-15 07:55:04', '2016-06-16 08:36:19'),
(366, 'kasuni.jayaweera@fjgdesaram.com', '$2y$10$Uyjfd/SNIZSWbzXc/K8L3.EKnBXV4S/aLvPbN0vCCHwrXVbAWOXAC', 'apple', '', 132, 'App\\FirmUser', NULL, '2016-06-15 07:55:05', '2016-06-16 09:58:36'),
(367, 'suganthie.wijayasuriya@fjgdesaram.com', '$2y$10$ddGLm83OYfVsK79l.oxC0OcPq771tbzE2FXBfffNQB2RB9R.hxqi.', 'apple', '', 133, 'App\\FirmUser', NULL, '2016-06-15 07:55:05', '2016-06-15 23:31:40'),
(368, 'ul.kadurugamuwa@fjgdesaram.com', '$2y$10$.WIulP9ZU83jakOk172JGO.l3rpcMpvnKcMLxwLL54U2/wrrPEW1W', '', '', 134, 'App\\FirmUser', NULL, '2016-06-15 07:55:06', '2016-06-15 23:31:26'),
(369, 'roshani.kobbekaduwa@fjgdesaram.com', '$2y$10$rHvyEOEnBPxvAQ6RcaXlc.0L2wK1tHqVC1c44UG18pWUAvFcAePii', 'apple', '', 135, 'App\\FirmUser', NULL, '2016-06-15 07:55:06', '2016-06-16 07:23:27'),
(370, 'jayathri.kulatilaka@fjgdesaram.com', '$2y$10$iugzN1z4Cu2cje3IWYeBaOYUh.UHoHDQrELgV.mp6H2nB/nst5sP6', 'Android', 'APA91bEYASBTs06TXQQob30VRGZz9y-zCZOsUqnQtvViB3OZuref1YRmVe1e133KiuNPX8WcbTOV6QFJa72EghRbHH3s5OzjAf9PvJ-TJ2ex7bTPy2RwxRyNUobQudrVBUIk_UMVYH9M', 136, 'App\\FirmUser', NULL, '2016-06-15 07:55:07', '2016-06-16 09:36:17'),
(371, 'roshika.kulatunga@fjgdesaram.com', '$2y$10$KcX61wnFWUHHOImFaV1.lusuJSEv90iA.lFlJyOL/2JB9axEViIT2', 'apple', '', 137, 'App\\FirmUser', NULL, '2016-06-15 07:55:07', '2016-06-16 08:36:49'),
(372, 'sankha.karunaratne@fjgdesaram.com', '$2y$10$.G1g2zi5E5dJngO3g5w.8.XEqH1fObzAsqxOVJ.VCKlPdV/RIKvBa', 'apple', '', 138, 'App\\FirmUser', NULL, '2016-06-15 07:55:08', '2016-06-16 09:41:16'),
(373, 'charana.kanankegamage@fjgdesaram.com', '$2y$10$2kbul.wT.yzHIr1bwMf7Me8OaSBZTnGc25w9bya3typKCqetlitcy', '', '', 139, 'App\\FirmUser', NULL, '2016-06-15 07:55:08', '2016-06-15 23:29:52'),
(374, 'himali.mudadeniya@fjgdesaram.com', '$2y$10$/IHBcyZoRrvnX.C8nrA.GecGfGzbZj17.9.XFLf4skONd7ZpMlJ8q', '', '', 140, 'App\\FirmUser', NULL, '2016-06-15 07:55:09', '2016-06-15 23:29:38'),
(375, 'shamil.mueen@fjgdesaram.com', '$2y$10$/o4wZraQmr7YthOC8zgF6eKt4Rdok6VDA.An3vbBoNzEIixaQGVp6', 'apple', 'sdn34ifn3eu9rnfvwrvwruinwedjkcvnwrijvn', 141, 'App\\FirmUser', NULL, '2016-06-15 07:55:09', '2016-06-24 06:39:59'),
(376, 'pavithra.navarathne@fjgdesaram.com', '$2y$10$6r3p17OzmAziw3M3OR5qnuT1tEDWkCWfgeiKgVzWo58ebGcM5YdpG', 'Android', 'APA91bFKu8NqPFYR2CxVrQRmc7FXBgzKjkBWJtEcEoD2MMh_FED_o6E_ODw2kZmL-eKvTO5n6McJWMtSB7gN9pY4w1jgBUXAFpRFj1oLD9zruLUQ_9MEabVIQBVmaB90Nqi08jPI2l6i', 142, 'App\\FirmUser', NULL, '2016-06-15 07:55:10', '2016-06-16 07:55:59'),
(377, 'shehani.nayagam@fjgdesaram.com', '$2y$10$JHTz6VAPUf0GJUY4b6qt1uuAQ.9ByVdkz/rWeqHqrTI47hXRv6.Ae', 'Android', 'APA91bF5QX9_XaTHTmgKL41g-mQn9iwAT4EueN2xmP-1zPqXlJXte6UZydYXAE8oaPGOlqyR-bJ6yKrlJrZrsJmhf0JIOgCF5cRS0WysfPymno6Av80lzYPAf6W31QOurmvkGxN_t4DX', 143, 'App\\FirmUser', NULL, '2016-06-15 07:55:10', '2016-06-16 08:55:09'),
(378, 'shevanthi.perera@fjgdesaram.com', '$2y$10$ZL17DMC.loKg27TTr1WG3.oXllEQWuQ2K7tdv8ECHCeskvmTwyYmC', '', '', 144, 'App\\FirmUser', NULL, '2016-06-15 07:55:11', '2016-06-15 23:28:25'),
(379, 'udeesha.perera@fjgdesaram.com', '$2y$10$LPOuhfh46PhX6uFSjeugi.3G/XOcEyjktbdnVP4jJPXl89KsOOfzG', 'Android', 'APA91bF0kjCM82NTQ4ZpXibMfVV66WQAcVHhDcq8KwJK2L6LGRzPKJFN4E-dXaKxg_tm0ITZfq4vs5899BYK47mzu5XiH1zsB9nfVP27wpY5nkPhZry5AhBh5mC2Ln2I7MKcApqtzN15', 145, 'App\\FirmUser', NULL, '2016-06-15 07:55:11', '2016-06-16 10:17:41'),
(380, 'amali.peiris@fjgdesaram.com', '$2y$10$Gv6BLS5ogS6F4hjyBt3sg.taOfZYqugHwJ5N2ubN9Cm4V/xkVtNwa', '', '', 146, 'App\\FirmUser', NULL, '2016-06-15 07:55:12', '2016-06-15 23:27:26'),
(381, 'avindra.rodrigo@fjgdesaram.com', '$2y$10$oRLKPTmjnTQE1Jxz67Avfu4Rr/kr8UC3esIVAMSAPTFf0Z7b7R382', '', '', 147, 'App\\FirmUser', NULL, '2016-06-15 07:55:12', '2016-06-15 23:27:06'),
(382, 'aruna.desilva@fjgdesaram.com', '$2y$10$mSbh0i6mhpxAAE/vODduueLI6nHfPCtYOF1K0mdPiilMS7EfRO.Ua', '', '', 148, 'App\\FirmUser', NULL, '2016-06-15 07:55:13', '2016-06-15 23:26:50'),
(383, 'medhya.samarasinghe@fjgdesaram.com', '$2y$10$9pyNXAbbv9ZUIh1eCNhfLe0.sZlQL6jTJSKvh6oWUEvMqoIOtfM6.', '', '', 149, 'App\\FirmUser', NULL, '2016-06-15 07:55:13', '2016-06-15 23:26:28'),
(384, 'upekha.senaratne@fjgdesaram.com', '$2y$10$wWCtwiKVlgw7oAN8prol7u4VoufNpX30s7GOIxjiUuH.jPm.8v7LW', '', '', 150, 'App\\FirmUser', NULL, '2016-06-15 07:55:14', '2016-06-15 23:26:08'),
(385, 'patali.thrimanne@fjgdesaram.com', '$2y$10$krSXakWZix8xn2jxeeYDaewfLqyf7TiC9OX3LqMqdR5cL8AWy6rYq', '', '', 151, 'App\\FirmUser', NULL, '2016-06-15 07:55:14', '2016-06-15 23:25:48'),
(386, 'dinukshi.thalgahagoda@fjgdesaram.com', '$2y$10$5Se5IW2zE.9xXKn2r7wGfO3PRZTM927ETW.SoDLz2DvWtJiSKdSe6', 'apple', '', 152, 'App\\FirmUser', NULL, '2016-06-15 07:55:15', '2016-06-16 11:23:51'),
(387, 'ananda.tikiriratne@fjgdesaram.com', '$2y$10$DctMsCP3rp2JEJV8t9/Og.joAVNPIYFpAWJrfxdgJSiQDKE4zaFDu', '', '', 153, 'App\\FirmUser', NULL, '2016-06-15 07:55:15', '2016-06-15 23:25:01'),
(388, 'shehara.varia@fjgdesaram.com', '$2y$10$LxRvU53.4ZAopYszHRgz.ublzup9W7Pmxux.pghofgH4AbCqOKxW6', 'Android', '', 154, 'App\\FirmUser', NULL, '2016-06-15 07:55:16', '2016-06-16 13:02:31'),
(389, 'rosy.vanlangenberg@fjgdesaram.com', '$2y$10$b312oW/Of.7QxPGP5dm1WuJjMNaqmLddXIIYtR3jKuy/f1ByoOLFq', 'Android', 'APA91bGO_1SPdLLcyBu0ZWrLjEX1C0SIfLAE01eELqUhauGZkBAuXh_V5UmhupYjEsYA01l9Tlru3CAahrOuMTm1LXgNNJ4c5DFTrR2Wt51x7sK2EqOyr0Wa_hVoFp6UMXvew7bhUmHP', 155, 'App\\FirmUser', NULL, '2016-06-15 07:55:16', '2016-06-16 09:43:34'),
(390, 'anuradha.wijesooriya@fjgdesaram.com', '$2y$10$pCd8pPgME06r/PsKIgx4MOxlnjyRcNW5ZEVq1P4JDFRtpvn2wvHwK', '', '', 156, 'App\\FirmUser', NULL, '2016-06-15 07:55:16', '2016-06-15 23:24:06'),
(391, 'ramesh.perera@fjgdesaram.com', '$2y$10$iilApPzl./bj7YvSToTgIOeVSASkl0hHwWcXT6UWs3xievVRNq5mi', 'Android', 'APA91bHR-a36U0fJDYzIOQaeoRAOlrsD26Mxjip6aNdbLIoGA19UAXB3mwJFjgqHdOuGmsSRJ_ETiz4OsprarOCl_vnv3YF7NzpNcev-MyIWCVYU1l6Q24ISMEmv7v-VwDTiyr5AYm-w', 157, 'App\\FirmUser', NULL, '2016-06-15 07:55:17', '2016-06-19 06:45:36'),
(392, 'deepthi.fernando@fjgdesaram.com', '$2y$10$EuIJEWL71aTl0N62hWhbru0.si4ng9n/oLGpe70v9UwUwFyPfs30C', '', '', 158, 'App\\FirmUser', NULL, '2016-06-15 07:55:17', '2016-06-15 23:38:24'),
(393, 'shehara.gunasekara@fjgdesaram.com', '$2y$10$Ocda1ygW0X6lvA.kVF7frOqjIXi5WVkdyNLdCedY.sKSH6pA7piES', 'Android', 'APA91bGirDy5WsueRpF_yKcnaIQcCm8wh-wx2qCBRQGY662kErgz5eedqV_cRiZOwqGNa20yl8WVMs68DgRQ0DbxIqPXMdOygs0rcN8zaNPnz5-4NPEg7AV-X8Bv0ElQ4SScWjkholHh', 159, 'App\\FirmUser', NULL, '2016-06-15 07:55:18', '2016-06-16 09:03:57'),
(394, 'anushika.abeywickrama@fjgdesaram.com', '$2y$10$9e4yV/kVPCUsKQELmhb.We3KYR.nAXTlfRAd9gfq9LtIhUz3CHviG', 'Android', 'APA91bFcFq1Un8eBTCgW-Xx5DTV4bqaPQw44wj9mnd7UeeImnqecR5QEFOdvI2H7_SKsj9iRRepm5pfNbCTxL7kOjSUXMvbRLNoo-UK_m190dlC0CvDgOeiyakITxlDgkCLb751iSioV', 160, 'App\\FirmUser', NULL, '2016-06-15 07:55:18', '2016-06-16 09:02:32'),
(395, 'oshani.wijewardena@fjgdesaram.com', '$2y$10$2o.ujDiVScJSWby155gwC.PYUDtb/5hpJIDwAdJ3yNd/8CrnHAwnW', 'Android', 'APA91bG2fqFqWciAFGCTSnaFoiLoEhSvljdw3XHN271nYkGbSeiqU7j_NPBsfFK0um3DQSNojZ54-QTsQLWycHFGApvjmzJIdTB4uQVOd5jsSEoYA6iUxcxl3ZoSJpcnDQ_7LuC6wFc_', 161, 'App\\FirmUser', NULL, '2016-06-15 07:55:19', '2016-06-16 08:29:38'),
(396, 'harshini.serasinghe@fjgdesaram.com', '$2y$10$LKrtOr.FpGZwfGPVXppVPuEuRgDfe9tt0w7Jo46kn5VOmnh.j7.HC', 'Android', 'APA91bFg9vz1aCpChCwFw5OHY1xmOuu9Qh0CV1pNCVQQT32X2QVFAjJK8DwGQl5NRFWm-pqAG6fBLw9eNC75OY6t_29YkjZtsXArvwYYObBOVhrXAW3NOSiN8jc4Ak9VfgRir_03SWnU', 162, 'App\\FirmUser', NULL, '2016-06-15 07:55:19', '2016-06-16 21:21:37'),
(397, 'thilini.samarawickrama@fjgdesaram.com', '$2y$10$sDQK4A01eAtAohvAMydRR.GDoISP6zpA5bVwVGMiKKKm.fP2T17LO', '', '', 163, 'App\\FirmUser', NULL, '2016-06-15 07:55:20', '2016-06-15 23:22:00'),
(398, 'tharushi.perera@fjgdesaram.com', '', 'Android', 'APA91bGjl6eInl0ga__rTWYgPOOgH5HB-Dr-VijDxjE7-mNKbjnfvUoiFdHJ393YrdoR17vdSV-zqzgZfC57GbdLI6V_zm_xiYJwg_NUzS2M7SZPXfSTHU-5m2sGV4azC09iKBVDy1wm', 164, 'App\\FirmUser', NULL, '2016-06-15 07:55:20', '2016-06-16 09:10:38'),
(399, '175@fjgdirect.com', '$2y$10$mZqVnrSruZz4JL55wGxhBOVOK3NNPNZa2wkBMt46POPCpTaoZOY8W', 'Android', 'APA91bFUC31aeXEWBsaCtPFHA5iPOda3L3EIyW6x-iI4R7U_YeAOlwciDp15rt93GfNwTig9znJBx14v4PSOYn6Zc8sVlIhBwYTp8qxJEr6HLLTpNJaqqo4cc-uhKZHpVkC1sEDD2jGZ', 39, 'App\\CorporateUser', NULL, '2016-06-15 11:02:44', '2016-06-17 03:37:36'),
(400, 'hettiarachchism@gmail.com', '$2y$10$IGSkSPlhz.U4hwGJJObfJ.YbVcB1jYU8Z.mhZOw9/aYtphYUZ86Xq', 'apple', '4296fdffc85656df09e99a99e3db233fe77507d308ddfe166d7177b886361f41', 40, 'App\\CorporateUser', NULL, '2016-06-15 11:45:30', '2016-06-22 08:36:50'),
(401, 'trinesh.fernando@dialog.lk', '$2y$10$fWn6fN4SbDXkKG9MwzeXleeXCiTbzQdY/A7QFlsILiefdk9LtRzw6', 'Android', 'APA91bFUC31aeXEWBsaCtPFHA5iPOda3L3EIyW6x-iI4R7U_YeAOlwciDp15rt93GfNwTig9znJBx14v4PSOYn6Zc8sVlIhBwYTp8qxJEr6HLLTpNJaqqo4cc-uhKZHpVkC1sEDD2jGZ', 41, 'App\\CorporateUser', NULL, '2016-06-17 04:13:36', '2016-06-17 04:14:20'),
(402, 'Bkealey@microsoft.com', '$2y$10$qRUB6Sr0KoSZBpetw3QccOMxDEB0kVOtoCAY5ctjZ1MPaguXWo7.C', 'Android', 'APA91bFljGD5fG1BLVT54jOEskuVSIViso22vOHRGPqgvf3q7ax97PNyTxMRP94ptSPuX8LV45v_-wXiiv0ASBxxF756m4CW2W9obh2ok7WgDVDUBE7RUycWssnfcsSV3EO216n0QcUQ', 42, 'App\\CorporateUser', NULL, '2016-06-17 04:16:12', '2016-06-17 07:28:45'),
(403, 'chandanika.jayawickrema@hayleysadvantis.com', '$2y$10$ZNpZYtX/kYrKDU5AD/hX6usTyGxxcBERsg.MeAzUoBeedbKYGfh0m', 'Android', 'APA91bH4JQjqhG8bqUXw2wE_WqXADtqTUU8_LqM61LT08WJnMLd55JY8l_Cf5htFIB5kLxxY6gluL3aopAi9ILxSy7ZYQitxxeTCg4MHmR0NezIVpJRFRUQMdIc8iNxZtmmVJZbaD13j', 43, 'App\\CorporateUser', NULL, '2016-06-17 04:17:47', '2016-06-17 07:36:00'),
(404, 'ravidila@gmail.com', '$2y$10$XFvi6MuKdD4dzqpmzYsfMOlzylUuqt122i9lS6T9RjO1UVXo4BWyG', 'apple', '', 44, 'App\\CorporateUser', NULL, '2016-06-17 04:18:35', '2016-06-17 14:43:06'),
(405, 'charmaine.tillekeratne@lk.pwc.com', '$2y$10$.N4gybY9MnO3uUTnL5bkDuT6BFio8UL1u07fQ6dnAjxnNW6gwupKS', 'Android', 'APA91bHr3WA0uAXaR2m3ENJrVE3pok_64jvVaXLRHNQVIBMRw-DpkJyocRaWX9OAPWgbMa_OFsSVbzhXol2tulDLjgDXOIiC4WjDXQjWysA-CaZLkf-cFHUZwT8mbKY7TLEaa2M7_8Xx', 45, 'App\\CorporateUser', NULL, '2016-06-17 04:19:40', '2016-06-23 10:08:16'),
(406, 'sanjeewa@jetwinghotels.com', '$2y$10$hB3rbQ7gtBuB9At/2D3jv.98vdOWbA1oaVyD4.yx3m4uTcjneFgnq', 'apple', '7841a0d9c4f527244f928575265470441b95695fb81f89d0bf2deec5f1f2619e', 46, 'App\\CorporateUser', NULL, '2016-06-20 05:10:41', '2016-06-21 07:02:25'),
(407, 'Md@expressnewspapers.lk', '$2y$10$msklB82TYnAtdExm98VtXeTloYfS0xsCLWLWIA5xDN1xtMmjmPVqm', '', '', 47, 'App\\CorporateUser', NULL, '2016-06-20 05:11:45', '2016-06-20 05:11:45'),
(408, 'shazil.ismail@gmail.com', '$2y$10$odotwkYeZNUMCSYi3Uc8l.XRJxCNbV/sGGu9xtHqxj9Iy7Ap0Emue', '', '', 48, 'App\\CorporateUser', NULL, '2016-06-22 04:46:13', '2016-06-22 04:46:13'),
(409, 'shihani@acuitystockbrokers.com', '$2y$10$Yw5yVJ8OJbctBcJDoUHAGOjWWB1UdcoKfQFZIByaZ/ZA.PQCIRHeu', 'Android', 'APA91bH280cb7Xsey3rmkPs-dwNga-7KBIr1aQOZKkTbdZM4-eS53sj7TUh4z6DX44wQlPwHONBdcxzhwGhO9kcRV3S1ET5UIMiawnLf6zOLZLUfPHbEFTAiNgr5zWeEdAiRIXdm6Prz', 49, 'App\\CorporateUser', NULL, '2016-06-22 04:47:05', '2016-06-22 10:02:34'),
(410, 'priyanthig@brandix.com', '$2y$10$ar0s4sLrXGDHJNUh4XieBelVNfCt7I8IrQwcyA3OtN3XbOxMmXMfa', '', '', 50, 'App\\CorporateUser', NULL, '2016-06-27 04:10:24', '2016-06-27 05:14:39'),
(411, 'sudesh_peter@bat.com', '$2y$10$5zGvx4I53aQI.oXG12eDo.ucZFyBjHvbm/Bjb/hd7/0NBnJkpki1i', '', '', 51, 'App\\CorporateUser', NULL, '2016-06-27 04:11:07', '2016-06-27 04:11:07'),
(412, 'Ceo@amt.lk', '$2y$10$sTSL7kvgnjNsqrWuJaDhneBK8/Tqy2cVdalzeadFzQu3c7rWzbc5C', '', '', 52, 'App\\CorporateUser', NULL, '2016-06-30 04:55:16', '2016-06-30 04:55:16');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment_request_users`
--
ALTER TABLE `appointment_request_users`
  ADD CONSTRAINT `appointment_request_users_appointment_request_id_foreign` FOREIGN KEY (`appointment_request_id`) REFERENCES `appointment_requests` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `appointment_request_users_lawyer_id_foreign` FOREIGN KEY (`lawyer_id`) REFERENCES `lawyers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `company_setup_directors`
--
ALTER TABLE `company_setup_directors`
  ADD CONSTRAINT `company_setup_directors_company_setup_id_foreign` FOREIGN KEY (`company_setup_id`) REFERENCES `company_setups` (`id`);

--
-- Constraints for table `company_setup_shareholders`
--
ALTER TABLE `company_setup_shareholders`
  ADD CONSTRAINT `company_setup_shareholders_company_setup_id_foreign` FOREIGN KEY (`company_setup_id`) REFERENCES `company_setups` (`id`);

--
-- Constraints for table `lawyers`
--
ALTER TABLE `lawyers`
  ADD CONSTRAINT `lawyers_division_id_foreign` FOREIGN KEY (`division_id`) REFERENCES `lawyer_divisions` (`id`);

--
-- Constraints for table `legal_alerts`
--
ALTER TABLE `legal_alerts`
  ADD CONSTRAINT `legal_alerts_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `legal_alert_types` (`id`);

--
-- Constraints for table `legal_matters`
--
ALTER TABLE `legal_matters`
  ADD CONSTRAINT `legal_matters_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `legal_matter_types` (`id`);

--
-- Constraints for table `legal_matter_notes`
--
ALTER TABLE `legal_matter_notes`
  ADD CONSTRAINT `legal_matter_notes_legal_matter_id_foreign` FOREIGN KEY (`legal_matter_id`) REFERENCES `legal_matters` (`id`);

--
-- Constraints for table `legal_matter_users`
--
ALTER TABLE `legal_matter_users`
  ADD CONSTRAINT `legal_matter_users_legal_matter_id_foreign` FOREIGN KEY (`legal_matter_id`) REFERENCES `legal_matters` (`id`),
  ADD CONSTRAINT `legal_matter_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `password_recovery_requests`
--
ALTER TABLE `password_recovery_requests`
  ADD CONSTRAINT `password_recovery_requests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
