myApp.controller('lawyerCtrl', ['$scope','$http','$filter', function ($scope,$http, $filter) {
   

    $scope.rowCollection = [];
    $http.get("lawyer/lawyers-all").success(function(response, status) {
        angular.forEach(response, function(data, key) {
            var load_data = {
                id: data.id,
                full_name: data.full_name,
                designation: data.designation,
                division: data.division.division,
                head: data.head,
                status: data.status
            }
            $scope.rowCollection.push(load_data);

        });
    });
    
    
}]);