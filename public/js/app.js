var myApp = angular.module('myApp',['smart-table','ngMessages','angularUtils.directives.dirPagination','ngAnimate', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});