myApp.controller('matterCtrl', ['$scope','$http', function ($scope,$http) {
   
   
    $scope.itemsByPage=5;

    $scope.rowCollection = [];
    $http.get("matter/requests-all").success(function(response, status) {
   
        angular.forEach(response, function(data, key) {
            var load_data = {
                id:data.id,
                name: data.name,
               // type: data.type.type,
                next_date: data.next_date,
                updated_date: data.updated_date,
                
                //status: data.status
            }
//alert(load_data);
            $scope.rowCollection.push(load_data);
        });
    });
}]);