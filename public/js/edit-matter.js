myApp.controller('editmatterCtrl', ['$scope', '$http', '$location', function($scope, $http, $location) {

    $scope.close = function() {
        $http.post("close").success(function(response) {
            console.log(response);
           var url = window.location.href;
          window.location.href = url.substr(0,url.indexOf('/web')) + '/web/matter-management';
      
            // scope.accounts = response;
        });

    }


    $scope.rowCollection4 = [];
    $scope.rowCollection = [];

    $scope.init = function() {

        $http.post("../request-lawyers-all", {}).success(function(response, status) {
            var data = {
                'lead_lawyer_id': $scope.data.lead_lawyer_id
            };

            angular.forEach(response.lawyers, function(data, key) {
                var load_data = {
                    id: data.id,
                    name: data.name,
                }
                var index = -1;

                for (var i = 0; i < $scope.data.lawyers.length; i++) {
                    if ($scope.data.lawyers[i].id == data.id) {
                        index = i;
                    }
                }
                if (index == -1) {
                    $scope.rowCollection4.push(load_data);
                }

            });

            console.info("load_data_lawyer", "OK");



        });
    }


    $scope.matter_id = -1;
    $scope.data = {
        name: "",
        type_id: 0,
        lead_lawyer_id: 0,
        represent_company: "",
        closed: false,
        clients: [],
        lawyers: []
    };

    $scope.initID = function(id) {
        $scope.matter_id = id;
        $http.get("getMetter").success(function(response, status) {
            response.data.lead_lawyer_id = response.data.lead_lawyer_id + "";

            $scope.data = response.data;
            console.info("set response", "OK");
            $scope.init();
            $scope.leaveChange();
        });
        console.log("matter_id = " + id);
    }



    setTimeout(function($location) {
        url = window.location.href.split("/");
        url.pop();
        $scope.initID(url.pop());
    }, 5);

    $scope.lawyerName = function() {
        if ($scope.nameselect) {
            $scope.data.clients.push($scope.nameselect);
            var index = $scope.rowCollection5.indexOf($scope.nameselect);
            $scope.rowCollection5.splice(index, 1);


        }
    }

    $scope.usersSelect = function() {
        if ($scope.selectlawyer) {

            $scope.data.lawyers.push($scope.selectlawyer);

            var index = $scope.rowCollection4.indexOf($scope.selectlawyer);
            $scope.rowCollection4.splice(index, 1);


        }
        //  alert($scope.selectlawyer);
        //alert($scope.nameselect);
        console.log($scope.selectlawyer);
        //   $scope.nameselect='';


    }

    $scope.leaveChange = function() {
        // alert($scope.represent_company);
     //  $scope.data.clients=[];
        var data = {
            'represent_company': $scope.data.represent_company
        };

        $scope.rowCollection5 = [];
        $http.post("../request-users-all", data).success(function(response, status) {
             $scope.rowCollection5 = [];
            angular.forEach(response.users, function(data, key) {
                var load_data = {
                    id: data.id,
                    name: data.name,
                }


                    $scope.rowCollection5.push(load_data);
               
            });

        });
    }


    $scope.removelawyer = function removelawyer(row) {
        var index = $scope.data.lawyers.indexOf(row);
        if (index !== -1) {
            $scope.rowCollection4.push(row);
            $scope.data.lawyers.splice(index, 1);
        }
    };
    $scope.removeItem1 = function removeItem1(row1) {
        var index = $scope.data.clients.indexOf(row1);
        if (index !== -1) {
            $scope.rowCollection.push(row1);
            $scope.data.clients.splice(index, 1);
        }
    };





   $scope.data= {
       represent_company:'',
        err:'',
    };
    $scope.data={
      clients:'',
    clients_err:''
    };

   $scope.data = {
        name:'',
       name_err:''
    };
    $scope.data={
        lawyers:'',
          lawyers_err:''
    }; 
 $scope.selectlawyer={
     
          err:'',
    };





    // submit form data to matterController

    $scope.submitdata = function() {


if($scope.data.name == '') {
            $scope.data.name_err = 'This field is required';
          
  }else{
    $scope.data.name_err='';
  }
    
 if($scope.data.lawyers == '') {
            $scope.data.lawyers_err = 'This field is required';
          
  }else{
       $scope.data.lawyers_err='';
  }
    
 if($scope.data.clients == '') {
            $scope.data.clients_err = 'This field is required';
            
  }else{
     $scope.data.clients_err='';
  }
    
 

        var user_ids = [];

        angular.forEach($scope.data.clients, function(client, i) {
            user_ids.push(client.id);
        });


        angular.forEach($scope.data.lawyers, function(lawyer, i) {
            user_ids.push(lawyer.id);
        });
//alert($scope.data.lawyers.length);
 if($scope.data.name!= ''
     && $scope.data.clients.length != 0
     && $scope.data.lawyers.length != 0
 
    ){       


        $http.post('updates-users', {
                id: $scope.id,
                name: $scope.data.name,
                type_id: $scope.data.type_id,
                lead_lawyer_id: $scope.data.lead_lawyer_id,
                represent_company: $scope.data.represent_company,
                user_ids: user_ids,

            }).success(function(response) {
                //  console.log(response);
               var url = window.location.href;
             window.location.href = url.substr(0,url.indexOf('/web')) + '/web/matter-management';
                // scope.accounts = response;
            })
            .error(function(response) {
                //console.log(response);
            });
}

    };

}]);