myApp.controller('accRequestCtrl', ['$scope','$http','$filter', function ($scope,$http,$filter) {
   

    $scope.itemsByPage=5;

    $scope.rowCollection = [];
    $http.get("requests-all").success(function(response, status) {

        angular.forEach(response, function(data, key) {
            var load_data = {
                id: data.id,
                name: data.name,
                designation: data.designation,
                company: data.company,
                email: data.email,
                phone: data.phone,
                status: data.status
            }
            $scope.rowCollection.push(load_data);
        });
    });
    
    $scope.declineUser = function declineUser(row) {
        $http.get("requests-declined/"+row.id).success(function(response, status) {
            //alert(response.id);
            var index = $scope.rowCollection.indexOf(row);
            $scope.rowCollection.splice(index, 1);
            var current_data = {
                id: response.id,
                name: response.name,
                designation: response.designation,
                company: response.company,
                email: response.email,
                phone: response.phone,
                status: response.status
            }
            $scope.rowCollection.push(current_data);
        });  

    };
    
    
}]);