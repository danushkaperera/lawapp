myApp.controller('loginCtrl', ['$scope','$http','$location','$window', function ($scope, $http,$location,$window) {
    $scope.email = {
        text: '',
        err:'',
    };
    $scope.password = {
        text: '',
        err:'',
    };
    $scope.login = function() {
        var credentials = {
            email: $scope.email.text,
            password:$scope.password.text,
        }
        $http.post("http://localhost:8888/lawapp/public/api/login", credentials).success(function(response, status) {
            //$location.path('/web/dashboard');
            $window.location.href = 'dashboard';
            //$scope.hello = data;
            //console.log(data);

            //var currunt_data = {
            //    id: id,
            //    index_no: index_no,
            //    year: year,
            //    subject: subject_name,
            //    grade: grade_name,
            //    app_id:app_id,
            //    inc_id: response.data.id,
            //}
            //$scope.rowCollection.push(currunt_data);
        });
    };
}]);