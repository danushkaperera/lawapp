myApp.controller('qrguidCtrl', ['$scope','$http','$filter', function ($scope,$http,$filter) {
   
   
    $scope.itemsByPage=5;

    $scope.rowCollection = [];
    $http.get("quick-reference-guides/request-all").success(function(response, status) {
    
        angular.forEach(response, function(data, key) {
            var load_data = {
                id: data.id,
                title: data.title,
                priority: data.priority,
                updated_at: data.updated_at,
               
                //status: data.status
            }
            $scope.rowCollection.push(load_data);
        });
    });
}]);