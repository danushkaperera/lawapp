<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use JWTAuth;
use Dingo\Api\Exception\ValidationHttpException;

use Illuminate\Support\Facades\Mail;
use App\User;
use App\PasswordRecoveryRequest;
use App\AccountRequest;
use PushNotification;

use App\LegalAlert;
use App\QuickReferenceGuide;
use App\LegalMatter;
use View;

use Validator;

class UserController extends BaseController
{
    
    public function __construct() 
    {
        $this->middleware('auth:api', ['except' => ['views','login','recoverPassword','requestAccount', 'testPush'] ]);
    }
    
    public function login(Request $request)
    {
        
        $account_request = AccountRequest::where(['email'=>$request->input('email'),'status'=>'pending'])->first();
        
        if($account_request != null)
        {
             return $this->response->error('account activation pending', 203);
        }
        
        if(User::where(['email'=>$request->input('email'),'status'=>'active'])->first() == null)
        {
            return $this->response->errorUnauthorized('login failed');
        }
        
        $credentials = $request->only('email', 'password');
        
        $token = Auth::guard('api')->attempt($credentials);
        
        if(!$token)
        {
            return $this->response->errorUnauthorized('login failed');
        }
        
        $user = Auth::setToken($token)->user();
        $user->update($request->only('device', 'push_id'));
        
        return $this->response->array(compact('token'));
        
    }
    
   
    public function recoverPassword(Request $request)
    {
        $user = User::where($request->only('email'))->first();
        
        if(!$user)
        {
            return $this->response->errorNotFound("can't find the user");
        }
        
        
        $company = $user->user->company == $request->input('company');
        
        if(!$company)
        {
            return $this->response->errorNotFound("can't find the user");
        }
        
        PasswordRecoveryRequest::create(['user_id'=> $user->id]);
        
        $data = "password recovery requested successfully";
        
        return $this->response->array(compact('data'));
        
    }
    
    public function getProfile()
    {
        $user = Auth::user();
        
        $data = [];
        $data['email'] = $user->email;
        $data['user_type'] = $user->user_type;
        
        $user_data = $user->user()->first();
        
        $data = array_merge($data, $user_data->toArray());
        
        return $this->response->array(compact('data'));
        
    }
    
    public function requestAccount(Request $request)
    {
        $inputs = $request->only(
            'name',	  
            'company',	 
            'email',	  
            'designation',	  
            'phone'
            );
            
            $rules = array(
                'name'           => 'required',                        
                'email'          => 'required|email|unique:account_requests|unique:users',     
                'company'        => 'required',
                'designation'    => 'required',          
                'phone'          => 'required',          
            );
            
             $validator = Validator::make($inputs, $rules);

    
            if ($validator->fails()) {
                
                //$messages = $validator->messages();
        
                throw new ValidationHttpException($validator->errors());
        
            } 
            
        AccountRequest::create($inputs);
        
        // return View('email.account_request', [ 'data' => $inputs ]);
        
        
        $data = "account requested successfully";
        
        Mail::send('email.account_request',[ 'data' => $inputs ] , function ($message) use ($request) {
            $message->from(env('APP_EMAIL'), env('APP_NAME'));
            $message->to($request->input('email'));
            $message->sender(env('APP_EMAIL'), env('APP_NAME'));
            $message->subject('FJG Direct - Request for Account');
        });
        
        
        Mail::send('email.account_request_admin',[ 'data' => $inputs ], function ($message) use ($request) {
            $message->from(env('APP_EMAIL'), env('APP_NAME'));
            $message->to(env('APP_EMAIL_ADMIN'));
            $message->sender(env('APP_EMAIL'), env('APP_NAME'));
            $message->subject('FJG Direct - Request for Account Activation');
        });
        
        
        
        return $this->response->array(compact('data'));
        
    }
    
    public function getUpdates(Request $request)
    {
        
            
        $legal_alert_datetime = $request->input('legal_alert');
        $count_legel_alert = LegalAlert::where('updated_at','>',$legal_alert_datetime)->count();
        
        
        $quick_reference = $request->input('quick_reference');
        $count_quick_reference = QuickReferenceGuide::where('updated_at','>',$quick_reference)->count();
        
        $track_matter = $request->input('track_matter');
        $count_track_matter = LegalMatter::where('updated_date','>',$track_matter)->count();
        
        $data = [];
        $data['legal_alert'] = $count_legel_alert;
        $data['quick_reference'] = $count_quick_reference;
        $data['track_matter'] = $count_track_matter;
        
        return $this->response->array(compact('data'));
        
        
    }
    
    public function getPhoneNumberVerification(Request $request)
    {
        $user = Auth::user();
         
        if($user)
        {
            $phone_verification = $user->phone_verification;
            
            $data = [];
            $data['phone_verification'] = (bool) $phone_verification;
            
            return $this->response->array(compact('data'));
            
        }    
         
         
    }
    
    public function testPush(Request $request)
    {
        
        // $push_id = "APA91bEqz-0UpI1fuRLiJUOOBfw4Mv6NzudsE5M5Jb7HFKpHxJzmwgDDLu7zAkK9pPFDiw7wJrypKAFGw01Zrsvn_FqZXphHXPxqu7sXFY-JaLP1zXnroXb-EDI7ESE8bpEBvJyAEm1q";
        
        // if($push_id)
        // {
        //     try{
                
            
        //     $collection = PushNotification::app('appNameAndroid')
        //         ->to($push_id);
        //         $collection->adapter->setAdapterParameters(['sslverifypeer' => false]);
        //         $collection->send("LawApp Test PushNotification 1");
                
        //         foreach ($collection->pushManager as $push) {
        //             $response = $push->getAdapter()->getResponse();
        //             var_export($response);
        //         }
                
        //     }
        //     catch(Exception $e)
        //     {
        //       //  return $this->response->errorBadRequest($e);
        //       dd($e->getMessage());
                
        //     }
        // }   
        
        $push_id = "c9066941deab2d0f290b81dad6a703914bc78b14fde1e880c89a669cdf4d988d";
        
        if($push_id)
        {
            try{
                
            
            $collection = PushNotification::app('appNameIOS')
                ->to($push_id);
                //$collection->adapter->setAdapterParameters(['sslverifypeer' => false]);
                $collection->send("LawApp Test PushNotification 1");
                
                foreach ($collection->pushManager as $push) {
                    $response = $push->getAdapter()->getResponse();
                    var_export($response);
                }
                
            }
            catch(Exception $e)
            {
              //  return $this->response->errorBadRequest($e);
              dd($e->getMessage());
                
            }
        }
        
    }
    
    
    public function views()
    {
        $data = [];
        
        
        $data = \App\AppointmentRequest::first()->toArray();
        
        
        return View('email.appointment_request_user',compact('data'));
    }
    
}
