<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Password Reset</h2>

<div>
    <p>Your password has been reset as you requested.</p>
   <p>Your new password is {{ $data['token'] }}</p>
    <br/>

</div>

</body>
</html>