@section('title', 'Dashboard')

  <nav class="navbar navbar-default">
        <div class="container">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('web/dashboard') }}">Home</a></li>
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                      

                           <a href="{{ url('web/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                            
                            
                        </li>
                </ul>
            </div>
        </div>
    </nav>

@section('footer')
    @parent
    {!! Html::script('js/login.js') !!}
@endsection