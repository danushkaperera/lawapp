<html ng-app="myApp">
<head>
    <title>LawApp - @yield('title')</title>
    @include('web.partials._header')
</head>
<body>
@section('sidebar')
@show

<div class="container">
    @yield('content')
</div>
</body>
@section('footer')
    @include('web.partials._footer')
@show
</html>