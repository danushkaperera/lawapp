@extends('web.layouts.master')
 @include('web.layouts.dashboard')
@section('title', 'Create Quick Reference Guide')
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@section('sidebar')
    @parent
@endsection

@section('content')
<div class="content">
<h2>Create Quick Reference Guide</h2>
 <button type="button" class="btn btn-sm btn-warning" id="backbtnqrg"  data-toggle="modal" >Go Back</button>
{{ Form::open(array('url' => 'web/quick-reference-guide')) }}
      {{ Form::hidden('priority',  \App\QuickReferenceGuide::count()+1 ) }}
       
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   {{ Form::label('title', 'Title',  ['class' => 'control-label']) }}</div>
                <div class="col-sm-9">
                     {{ Form::input('title', 'title',$qrg->title,['class' => 'form-control']) }}
                
               @if ($errors->has('title'))
                        <span class="error-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif  
                </div>
            </div>
        </div>
    </div>
   
  
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>     {{ Form::label('body', 'Notes:',  ['class' => 'control-label']) }}</div>
                <div class="col-sm-9">
                    {!! Form::textarea('body',$qrg->body, ['class' => 'form-control']) !!}
                  
               
                 @if ($errors->has('body'))
                       <span class="error-block">
                            <strong>{{ $errors->first('body') }}</strong>
                        </span>
                    @endif
 </div>
            </div>
        </div>
    </div>
  
         
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>    {{ Form::label('created_at', ' Publish Date ') }}</div>
                <div class="col-sm-9">
                  {{ Form::input('text', 'created_at', null,['id' => 'datepicker','class' => 'form-control datepicker','data-date-format'=>'y-m-d']) }}
                  
               
                   @if ($errors->has('created_at'))
                        <span class="error-block">
                            <strong>{{ $errors->first('created_at') }}</strong>
                        </span>
                    @endif </div>
            </div>
        </div>
    </div>
  
  
   
  <button type="submit" class="btn btn-primary">Submit</button>
{!! Form::close() !!}

</div>
  <script>
  $('.datepicker').datepicker({
    dateFormat: 'yy-mm-dd',
   
});
  </script>


@endsection
@section('footer')
    @parent
  
@endsection