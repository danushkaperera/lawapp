@extends('web.layouts.master')
 @include('web.layouts.dashboard')
@section('title', 'Matter Manager')
 @include('web.layouts.loading')
@section('sidebar')
    @parent
@endsection

@section('content')

 <div class="row">
        <div class="col-sm-12">            
                <div class="col-sm-9">
                   <h2>Matter Manager </h2>  
                   <button type="button" class="btn btn-sm btn-warning" id="btnhome"  data-toggle="modal" >Go Back</button>
                </div>
                <div class="col-sm-3">
                  </br>
                  <a href="{{url('web/matter-management/create')}}" class="btn btn-primary">Create Matter</a>
                </div>
         </div>
</div>







    {!! Form::open(array('url' => 'foo/bar')) !!}
    <div ng-controller="matterCtrl">
        <table st-table="rowCollection" class="table table-striped">
            <thead>
            <tr>
                <th st-sort="name">Matter Name</th>
                <th st-sort="type_id">Type</th>
                <th st-sort="next_date">Next Hearing Date</th>
                <th st-sort="updated_date">Last Update</th>
                <th st-sort="#"></th>
            </tr>
       
            <tr>
                <th>
                    <input st-search="'title'" ng-model="search"placeholder="search by matter name" class="input-sm form-control" type="search"/>
                </th>
               
            </tr>
            </thead>
            <tbody>
            <tr dir-paginate="row in rowCollection|itemsPerPage:15|filter:search">
                 <td><%row.name| uppercase %></td>
                <td><%row.type %></td>
                <td><%row.next_date.slice(0,10)%></td>
                <td><%row.updated_date.slice(0,10)%></td>
                <td>
                <a href="{{url('web/matter-note') }}/<% row.id %>/edit" class="btn btn-xs col-sm-12 btn-primary">create matter note</a>
                </td>
                <td>
                <a href="{{url('web/matter-management') }}/<% row.id %>/edit" class="btn btn-xs col-sm-12 btn-primary">Edit</a>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5" class="text-center">
                    <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="7"></div>
                </td>
            </tr>
            </tfoot>
        </table>
         <div class="text-center">
                          	
            		 <dir-pagination-controls
            		 max-sizes="5"
            		 on-page-change="pageChangeHandler(newPageNumber)"
            		 direction-links="true"
            		 boundary-links="true" >
            		</dir-pagination-controls>
            		      
            </div>
    </div>
    {!! Form::close() !!}
@endsection
@section('footer')
    @parent
    {!! Html::script('js/matter-management.js') !!}
@endsection