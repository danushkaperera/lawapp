@extends('web.layouts.master')
 @include('web.layouts.dashboard')
@section('title', 'Create Matter Note')
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@section('sidebar')
    @parent
@endsection

@section('content')
<div class="content">
<h2>Create Matter Note</h2>
<button type="button" class="btn btn-sm btn-warning" id="backbtnmatter"  data-toggle="modal" >Go Back</button>
 {!! Form::open(array('url' => 'web/matter-note')) !!}
     
      {{ Form::hidden('legal_matter_id', $matter->id) }}
     
      <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span> {!! Form::label('note', 'Note :') !!}</div>
                <div class="col-sm-9">
                    {{ Form::textarea('note', $matternote->note,['class' => 'form-control']) }}
                    @if ($errors->has('note'))
                        <span class="error-block">
                            <strong>{{ $errors->first('note') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    

     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   {{ Form::label('date', '  Date : ') }}</div>
                <div class="col-sm-9">
                  
                        {{ Form::input('text', 'date', null,['class' => 'form-control datepicker','data-date-format'=>'y-m-d']) }}
                    
                     @if ($errors->has('date'))
                        <span class="error-block">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
                    @endif</div>
                 </div>
        </div>
    </div>
    
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   {{ Form::label('next_date', ' Next Hearing Date ') }}</div>
                <div class="col-sm-9">
                  
                        {{ Form::input('text', 'next_date', null,['class' => 'form-control datepicker','data-date-format'=>'y-m-d']) }}
                    
                     @if ($errors->has('next_date'))
                        <span class="error-block">
                            <strong>{{ $errors->first('next_date') }}</strong>
                        </span>
                    @endif</div>
                 </div>
        </div>
    </div>
    
     
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span> {!! Form::label('name', 'Matter Title:') !!}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'name', $matter->name,['class' => 'form-control','readonly'=>'readonly']) }}
                    @if ($errors->has('name'))
                        <span class="error-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
      <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   {!! Form::label('', 'Type of Matter:') !!}</div>
                <div class="col-sm-9">
                    <!--<input type="radio" ng-model="formdata.type_id" name="type_id" value="1">-->
                   <input type="radio"  name="type_id" value="5" id="myradio"  {{ ($matter->type_id)=="1" ? 'checked='.'"'.'checked'.'"' : '' }} />{!! Form::label('type_id', 'Incorporation of a Company') !!}
                   <input type="radio"  name="type_id" value="5" id="myradio2" {{ ($matter->type_id)=="2" ? 'checked='.'"'.'checked'.'"' : '' }} />{!! Form::label('type_id', 'Litigation') !!}
                   <input type="radio"  name="type_id" value="5" id="myradio3" {{ ($matter->type_id)=="3" ? 'checked='.'"'.'checked'.'"' : '' }} /> {!! Form::label('type_id', 'IP Registration') !!}
                  
                    @if ($errors->has('type_id'))
                        <span class="error-block">
                            <strong>{{ $errors->first('type_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
  
    
      
       
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span> {!! Form::label('name', 'Selected Lawyers') !!}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'name', $lawyer,['class' => 'form-control','readonly'=>'readonly']) }}
                  
                </div>
            </div>
        </div>
    </div>
    
  <a href="{{url('web/matter-note/'.$matter->id)}}/viewnote" class="btn btn-default"> VIEW PAST UPDATE</a>
  <button type="submit" class="btn btn-default ">CREATE</button>
  
{!! Form::close() !!}
</div>
   <script>
document.getElementById("myradio").disabled = true;
document.getElementById("myradio2").disabled = true;
document.getElementById("myradio3").disabled = true;
  $('.datepicker').datepicker({
    dateFormat: 'yy-mm-dd',
   
});
  </script>
@endsection
@section('footer')


    @parent
  
@endsection