@extends('web.layouts.master')
 @include('web.layouts.dashboard')
@section('title', 'Create User')

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <h1>Add New User</h1>
            <button type="button" class="btn btn-sm btn-warning" id="backbtnlawyer"  data-toggle="modal" >Go Back</button>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2>Personal Details</h2>
        </div>
        
    </div>
    {!! Form::open(array('url' => 'web/lawyers','files' => true )) !!}

 <br>
 
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('full_name', 'Full Name:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'full_name', $lawyrs->full_name,['class' => 'form-control']) }}
                    @if ($errors->has('full_name'))
                        <span class="error-block">
                            <strong>{{ $errors->first('full_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required"></span>{{ Form::label('designation', 'Is A Key Contact:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                  {{ Form::select('head', ['0' => 'No', '1' => 'Yes' ]) }}
                    @if ($errors->has('head'))
                        <span class="error-block">
                            <strong>{{ $errors->first('head') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('designation', 'Designation:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'designation',$lawyrs->designation,['class' => 'form-control']) }}
                    @if ($errors->has('designation'))
                        <span class="error-block">
                            <strong>{{ $errors->first('designation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required"></span>{{ Form::label('division_id', 'Division:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">  
   {{ Form::select('division_id', $divisions, null, ['class' => 'form-control']
) }}
     @if ($errors->has('division_id'))
                        <span class="error-block">
                            <strong>{{ $errors->first('division_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
   
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
            <div class="col-sm-3"><span class="error_required"></span>{{ Form::label('image', 'Upload Image:',array('class' => 'control-label')) }}</div>
            <div class="col-sm-9">{{ Form::file('image',$lawyrs->image, ['class' => 'field']) }}
                   
                    @if ($errors->has('image'))
                        <span class="error-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div>

   
    <div class="row">
        <div class="col-sm-12">
            <h2>Contact Details</h2>
        </div>
    </div>
  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('direct_phone', 'Direct Phone Number:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'direct_phone',null,['class' => 'form-control' ,'placeholder'=>'(071) 333 3333']) }}
                    @if ($errors->has('direct_phone'))
                        <span class="error-block">
                            <strong>{{ $errors->first('direct_phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div>  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('email', 'Email Address:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'email',null,['class' => 'form-control']) }}
                    @if ($errors->has('email'))
                        <span class="error-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div> 
   <div class="row">
    <div class="col-sm-12">
     <h2>Login Details</h2>
    </div>
   </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('email', 'Login Email:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'email',null,['class' => 'form-control']) }}
                    @if ($errors->has('email'))
                        <span class="error-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div> 
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('password', 'Password:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('password', 'password',null,['class' => 'form-control']) }}
                    @if ($errors->has('password'))
                        <span class="error-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    {{ Form::input('hidden', 'status','active',['class' => 'form-control']) }}
                </div>
            </div>
        </div>
   </div>
 <br>
 </div>
 
  {{ Form::submit('Submit',array('class' => 'btn btn-submit')) }}
    </br> </br> </br>
    
   {!! Form::close() !!}
   
  
</div>
@endsection
@section('footer')
    @parent
  
@endsection