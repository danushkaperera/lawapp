@extends('web.layouts.master')

@section('title', 'Dashboard')

@section('sidebar')
    @parent
@endsection


  <nav class="navbar navbar-default">
        <div class="container">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('web/dashboard') }}">Home</a></li>
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                        <a href="{{ url('web/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                           
                        </li>
                </ul>
            </div>
        </div>
    </nav>
@section('content')
    
    
   
    <div class="row">
                            <h2> Dashboard </h2>
        <div class="col-sm-12">
            <div class="col-sm-4">
                <a href="{{url('web/legal-alert')}}" class="btn dashboard btn-default disabled">Legal Alerts Manager</a>
            </div>
            <div class="col-sm-4">
                <a href="{{url('web/quick-reference-guide')}}" class="btn dashboard btn-default ">Quick Reference Guide Manager</a>
            </div>
            <div class="col-sm-4">
                 <a href="{{url('web/lawyers')}}" class="btn dashboard  btn-default disabled">Lawyer Manager</a>
            </div>
         </div>  
          
       
         <div class="col-sm-12">
            <div class="col-sm-4">
                <a href="{{url('web/client/requests')}}" class="btn dashboard btn-default disabled">Client Request</a>
            </div>
           
             <div class="col-sm-4">
                <a href="{{url('web/matter-management')}}" class="btn dashboard btn-default disabled">Matter Manager</a>
            </div>
             <div class="col-sm-4">
                <a href="{{url('web/clients')}}" class="btn dashboard btn-default disabled">Client Manager</a>
            </div>
         </div>
          
    </div>

@endsection
@section('footer')
    @parent
    {!! Html::script('js/login.js') !!}
@endsection