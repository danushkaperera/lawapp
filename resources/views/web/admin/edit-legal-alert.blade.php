@extends('web.layouts.master')
 @include('web.layouts.dashboard')
@section('title', 'Edit Legal Alert')

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="content">
<h2>Edit Legal Alert</h2>
  <button type="button" class="btn btn-sm btn-warning" id="backbtnlegalalert"  data-toggle="modal" >Go Back</button>
 
   {!! Form::model($alert, [
    'method' => 'PUT',
    'route' => array('web.legal-alert.update', $alert->id),
    'files' => true  
    ]) !!}  
    
  <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>     {{ Form::label('title', 'Title',  ['class' => 'control-label']) }}</div>
                <div class="col-sm-9">
                      {{ Form::input('title', 'title',$alert->title,['class' => 'form-control']) }}
                    @if ($errors->has('title'))
                        <span class="error-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
  
 <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   {!! Form::label('type_id', 'Legal Alert Type:') !!}</div>
                <div class="col-sm-9">
                    {{ Form::select('type_id',  $types,$alert->type_id, ['class' => 'form-control'  ]) }}
                    @if ($errors->has('type_id'))
                        <span class="error-block">
                            <strong>{{ $errors->first('type_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
  
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>     {{ Form::label('message', 'Notes',  ['class' => 'control-label']) }}</div>
                <div class="col-sm-9">
                     {{ Form::textarea('message', $alert->message,['class' => 'form-control']) }}
                    @if ($errors->has('message'))
                        <span class="error-block">
                            <strong>{{ $errors->first('message') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
  
 
 <br>
  <button type="submit" class="btn btn-default">Submit</button>
{!! Form::close() !!}
</div>
@endsection
