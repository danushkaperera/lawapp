@extends('web.layouts.master')
 @include('web.layouts.dashboard')
@section('title', ' matter management')

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="content">
   
 
    
<h2> Matter Updates</h2>
  <a href="{{ URL::previous() }}" class="btn btn-sm btn-warning" >Go Back</a>
 {!! Form::open(array('url' => 'web/matter-management/update')) !!}
 
 <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span> {!! Form::label('name', 'Matter Title:') !!}</div>
                <div class="col-sm-9">
                     {{ Form::input('text', 'name', $matter->name,['class' => 'form-control','readonly'=>'readonly']) }}
                    @if ($errors->has('name'))
                        <span class="error-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
 <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   {!! Form::label('', 'Type of Matter:') !!}</div>
                <div class="col-sm-9">
                    <!--<input type="radio" ng-model="formdata.type_id" name="type_id" value="1">-->
                   <input type="radio"  name="type_id" value="5" id="myradio"{{ ($matter->type_id)=="1" ? 'checked='.'"'.'checked'.'"' : '' }} />{!! Form::label('type_id', 'Incorporation of a Company') !!}
                   <input type="radio"  name="type_id" value="5" id="myradio2" {{ ($matter->type_id)=="2" ? 'checked='.'"'.'checked'.'"' : '' }} />{!! Form::label('type_id', 'Litigation') !!}
                   <input type="radio"  name="type_id" value="5" id="myradio3" {{ ($matter->type_id)=="3" ? 'checked='.'"'.'checked'.'"' : '' }} /> {!! Form::label('type_id', 'IP Registration') !!}
                  
                    @if ($errors->has('type_id'))
                        <span class="error-block">
                            <strong>{{ $errors->first('type_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
  
 
  <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
               
                <div class="col-sm-6">
          
               <ol>
@if(count($legal_matter_note))
  

                    @foreach ($legal_matter_note as $legal_matter_notes)

 
                    
                        <li>  {{ Form::hidden('id', $legal_matter_notes->id) }}
                        {{ Form::text('text',$legal_matter_notes->updated_at->format('Y-m-d'), null ,['class' => 'form-control ','id' => 'datepicker']) }} 
                               <button type="button" class="btn btn-sm btn-primary"  id="myBtn" data-toggle="modal" data-target="#{{$legal_matter_notes->id}}">VIEW</button>
                        
                                         
                                            <!-- Modal -->
                            <div class="modal fade" id="{{$legal_matter_notes->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"> {!! Form::label('note', 'View Note:') !!}</h4>
                                  </div>
                                  <div class="modal-body">
                                          {{ Form::textarea('note', $legal_matter_notes->note,['class' => 'form-control']) }}
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                       </li> 
                        @endforeach
@else
   There is no any updates!
@endif
                    
               </ol>
          </div>
        </div>
    </div>
   
   

   
{!! Form::close() !!}
</div>
<script>
document.getElementById("myradio").disabled = true;
document.getElementById("myradio2").disabled = true;
document.getElementById("myradio3").disabled = true;
</script>

@endsection
@section('footer')
    @parent
   {!! Html::script('js/update-view.js') !!}
@endsection