@extends('web.layouts.master')
 @include('web.layouts.dashboard')
@section('title', 'Create Legal Alert')
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="content">
<h2>Create Legal Alert</h2>
   <button type="button" class="btn btn-sm btn-warning" id="backbtnlegalalert"  data-toggle="modal" >Go Back</button>
 {{ Form::open(array('url' => 'web/legal-alert')) }}
      <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>  {{ Form::label('title', 'Title') }}</div>
                <div class="col-sm-9">
                       {{ Form::input('title', 'title',$alert->title,['class' => 'form-control']) }}
                    @if ($errors->has('title'))
                        <span class="error-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
  
 <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   {!! Form::label('type_id', 'Legal Alert Type:') !!}</div>
                <div class="col-sm-9">
                    {{ Form::select('type_id',  $types,null, ['class' => 'form-control' , 'ng-model' => 'data.type', ]) }}
                    @if ($errors->has('type_id'))
                        <span class="error-block">
                            <strong>{{ $errors->first('type_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
  
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>  {{ Form::label('message', 'Note') }}</div>
                <div class="col-sm-9">
                        {{ Form::textarea('message', $alert->message,['class' => 'form-control']) }}
                    @if ($errors->has('message'))
                        <span class="error-block">
                            <strong>{{ $errors->first('message') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
  
  
 <br>
  <button type="submit" class="btn btn-default">Submit</button>
{!! Form::close() !!}
</div>
 <script>

   $('.datepicker').datepicker({
    dateFormat: 'yy-mm-dd',
   
});
  </script>
@endsection
@section('footer')
    @parent
  
@endsection