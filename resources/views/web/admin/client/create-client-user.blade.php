@extends('web.layouts.master')
 @include('web.layouts.dashboard')
@section('title', 'Create User')

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <h1>Add New User</h1>
            <a href="{{ URL::previous() }}" class="btn btn-sm btn-warning" >Go Back</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2>Personal Details</h2>
        </div>
    </div>
    {!! Form::open(array('url' => 'web/clients','files' => true )) !!}
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('full_name', 'Full Name:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'name',$client->name,['class' => 'form-control']) }}
                    @if ($errors->has('name'))
                        <span class="error-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('designation', 'Designation:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'designation',$client->designation,['class' => 'form-control']) }}
                    @if ($errors->has('designation'))
                        <span class="error-block">
                            <strong>{{ $errors->first('designation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3">{{ Form::label('division', 'Division:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'division',$client->division,['class' => 'form-control']) }}
                    @if ($errors->has('division'))
                        <span class="error-block">
                            <strong>{{ $errors->first('division') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('company', 'Company:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'company',$client->company,['class' => 'form-control']) }}
                    @if ($errors->has('company'))
                        <span class="error-block">
                            <strong>{{ $errors->first('company') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div>
   
    <div class="row">
        <div class="col-sm-12">
            <h2>Contact Details</h2>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('department', 'Department:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'department',null,['class' => 'form-control']) }}
                    @if ($errors->has('department'))
                        <span class="error-block">
                            <strong>{{ $errors->first('department') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div>  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('direct_phone_number', 'Direct Phone Number:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'phone',null,['class' => 'form-control']) }}
                    @if ($errors->has('phone'))
                        <span class="error-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div>  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('email', 'Email Address:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'email',$client->email,['class' => 'form-control']) }}
                    @if ($errors->has('email'))
                        <span class="error-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div> 
   <div class="row">
    <div class="col-sm-12">
     <h2>Login Details</h2>
    </div>
   </div>
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('email', 'User Name:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'email',$client->email,['class' => 'form-control']) }}
                </div>
            </div>
        </div>
   </div> 
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('password', 'Password:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('password', 'password',null,['class' => 'form-control']) }}
                    @if ($errors->has('password'))
                        <span class="error-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    {{ Form::input('hidden', 'id',$client->id,['class' => 'form-control']) }}
                      {{ Form::input('hidden', 'status','active',['class' => 'form-control']) }}
                     
                </div>
            </div>
        </div>
   </div>
 <br>
 <div class="row">
  <div class="col-sm-12">
    {{ Form::submit('Submit',array('class' => 'btn btn-submit')) }}
  </div>
 </div>
   {!! Form::close() !!}

  
</div>
@endsection
@section('footer')
    @parent
  
@endsection