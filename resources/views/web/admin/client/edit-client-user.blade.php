@extends('web.layouts.master')
 @include('web.layouts.dashboard')
@section('title', 'Create User')

@section('sidebar')
    @parent
@endsection
<style>
#status, input[type="radio"]{
margin: 0px 0 0 !important;

}
</style>
@section('content')
<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <h1>Add New User</h1>
           <button type="button" class="btn btn-sm btn-warning" id="backbtnclient"  data-toggle="modal" >Go Back</button>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2>Personal Details</h2>
        </div>
    </div>
    <!--{!! Form::open(array('url' => 'web/clients','files' => true )) !!}-->
        {!! Form::model($client, [
        'method' => 'PUT',
        'route' => array('web.clients.update', $client->id),
        'files' => true
        ]) !!}


    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('status', 'Status',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::label('active','Active',array('id'=>'','class'=>'')) }}
                    {{ Form::radio('status', 'active', 'true', ['class' => 'radio-inline']) }}
                    {{ Form::label('deactive','Deactive',array('id'=>'','class'=>'')) }}
                    {{ Form::radio('status', 'deactive', 'false', ['class' => 'radio-inline']) }}
                    @if ($errors->has('status'))
                        <span class="error-block">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>              
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('full_name', 'Full Name:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'name',$client->full_name, ['class' => 'form-control']) }}
                    @if ($errors->has('name'))
                        <span class="error-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('designation', 'Designation:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'designation',$client->designation,['class' => 'form-control']) }}
                    @if ($errors->has('designation'))
                        <span class="error-block">
                            <strong>{{ $errors->first('designation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('company', 'Company:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'company',$client->company,['class' => 'form-control']) }}
                    @if ($errors->has('company'))
                        <span class="error-block">
                            <strong>{{ $errors->first('company') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div>
    <div class="row">
        <div class="col-sm-12">
            <h2>Contact Details</h2>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('department', 'Department:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'department',$client->department,['class' => 'form-control']) }}
                    @if ($errors->has('department'))
                        <span class="error-block">
                            <strong>{{ $errors->first('department') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div>  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('direct_phone_number', 'Direct Phone Number:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'phone',$client->phone,['class' => 'form-control']) }}
                    @if ($errors->has('phone'))
                        <span class="error-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div>  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('email', 'Email Address:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'email',$client->users()->first()->email,['class' => 'form-control']) }}
                    @if ($errors->has('email'))
                        <span class="error-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
   </div> 
   <div class="row">
    <div class="col-sm-12">
     <h2>Login Details</h2>
    </div>
   </div>
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>{{ Form::label('email', 'User Email:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('text', 'email',$client->users()->first()->email,['class' => 'form-control']) }}
                </div>
            </div>
        </div>
   </div> 
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required"></span>{{ Form::label('password', 'Password:',array('class' => 'control-label')) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('password', 'password',$client->password,['class' => 'form-control']) }}
                    @if ($errors->has('password'))
                        <span class="error-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                   
                </div>
            </div>
        </div>
   </div>
 <br>
 <div class="row">
  <div class="col-sm-12">
    {{ Form::submit('Submit',array('class' => 'btn btn-submit')) }}
  </div>
 </div>
   {!! Form::close() !!}
    <!--@if ($errors->any())-->
    <!--  <div class="row">-->
    <!--      <div class="col-sm-12">-->
    <!--        <ul class="alert alert-danger" >-->
    <!--          @foreach ($errors->all() as $errors)-->
    <!--          <li>{{$errors}}</li>-->
    <!--          @endforeach-->
    <!--          </ul>-->
    <!--      </div>-->
    <!--  </div>-->
    <!-- @endif-->

  
</div>
@endsection
@section('footer')
    @parent
  
@endsection