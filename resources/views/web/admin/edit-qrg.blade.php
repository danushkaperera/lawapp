@extends('web.layouts.master')
 @include('web.layouts.dashboard')
@section('title', 'Edit Quick Reference Guide')

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="content">
<h2>Edit Quick Reference Guide</h2>
<button type="button" class="btn btn-sm btn-warning" id="backbtnqrg"  data-toggle="modal" >Go Back</button>

   {!! Form::model($qrg, [
    'method' => 'PUT',
    'route' => array('web.quick-reference-guide.update', $qrg->id),
    'files' => true  
    ]) !!}
    
     
  <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>     {{ Form::label('title', 'Title',  ['class' => 'control-label']) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('title', 'title',$qrg->title,['class' => 'form-control']) }}
                    @if ($errors->has('title'))
                        <span class="error-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>

 <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required"></span>     {{ Form::label('priority', 'Priority',  ['class' => 'control-label']) }}</div>
                <div class="col-sm-9">
                    {{ Form::input('priority', 'priority',$qrg->priority,['class' => 'form-control']) }}
                    @if ($errors->has('priority'))
                        <span class="error-block">
                            <strong>{{ $errors->first('priority') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div
  

   <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>  {{ Form::label('body', 'Notes:',  ['class' => 'control-label']) }}</div>
                <div class="col-sm-9">
                 {!! Form::textarea('body', $qrg->body, ['class' => 'form-control']) !!}
                    @if ($errors->has('body'))
                        <span class="error-block">
                            <strong>{{ $errors->first('body') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
  
         
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>    {{ Form::label('created_at', ' Publish Date ') }}</div>
                <div class="col-sm-9">
                  {{ Form::input('text', 'created_at', $qrg->created_at->format('Y-m-d'),['id' => 'datepicker','readonly'=>'readonly','class' => 'form-control datepicker','data-date-format'=>'y-m-d']) }}
            </div>
        </div>
    </div>
  
 <br>
  <button type="submit" class="btn btn-primary">Submit</button>
{!! Form::close() !!}
</div>

@endsection
@section('footer')
    @parent
  
@endsection