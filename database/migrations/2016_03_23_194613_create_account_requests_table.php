<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');	  
            $table->string('company');	  
            $table->string('email')->unique();	  
            $table->string('designation'); 	  
            $table->string('phone');	  
            $table->enum('status',['pending','approved','declined']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('account_requests');
    }
}
