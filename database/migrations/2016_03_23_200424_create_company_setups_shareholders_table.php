<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanySetupsShareholdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_setup_shareholders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_setup_id')->unsigned();	  
            $table->string('nic_country');	  
            $table->string('nic_id');	  
            $table->string('residentail_address_local');	  
            $table->string('residentail_address_foregin');	 
            $table->string('current_occupation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_setup_shareholders');
    }
}
