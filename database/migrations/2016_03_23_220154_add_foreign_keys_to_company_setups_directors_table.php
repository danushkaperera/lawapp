<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToCompanySetupsDirectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_setup_directors', function (Blueprint $table) {
            
            $table->foreign('company_setup_id')
            ->references('id')
            ->on('company_setups');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_setup_directors', function (Blueprint $table) {
             
            $table->dropForeign('company_setup_directors_company_setup_id_foreign');
            
        });
    }
}
