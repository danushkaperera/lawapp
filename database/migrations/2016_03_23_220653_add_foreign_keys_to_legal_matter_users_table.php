<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToLegalMatterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('legal_matter_users', function (Blueprint $table) {
            
            $table->foreign('legal_matter_id')
            ->references('id')
            ->on('legal_matters');
            
            $table->foreign('user_id')
            ->references('id')
            ->on('users');
            
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('legal_matter_users', function (Blueprint $table) {
            
            $table->dropForeign('legal_matter_users_legal_matter_id_foreign');
            $table->dropForeign('legal_matter_users_user_id_foreign');
            
        });
    }
}
