<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToLegalMatterNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('legal_matter_notes', function (Blueprint $table) {
            
            $table->foreign('legal_matter_id')
            ->references('id')
            ->on('legal_matters');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('legal_matter_notes', function (Blueprint $table) {
             
            $table->dropForeign('legal_matter_notes_legal_matter_id_foreign');
            
        });
    }
}
