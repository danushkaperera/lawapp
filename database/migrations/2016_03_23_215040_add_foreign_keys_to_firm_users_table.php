<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToFirmUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('firm_users', function (Blueprint $table) {
            
            // $table->foreign('designation_id')
            // ->references('id')
            // ->on('designations');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('firm_users', function (Blueprint $table) {
             
            // $table->dropForeign('firm_users_designation_id_foreign');
            
        });
    }
}
