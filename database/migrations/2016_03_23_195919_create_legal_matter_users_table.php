<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegalMatterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_matter_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('legal_matter_id')->unsigned();	 
             $table->integer('user_id')->unsigned();	 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('legal_matter_users');
    }
}
