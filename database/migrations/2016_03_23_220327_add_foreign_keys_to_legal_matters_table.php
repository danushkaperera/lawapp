<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToLegalMattersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('legal_matters', function (Blueprint $table) {
            
            $table->foreign('type_id')
            ->references('id')
            ->on('legal_matter_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('legal_matters', function (Blueprint $table) {
             
            $table->dropForeign('legal_matters_type_id_foreign');
            
        });
    }
}
