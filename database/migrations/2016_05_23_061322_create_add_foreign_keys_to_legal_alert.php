<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddForeignKeysToLegalAlert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('legal_alerts', function (Blueprint $table) {
              $table->foreign('type_id')
            ->references('id')
            ->on('legal_alert_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('legal_alerts', function (Blueprint $table) {
            $table->dropForeign('legal_alerts_type_id_foreign');
        });
    }
}
