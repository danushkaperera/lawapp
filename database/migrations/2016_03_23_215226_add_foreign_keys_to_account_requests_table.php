<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToAccountRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_requests', function (Blueprint $table) {
            
            // $table->foreign('designation_id')
            // ->references('id')
            // ->on('designations');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_requests', function (Blueprint $table) {
             
            // $table->dropForeign('account_requests_designation_id_foreign');
            
        });
    }
}
