<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFirmUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('firm_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name'); 
            $table->integer('division_id')->unsigned();	  
            $table->string('designation');	  
            $table->string('phone');	  
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('firm_users');
    }
}
