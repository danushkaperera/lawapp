<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToPasswordRecoveryRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('password_recovery_requests', function (Blueprint $table) {
            
            $table->foreign('user_id')
            ->references('id')
            ->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('password_recovery_requests', function (Blueprint $table) {
            
             $table->dropForeign('password_recovery_requests_user_id_foreign');
            
        });
    }
}
