<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as Adapter;

class CreateLawyersTable extends Migration
{
    private $filesystem;

    public function __construct()
    {
        $this->filesystem = new Filesystem(new Adapter( public_path() ));
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->filesystem->createDir('images/lawyers');
        
        Schema::create('lawyers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->string('designation');
            $table->integer('division_id')->unsigned();
            $table->string('email');
            $table->string('direct_phone');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lawyers');
    }
}
