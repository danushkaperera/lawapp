<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToAppointmentRequestUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointment_request_users', function (Blueprint $table) {
            
            $table->foreign('appointment_request_id')
            ->references('id')
            ->on('appointment_requests')
            ->onDelete('cascade');
            
            $table->foreign('lawyer_id')
            ->references('id')
            ->on('lawyers')
            ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointment_request_users', function (Blueprint $table) {
            $table->dropForeign('appointment_request_users_appointment_request_id_foreign');
            $table->dropForeign('appointment_request_users_lawyer_id_foreign');
        });
    }
}
