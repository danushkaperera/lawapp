<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class LegalAlertTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        App\LegalAlert::truncate();
        App\LegalAlertType::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
        $faker = Faker::create();

        App\LegalAlertType::insert([
            ['id'=> 1, 'type'=> 'A'],
            ['id'=> 2, 'type'=> 'B'],
            ['id'=> 3, 'type'=> 'C'],
            ]);
    
        
        foreach (range(1,10) as $index) 
        {
            $row = [];
            $row['title'] = $faker->sentence;
            $row['message'] = $faker->paragraph;
            $row['type_id'] = 1;
            
            App\LegalAlert::create($row);
	        
        }
    }
}
