<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\User;
use App\CorporateUser;
use App\FirmUser;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        App\User::truncate();
        App\CorporateUser::truncate();
        App\FirmUser::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
        $faker = Faker::create();
        
        for ($i=0; $i < 15; $i++) {
            $u = $this->createUser($faker);
            $this->createCoperateUser($faker)->users()->save($u);
        }
        
        for ($i=0; $i < 15; $i++) {
            $u = $this->createUser($faker);
            $this->createFirmUser($faker)->users()->save($u);
        }
    }
    
    public function createUser($faker)
    {
        $inputs = 
        [
            'email' => $faker->email, 
            'password' => '1234', 
            'device' => 'android',
            'push_id' => 'sdasdwsn3ekfndklnsdkvnrkn'  
        ];
        
        return User::create($inputs);
    }
    
    public function createCoperateUser($faker)
    {
        $fillable = [
        'name' => $faker->name,	  
        'company' => $faker->company,	  
        'department' => 'accounts',	  
        'designation' => 'manager',	  
        'phone' => '(940) 112 22365',	  
        'status' => 'active'
        ];
        
        return App\CorporateUser::create($fillable);
    }
    
    public function createFirmUser($faker)
    {
        $fillable = [
        'name' => $faker->name,	  
        'department' => 'Litigation',	  
        'designation' => 'lawyer',	  
        'phone' => '(940) 112 23234',	  
        'status' => 'active'
        ];
        
        return App\FirmUser::create($fillable);
        
    }

}
