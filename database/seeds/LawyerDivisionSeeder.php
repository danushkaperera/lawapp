<?php

use Illuminate\Database\Seeder;

class LawyerDivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        App\LawyerDivision::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
        $dividions = 
        [
            'Corporate Law Division',
            'Litigation Division',
            'Intellectual Property Division',
            'Real Property Division',
            'Corporate Services (Private) Limited'
        ];
        
        $collection = collect($dividions);
        
        $collection->each(function ($item, $key) {
            App\LawyerDivision::create(['division'=>$item]);
        });
        
        
         
        
    }
}
