<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class QRGTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        App\QuickReferenceGuide::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
        $faker = Faker::create();

    
        
        foreach (range(1,10) as $index) 
        {
            $row = [];
            $row['title'] = $faker->sentence;
            $row['body'] = $faker->paragraph;
            $row['publish'] = true;
            
            App\QuickReferenceGuide::create($row);
	        
        }
        
        
        
        
        
    }
}
