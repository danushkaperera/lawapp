<?php

use Illuminate\Database\Seeder;

class LegalMatterTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        App\LegalMatterType::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
        $dividions = 
        [
            'Incorporation of a Company',
            'Litigation',
            'IP Registration'
        ];
        
        $collection = collect($dividions);
        
        $collection->each(function ($item, $key) {
            App\LegalMatterType::create(['type'=>$item]);
        });
        
    }
}
