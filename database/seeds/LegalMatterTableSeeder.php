<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

use App\LegalMatterNote;
use App\LegalMatterUser;
use App\LegalMatter;

class LegalMatterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        App\LegalMatterNote::truncate();
        App\LegalMatterUser::truncate();
        App\LegalMatter::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
        $faker = Faker::create();
        
        for ($i=0; $i < 5; $i++) {
            
            $matter = [];
            $matter['name'] = $faker->name . " vs " . $faker->name;
            $matter['type_id'] = $faker->numberBetween(1,3);
            $matter['next_date'] = Carbon::now()->toDateTimeString();
            $matter['updated_date'] = Carbon::now()->toDateTimeString();
            $matter['closed'] = false;
            
            $legel_matter = LegalMatter::create($matter);
            
            for ($j=0; $j < 5; $j++) {
                
                $matter_note = [];
                $matter_note['legal_matter_id'] = $legel_matter->id;
                $matter_note['date'] = Carbon::now()->toDateTimeString();
                $matter_note['note'] = $faker->sentence;
                
                LegalMatterNote::create($matter_note);
            }
            
             for ($j=0; $j < 5; $j++) {
                
                $matter_user = [];
                $matter_user['legal_matter_id'] = $legel_matter->id;
                $matter_user['user_id'] =  $faker->numberBetween(1,30);

                LegalMatterUser::create($matter_user);
            }
          
        }
    }
}
