<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        // $this->call(UserTableSeeder::class);
        $this->call(LawyersTableSeeder::class);
        $this->call(LegalAlertTableSeeder::class);
        $this->call(QRGTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(LegalMatterTypeTableSeeder::class);
        $this->call(LegalMatterTableSeeder::class);
        $this->call(AdminSeeder::class);
       
        
        Model::reguard();

    }
}
