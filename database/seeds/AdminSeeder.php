<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        App\Admin::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
       // -------------------------------------------

        $row = [];
        $row['email'] = 'admin1@gmail.com';
        $row['password'] = '1234';
        $row['type'] = 'Track your matter - incorporation of a company';
        
        App\Admin::create($row);
        
       // -------------------------------------------
        
        $row['email'] = 'admin2@gmail.com';
        $row['password'] = '1234';
        $row['type'] = 'Track your matter - Litigation';
        
        App\Admin::create($row);
        
       // -------------------------------------------
        
        
        $row['email'] = 'admin3@gmail.com';
        $row['password'] = '1234';
        $row['type'] = 'Track your matter - IP registration';
        
        App\Admin::create($row);
        
       // -------------------------------------------
        
        $row['email'] = 'admin4@gmail.com';
        $row['password'] = '1234';
        $row['type'] = 'General IT administrator';
        
        App\Admin::create($row);
        
       

    }
}
