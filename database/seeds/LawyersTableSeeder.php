<?php

use Illuminate\Database\Seeder;


class LawyersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        App\Lawyer::truncate();
        App\LawyerDivision::truncate();
        App\Designation::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        
        $data = 
        [
            ['name'=>'Deepthi Abeysena', 'designation'=>'Director- Real Property', 'division'=>'Real Property Division', 'email'=>'deepthi.fernando@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5149', 'head'=>false ],
            ['name'=>'Ayomi Aluwihare-Gunawardena', 'designation'=>'Partner', 'division'=>'Corporate Law Division', 'email'=>'ayomi.aluwihare@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5115', 'head'=>true ],
            ['name'=>'Praveeni Algama', 'designation'=>'Senior Associate', 'division'=>'Corporate Law Division', 'email'=>'praveeni.algama@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5171', 'head'=>false ],
            ['name'=>'Buwaneka Basnayake', 'designation'=>'Senior Legal Counsel', 'division'=>'Corporate Law Division', 'email'=>'buwaneka.basnayake@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5181', 'head'=>false ],
            ['name'=>'Chamari de Silva', 'designation'=>'Partner', 'division'=>'Real Property Division', 'email'=>'chamari.desilva@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5150', 'head'=>false ],
            ['name'=>'Nimesha de Silva', 'designation'=>'Litigator', 'division'=>'Litigation Division', 'email'=>'nimesha.desilva@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5160', 'head'=>false ],
            ['name'=>'Raneesha de Alwis', 'designation'=>'Senior Litigator', 'division'=>'Litigation Division', 'email'=>'raneesha.dealwis@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5183', 'head'=>false ],
            ['name'=>'Harshana de Alwis', 'designation'=>'Litigator', 'division'=>'Litigation Division', 'email'=>'harshana.dealwis@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5162', 'head'=>false ],
            ['name'=>'Kolitha Dissanayake', 'designation'=>'Senior Associate', 'division'=>'Corporate Law Division', 'email'=>'kolitha.dissanayake@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5175', 'head'=>false ],
            ['name'=>'Manjula Ellepola', 'designation'=>'Partner', 'division'=>'Real Property Division', 'email'=>'manjula.ellepola@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5147', 'head'=>true ],
            ['name'=>'Anjali Fernando', 'designation'=>'Partner', 'division'=>'Corporate Law Division', 'email'=>'anjali.fernando@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5123', 'head'=>false ],
            ['name'=>'Rozali Fernando', 'designation'=>'Litigator', 'division'=>'Litigation Division', 'email'=>'rozali.fernando@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5132', 'head'=>false ],
            ['name'=>'Tania Fernando', 'designation'=>'Associate', 'division'=>'Intellectual Property Division', 'email'=>'tania.fernando@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5186', 'head'=>false ],
            ['name'=>'Tilani Ford', 'designation'=>'Senior Associate', 'division'=>'Corporate Law Division', 'email'=>'tilani.ford@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5121', 'head'=>false ],
            ['name'=>'Shanaka Gunasekara', 'designation'=>'Senior Litigator', 'division'=>'Litigation Division', 'email'=>'shanaka.gunasekara@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5116', 'head'=>false ],
            ['name'=>'Shehara Gunasekara', 'designation'=>'Associate', 'division'=>'Real Property Division', 'email'=>'shehara.gunasekara@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5142', 'head'=>false ],
            ['name'=>'Natalie Gnaniah', 'designation'=>'Executive-Legal', 'division'=>'Corporate Services (Private) Limited', 'email'=>'natalie.gnaniah@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5119', 'head'=>false ],
            ['name'=>'Christina Hettiarachchi', 'designation'=>'Legal Assistant', 'division'=>'Litigation Division', 'email'=>'christina.hettiarachchi@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5131', 'head'=>false ],
            ['name'=>'M. Ashiq Hassim', 'designation'=>'Litigator', 'division'=>'Litigation Division', 'email'=>'ashiq.hassim@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5157', 'head'=>false ],
            ['name'=>'Kasuni Jayaweera', 'designation'=>'Litigator', 'division'=>'Litigation Division', 'email'=>'kasuni.jayaweera@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5178', 'head'=>false ],
            ['name'=>'Suganthie Wijayasuriya-Kadirgamar', 'designation'=>'Senior Partner', 'division'=>'Intellectual Property Division', 'email'=>'suganthie.wijayasuriya@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5141', 'head'=>true ],
            ['name'=>'U.L. Kadurugamuwa', 'designation'=>'Consultant', 'division'=>'Corporate Law Division', 'email'=>'ul.kadurugamuwa@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5114', 'head'=>false ],
            ['name'=>'Roshani Kobbekaduwa', 'designation'=>'Partner', 'division'=>'Corporate Law Division', 'email'=>'roshani.kobbekaduwa@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5122', 'head'=>false ],
            ['name'=>'Jayathri Kulatilleke', 'designation'=>'Partner', 'division'=>'Intellectual Property Division', 'email'=>'jayathri.kulatilaka@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5152', 'head'=>false ],
            ['name'=>'Roshika Kulatunge', 'designation'=>'Manager, Corporate Services (Pvt) Ltd.', 'division'=>'Corporate Services (Private) Limited', 'email'=>'roshika.kulatunga@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5127', 'head'=>false ],
            ['name'=>'Sankha Karunaratne', 'designation'=>'Senior Associate', 'division'=>'Corporate Law Division', 'email'=>'sankha.karunaratne@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5121', 'head'=>false ],
            ['name'=>'Charana Kanankegamage', 'designation'=>'Associate', 'division'=>'Corporate Law Division', 'email'=>'charana.kanankegamage@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5156', 'head'=>false ],
            ['name'=>'Himali Mudadeniya', 'designation'=>'Partner', 'division'=>'Corporate Law Division', 'email'=>'himali.mudadeniya@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5120', 'head'=>false ],
            ['name'=>'Shamil Mueen', 'designation'=>'Associate', 'division'=>'Corporate Law Division', 'email'=>'shamil.mueen@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5136', 'head'=>false ],
            ['name'=>'Pavithra Navarathne', 'designation'=>'Senior Associate', 'division'=>'Corporate Law Division', 'email'=>'pavithra.navarathne@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5179', 'head'=>false ],
            ['name'=>'Shehani Nayagam', 'designation'=>'Executive-Legal', 'division'=>'Corporate Services (Private) Limited', 'email'=>'shehani.nayagam@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5170', 'head'=>false ],
            ['name'=>'Shevanthi Seresinhe-Perera', 'designation'=>'Senior Associate', 'division'=>'Corporate Law Division', 'email'=>'shevanthi.perera@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5177', 'head'=>false ],
            ['name'=>'Udeesha Perera', 'designation'=>'Executive-Legal', 'division'=>'Corporate Services (Private) Limited', 'email'=>'udeesha.perera@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5192', 'head'=>false ],
            ['name'=>'Ramesh Perera', 'designation'=>'Legal Assistant', 'division'=>'Corporate Law Division', 'email'=>'ramesh.perera@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5158', 'head'=>false ],
            ['name'=>'Amali Peiris', 'designation'=>'Executive-Legal', 'division'=>'Corporate Services (Private) Limited', 'email'=>'amali.peiris@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5193', 'head'=>false ],
            ['name'=>'Avindra Rodrigo', 'designation'=>'Counsel', 'division'=>'Litigation Division', 'email'=>'avindra.rodrigo@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5137', 'head'=>true ],
            ['name'=>'Aruna D. de Silva', 'designation'=>'Litigator', 'division'=>'Litigation Division', 'email'=>'aruna.desilva@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5151', 'head'=>false ],
            ['name'=>'Medhya Samarasinghe', 'designation'=>'Litigator', 'division'=>'Litigation Division', 'email'=>'medhya.samarasinghe@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5133', 'head'=>false ],
            ['name'=>'Upekha Senaratne', 'designation'=>'Partner', 'division'=>'Intellectual Property Division', 'email'=>'upekha.senaratne@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5154', 'head'=>false ],
            ['name'=>'Patali H. Thrimanne', 'designation'=>'Litigator', 'division'=>'Litigation Division', 'email'=>'patali.thrimanne@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5100', 'head'=>false ],
            ['name'=>'Dinukshi Thalgahagoda', 'designation'=>'Partner', 'division'=>'Corporate Law Division', 'email'=>'dinukshi.thalgahagoda@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5180', 'head'=>false ],
            ['name'=>'Ananda Tikiriratne', 'designation'=>'Senior Litigator', 'division'=>'Litigation Division', 'email'=>'ananda.tikiriratne@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5159', 'head'=>false ],
            ['name'=>'Shehara Varia', 'designation'=>'Partner', 'division'=>'Corporate Law Division', 'email'=>'shehara.varia@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5128', 'head'=>false ],
            ['name'=>'Rosy Vanlangenberg', 'designation'=>'Associate', 'division'=>'Intellectual Property Division', 'email'=>'rosy.venlangenberg@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5187', 'head'=>false ],
            ['name'=>'Anuradha Wijesooriya', 'designation'=>'Litigator', 'division'=>'Litigation Division', 'email'=>'anuradha.wijesooriya@fjgdesaram.com', 'phone'=>'(+94) (11) 460 5139', 'head'=>false ],

        ];
        
        foreach($data as $lawyer)
        {
            $row = [];
            $row['division_id'] = App\LawyerDivision::firstOrCreate(['division' => $lawyer['division']])->id;
            $row['designation'] = $lawyer['designation'];
            $row['full_name'] = $lawyer['name'];
            $row['email'] = $lawyer['email'];
            $row['direct_phone'] = $lawyer['phone'];
            $row['image'] = 'images/lawyers/lawyer.jpg';
            $row['head'] = $lawyer['head'];
            App\Lawyer::create($row);
        }
    }
}
