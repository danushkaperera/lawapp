<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalAlert extends Model
{
    
       /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'message', 'type_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    
    public function type()
    {
        return $this->belongsTo('App\LegalAlertType');
    }
    
}
