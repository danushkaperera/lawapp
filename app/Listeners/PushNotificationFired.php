<?php

namespace App\Listeners;

use App\Events\SendPushNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Getupdates;
use App\Http\Controllers\Api\UserController;
use Auth;
use PushNotification;
class PushNotificationFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendPushNotification  $event
     * @return void
     */
    public function handle(SendPushNotification $event)
    {   

        if($event->user_id){
          //use for trackmatter users
            $user = User::Find($event->user_id);
            $push_id = $user->push_id;
            $device  = $user->device;
            if($push_id){ 
            $this->push_sender($push_id,$device,$event);
            }

        }else{
          //use for all users
            $all_users = User::All();
        $uController = new  UserController();
        foreach ($all_users as $key => $user) {
          $push_id = $user['push_id'];
          $device = $user['device'];

          $last_viewed_dates = Getupdates::where('user_id','=',$user['id'])->first();

        if($last_viewed_dates){

          $event->badges = array_sum($uController->getCounts($last_viewed_dates));
         
          
          }
        if($push_id && $device)
        {
            $this->push_sender($push_id,$device,$event);
        }
        }

        }
        

        

    }



    public function push_sender($push_id,$device,$event)
    {
         try{
               $device_os=''; 
            if ($device=='apple') {
               $device_os = 'appNameIOS';
            }elseif ($device=='Android') {
                $device_os = 'appNameAndroid';
            }


         if(($device_os)&&(strlen($push_id)>50)){


                $collection = PushNotification::app($device_os)
                ->to($push_id);
                             
               $message = PushNotification::Message($event->message,array(
                   'badge' => $event->badges
                ));
                $collection->send($message);

                
            }
            
                
            }
            catch(Exception $e)
            {
               // return $this->response->errorBadRequest($e);
                //dd($e->getMessage());
                
            }
    }
}
