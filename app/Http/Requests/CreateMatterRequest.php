<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateMatterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
       //  'type_id' => 'required', 
         'lead_lawyer_id' => 'required', 
        'represent_company' => 'required',
        	
        // 'division' => 'required|min:5',	
        // 'designation' => 'required|min:5',	
        // 'direct_phone' => 'required|min:3',		  
        // 'status' => 'required'	
        ];
    }
    
}
