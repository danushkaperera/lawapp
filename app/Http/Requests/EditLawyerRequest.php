<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditLawyerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
        'full_name' => 'required', 
        'email' => 'required|email|unique:users', 
        'password' => 'required',
         'image'=>'mimes:jpeg,jpg,png',      
        'designation' => 'required',    
        'direct_phone' => 'required|min:10|numeric',          
     
        ];
    }
    
}
