<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class CreateCorporateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
        'name' => 'required', 
        'email' => 'required|email|unique:users', 
        'password' => 'required|present',
        'company' => 'required',	
        'department' => 'required',	
        'designation' => 'required',	
        'phone' => 'required',		  
        'status' => 'required'	
        ];
    }
    

    
    
}
