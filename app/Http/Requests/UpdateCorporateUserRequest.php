<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateCorporateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
        
     
        return  [
        'name' => 'required', 
        'email' => 'required|email|unique:users,email,'.$this->route('id'), 
        'password' => 'present',
        'company' => 'required',	
        'department' => 'required',	
        'designation' => 'required',	
        'phone' => 'required|min:10',		  
        'status' => 'required'		 
        ];
    }
}
