<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateLawyerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       
        return  [
        'full_name' => 'required', 
         'image'=>'mimes:jpeg,jpg,png',
       //'division_id' => '',	
        'designation' => 'required',	
        'email' => 'required|email|unique:users', 
        'password' => 'required|present',
        'direct_phone' => 'required|digits:10|numeric',		  
       // 'status' => 'required'	
        ];
    }
    
}
