<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateFirmUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return  [
        'name' => 'required', 
        'email' => 'email|unique:users,'.$this->route('id'), 
        'password' => 'present',
        'company' => 'required',	
        'department' => 'required',	
        'designation' => 'required',	
        'phone' => 'required|min:3',		  
        'status' => 'required'		 
        ];
    }
}
