<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Lawyer;
use App\LawyerDivision;
use Event;
use App\Events\SendPushNotification;
class LawyerController extends BaseController
{
    
    public function __construct()
    {
       //$this->middleware('auth:api');
    }
    
    
    public function all()
    {   
        
        $lawyers = Lawyer::with(['division'])->orderBy('full_name', 'asc')->get(['id','full_name','designation','division_id']);
        
        $key_contacts = Lawyer::where('head', '1')->with(['division'])->orderBy('full_name', 'asc')->get(['id','full_name','designation','division_id']);
        
        
        $groups_p = array();
        $lawyers_data_p = $key_contacts->toArray();

        foreach($lawyers_data_p as $lawyer_p) {
            $groups_p[strtoupper($lawyer_p['full_name'][0])][] = $lawyer_p;
        }
        
        ksort($groups_p);
        
       
        
        $data = [];
        $data['partners'] = $groups_p;
        
        $groups = array();
        $lawyers_data = $lawyers->toArray();

        foreach($lawyers_data as $lawyer) {
            $groups[$lawyer['full_name'][0]][] = $lawyer;
        }
        
        ksort($groups);
        
        $data['data'] = $groups;
        
        
        return $this->response->array($data);
    }
    
    public function all_android()
    {
        $lawyers = Lawyer::with(['division'])->orderBy('full_name', 'asc')->get(['id','full_name','designation','division_id']);
        
        $key_contacts = Lawyer::where('head', '1')->with(['division'])->orderBy('full_name', 'asc')->get(['id','full_name','designation','division_id']);
        
        
        // $groups_p = array();
        // $lawyers_data_p = $key_contacts->toArray();

        // foreach($lawyers_data_p as $lawyer_p) {
        //     $groups_p[strtoupper($lawyer_p['full_name'][0])][] = $lawyer_p;
        // }
        
        // ksort($groups_p);
        
       
        
        $data = [];
        $data['partners'] = $key_contacts->values();
        
        $groups = array();
        $lawyers_data = $lawyers->toArray();

        foreach($lawyers_data as $lawyer) {
            $groups[$lawyer['full_name'][0]][] = $lawyer;
        }
        
        ksort($groups);
        
        $data['data'] = $groups;
        
        
        return $this->response->array($data);
    }
    
    
    
    public function item($id)
    {
        $lawyer = Lawyer::with(['division'])->find($id);
        
        if(!$lawyer)
        {
            return $this->response->errorNotFound("can't find the laywer");
        }
        
        $data = [];
        $data['data'] = $lawyer->toArray();
        
        return $this->response->array($data);
        
    }
    
    public function getAllDivisions(Request $request)
    {
        return $this->response->array([ 'data' => LawyerDivision::all() ]);
    }
    
    public function search(Request $request)
    {
        $by = $request->input('by');
        $q = $request->input('q');
        
        $base = new Lawyer;
        
        $len = strlen($q)>0;
        
        if($by == 'name' && $len)
        {
            $base = $base->where('full_name', 'LIKE', "%$q%");
        }
        else 
        {
            if($len){
                $base = LawyerDivision::where('division',$by)->first()->lawyers()->where('full_name', 'LIKE', "%$q%");
            }
            else
            {
                $base = LawyerDivision::where('division',$by)->first()->lawyers();
            }
            
        }
        
        $lawyers = $base->with(['division'])->orderBy('full_name', 'asc')->get(['id','full_name','designation','division_id']);
        
        $groups = array();
        $lawyers_data = $lawyers->toArray();

        foreach($lawyers_data as $lawyer) {
            $groups[$lawyer['full_name'][0]][] = $lawyer;
        }
        
        ksort($groups);
        
        $data['data'] = $groups;
        
        
       // $data = [];
       // $data['data'] = $lawyers->toArray();
        
        return $this->response->array($data);
        
    }
    
    
    
}
