<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\CompanySetupDirector;
use App\CompanySetupShareholder;


class ConpanyController extends BaseController
{
    
     public function __construct() 
    {
        $this->middleware('auth:api');
    }
    
    
    public function setup(Request $request)
    {
        $directors = $request->input('directors');
        
        if(!$directors || count($directors) == 0 )
        {
            return $this->response->errorBadRequest('at least one directors is required');
        }
        
        $shareholders = $request->input('shareholders');
        
        if(!$shareholders || count($shareholders) == 0 )
        {
            return $this->response->errorBadRequest('at least one shareholders is required');
        }
        
        try {
        
        $inputs = $request->only
        ([
            'name', 'address'
        ]);
        
        
        $company_setup = CompanySetup::create($inputs);
        
        foreach($directors as $director)
        {
            $director['company_setups_id'] =  $company_setup->id;
            CompanySetupDirector::create($director);
        }
        
        foreach($shareholders as $shareholder)
        {
            $shareholders['company_setups_id'] =  $company_setup->id;
            CompanySetupShareholder::create($shareholders);
        }
            
        
        } catch ( Exception $e){
            return $this->response->errorBadRequest($e->getMessage());
        }
        
        $data = [];
        $data['message'] = 'company setup request saved successfully';
        
        
        return $this->response->array($data);
    }
}
