<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests;
use Event;
use App\Events\SendPushNotification;
use Auth;
use JWTAuth;
use Dingo\Api\Exception\ValidationHttpException;

use Illuminate\Support\Facades\Mail;
use App\User;
use App\PasswordRecoveryRequest;
use App\AccountRequest;
use PushNotification;
use App\DeviceDetect;
use App\LegalAlert;
use App\QuickReferenceGuide;
use App\LegalMatter;
use View;
use App\Getupdates;
use Validator;

class UserController extends BaseController
{
    
    public function __construct() 
    {
        $this->middleware('auth:api', ['except' => ['views','login','recoverPassword','requestAccount','testPush'] ]);
    }
    
    public function login(Request $request)
    {
        
        $account_request = AccountRequest::where(['email'=>$request->input('email'),'status'=>'pending'])->first();
        if($account_request != null)
        {
             return $this->response->error('account activation pending', 203);
        }


        if(User::where(['email'=>$request->input('email')])->first()->user->status != 'active')
        {
            return $this->response->errorUnauthorized('login failed');
        }
        
        $credentials = $request->only('email', 'password');
        

        
        $token = Auth::guard('api')->attempt($credentials);
        
        if(!$token)
        {
            return $this->response->errorUnauthorized('login failed');
        }
        
        $user = Auth::setToken($token)->user();

	if (!$request->has('push_id')) {
	
	 $input_hack_push_id = $request->except(['email','password','device']);
	
	 $input_hack_push_id = key( $input_hack_push_id);
	//$input_hack_push_id = substr($input_hack_push_id,'"',1);
	
	$input_hack_push_id = str_replace("push_id","",$input_hack_push_id);
	//$request->input('push_id') = $input_hack_push_id;
	$request['push_id'] = $input_hack_push_id;
	//$request->input('push_id',$input_hack_push_id);

		}
	//dd($request->all());
	 $user->update($request->only('device', 'push_id'));
     
     $device_detect_data = array_add( $request->except(['email','password','device','push_id']), "user_id", $user->id);
     
if(isset($device_detect_data['device_id'])){
    if (!DeviceDetect::where('user_id',$device_detect_data['user_id'])->where('device_id',$device_detect_data['device_id'])->exists()) {
  
	 DeviceDetect::create($device_detect_data);
}
}
     	 
        
        return $this->response->array(compact('token'));
        
    }
    
   
    public function recoverPassword(Request $request)
    {
        $user = User::where($request->only('email'))->first();
        
        if(!$user)
        {
            return $this->response->errorNotFound("can't find the user");
        }
        
        
        $company = $user->user->company == $request->input('company');
        
        if(!$company)
        {
            return $this->response->errorNotFound("can't find the user");
        }
        
        PasswordRecoveryRequest::create(['user_id'=> $user->id]);

        $data = [];
        $data['token'] = rand(11111, 99999);

        if ($user) {
            
            $user->password = $data['token'];
            $user->save();
            
             try {
                Mail::send('email.forget_password', ['data' => $data],
                function ($message) use ($request) {
                     $message->from(env('APP_EMAIL'), env('APP_NAME'));
                     $message->to($request->input('email'));
                     $message->sender(env('APP_EMAIL'), env('APP_NAME'));
                     $message->subject('FJG Direct - Password Reset');
                });
            } catch(Exception $e) {}
            
    

        }

        
        
        $data = "password recovery requested successfully";
        
        return $this->response->array(compact('data'));
        
    }
    
    public function getProfile()
    {
        $user = Auth::user();
        
        $data = [];
        $data['email'] = $user->email;
        $data['user_type'] = $user->user_type;
        
        $user_data = $user->user()->first();
        
        $data = array_merge($data, $user_data->toArray());
        
        return $this->response->array(compact('data'));
        
    }
    
    public function requestAccount(Request $request)
    {  

        $request->email = str_replace(".stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))","",$request->email);

        $inputs = $request->only(
            'name',	  
            'company',	 
            'email',	  
            'designation',	  
            'phone'
            );
      $inputs['email'] = $request->email;

            $rules = array(
                'name'           => 'required',                        
                'email'          => 'required|email|unique:users|unique:account_requests',     
                'company'        => 'required',
                'designation'    => 'required',          
                'phone'          => 'required',          
            );
   
             $validator = Validator::make($inputs, $rules);

    
            if ($validator->fails()) {

                //$messages = $validator->messages();
        
                throw new ValidationHttpException($validator->errors());
        
            } 
            
        AccountRequest::create($inputs);
        
        // return View('email.account_request', [ 'data' => $inputs ]);
        
        
        $data = "Account requested successfully";

        
        Mail::send('email.account_request',[ 'data' => $inputs ] , function ($message) use ($inputs) {
            $message->from(env('APP_EMAIL'), env('APP_NAME'));
            //$message->to($request->input('email'));
            $message->to($inputs['email']);
            $message->sender(env('APP_EMAIL'), env('APP_NAME'));
            $message->subject('FJG Direct - Request for Account');
        });
        
        
        Mail::send('email.account_request_admin',[ 'data' => $inputs ], function ($message) use ($request) {
            $message->from(env('APP_EMAIL'), env('APP_NAME'));
            $message->to(env('APP_EMAIL_ADMIN'));
            $message->sender(env('APP_EMAIL'), env('APP_NAME'));
            $message->subject('FJG Direct - Request for Account Activation');
        });
        
        

        return $this->response->array(compact('data'));
        
    }
    
    public function getUpdates(Request $request)
    {
        
        $data = $this->getCounts($request);
        
        
        $user_id = Auth::id();
        $request['user_id'] = $user_id; 

        if($user_id)
        {
          $exist_user =  Getupdates::where('user_id','=',$user_id)->first();
          $request_data = $request->except(['token']);
          
          if(!$exist_user){
          Getupdates::create($request->all());        
          }else{
            Getupdates::where('user_id','=',$user_id)->update($request_data);   
          }
            
        }

        return $this->response->array(compact('data'));
        
        
    }


    public function getCounts($request)
    {   


        $legal_alert_datetime = $request['legal_alert'] ? $request['legal_alert'] : $request->input('legal_alert');
        // $legal_alert_datetime = $request->input('legal_alert');
        $count_legel_alert = LegalAlert::where('updated_at','>',$legal_alert_datetime)->count();
        
        
        // $quick_reference = $request->input('quick_reference');
        $quick_reference = $request['quick_reference'] ? $request['quick_reference'] : $request->input('quick_reference');
        $count_quick_reference = QuickReferenceGuide::where('updated_at','>',$quick_reference)->count();
        
        // $track_matter = $request->input('track_matter');
        $track_matter = $request['track_matter'] ? $request['track_matter'] : $request->input('track_matter');

        $count_track_matter = LegalMatter::where('updated_date','>',$track_matter)->where(['closed'=>'0'])->count();
        
        $data = [];
        $data['legal_alert'] = $count_legel_alert;
        $data['quick_reference'] = $count_quick_reference;
        $data['track_matter'] = $count_track_matter;

        return $data;
    }
    
    public function getPhoneNumberVerification(Request $request)
    {
        $user = Auth::user();
         
        if($user)
        {
            $phone_verification = $user->phone_verification;
            
            $data = [];
            $data['phone_verification'] = (bool) $phone_verification;
            
            return $this->response->array(compact('data'));
            
        }    
         
         
    }
    
    public function testPush(Request $request)
    {   
       Event::fire(new SendPushNotification('Number Of Badges You have....',1)); 
       // Event::fire(new SendPushNotification('',0));          

    }    
    
    public function views()
    {
        $data = [];
        
        
        $data = \App\AppointmentRequest::first()->toArray();
        
        
        return View('email.appointment_request_user',compact('data'));
    }


    
}