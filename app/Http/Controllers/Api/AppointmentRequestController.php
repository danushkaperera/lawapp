<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;


use App\Http\Requests;
use Exception;
use App\Lawyer;
use Illuminate\Support\Facades\Mail;

use App\AppointmentRequest;
use App\AppointmentRequestUser;



class AppointmentRequestController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $lawyer_ids = $request->input('lawyer_ids');
        
      
        
        if(!$lawyer_ids || count($lawyer_ids) == 0 )
        {
            return $this->response->errorBadRequest('at least one lawyer is required');
        }
        
        try {
        
        $inputs = $request->only
        ([
             'subject', 'email', 'datetime', 'phone', 'company', 
        ]);
        
        
        $appointment_request = AppointmentRequest::create($inputs);
        
      
        foreach($lawyer_ids as $lawyer_id)
        {
            AppointmentRequestUser::create
            ([
                'lawyer_id' => $lawyer_id,
                'appointment_request_id' => $appointment_request->id
            ]);
        }
        
        } catch ( Exception $e){
            return $this->response->errorBadRequest($e->getMessage());
        }
        
       
        
        $data = [];
        $data['message'] = 'appointment request saved successfully';
        
        $emails = Lawyer::findMany($lawyer_ids,['email']);
        
        if($emails != null && ($emails->count()>0))
        {
            $emails = array_flatten($emails->toArray());
            
            Mail::send('email.appointment_request', [ 'data' => $inputs ], function ($message) use ($emails) {
                $message->from(env('APP_EMAIL'), env('APP_NAME'));
                $message->to($emails);
                $message->sender(env('APP_EMAIL'), env('APP_NAME'));
                $message->subject('FJG Direct - Appointment Request');
            });
           
        }
        
        
        $full_name = Lawyer::findMany($lawyer_ids,['full_name']);
        $full_names = array_flatten($full_name->toArray());
        
        if(count($full_names) == 0)
        {
            $inputs['has'] = "has";
        }
        else {
            $inputs['has'] = "have";
        }
        
        $inputs['lawyers'] = implode($full_names, ", ");
        
       // dd($inputs);
        
    // return View('email.appointment_request_user', [ 'data' => $inputs ]);
    
          
        Mail::send('email.appointment_request_user', [ 'data' => $inputs ], function ($message) use ($request) {
                $message->from(env('APP_EMAIL'), env('APP_NAME'));
                $message->to($request->input('email'));
                $message->sender(env('APP_EMAIL'), env('APP_NAME'));
                $message->subject('FJG Direct - Appointment Request');
        });
        
        
        
        
        return $this->response->array($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
