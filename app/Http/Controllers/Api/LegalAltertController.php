<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\LegalAlert;
use Carbon\Carbon;

class LegalAltertController extends BaseController
{
    
    public function __construct() 
    {
       $this->middleware('auth:api');
    }
    
    
    public function all()
    {
        $data = [];
        
        $data['data'] = LegalAlert::orderBy('updated_at', 'desc')->with('type')->where('created_at','>=',Carbon::now()->subMonths(6)->toDateTimeString())->get() ? : [];
        return $this->response->array($data);
    }
}
