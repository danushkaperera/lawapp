<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\LegalMatter;
use App\LegalMatterUser;
use Auth;
use Carbon\Carbon;

class CaseController extends BaseController {

	public function __construct() {
		$this->middleware('auth:api');
	}

	public function all() {
		$user = Auth::user();

		$matter_ids = LegalMatterUser::where('user_id', $user->id)->get()->map(function ($i) {return $i->legalMatter->id;})->toArray();

		$data = [];
		$data['data'] = legalMatter::with(['type'])->orderBy('updated_date', 'desc')->where('created_at', '>=', Carbon::now()->subMonths(6)->toDateTimeString())->where('closed', '=', 0)->find(array_values($matter_ids), ['id', 'name', 'type_id', 'updated_date', 'next_date']) ?: [];

		// update /////
		/////
		$txt = new \stdClass;
		$txt->value = "";

		foreach ($data['data'] as $k => $v) {
			$val = $v->legalMatterNotes();

			$val->each(function ($l) use ($txt) {
				$txt->value = $txt->value . explode(" ", $l['date'])[0] . "\n" . $l['note'] . "\n\n";
			});

			$v['type']['type'] = $v['type']['type'] . "\n" . $txt->value;
			$data['data'][$k] = $v;

		}
		///
		/////////

		return $this->response->array($data);
	}

	public function item($id) {

		$user = Auth::user();

		$matter_ids = LegalMatterUser::where('user_id', $user->id)->get()->map(function ($i) {return $i->legalMatter->id;})->toArray();

		if (!in_array($id, $matter_ids)) {
			return $this->response->errorBadRequest("can't find the case matter");
		}

		$legal_matter = LegalMatter::with
		([
			'type',
			'legalMatterNotes',
			'users.user',
		])->find($id);

		if (!$legal_matter) {
			return $this->response->errorNotFound("can't find the requested case");
		}

		$rels = $legal_matter->toArray()['users'];

		$data = [];
		$data['data'] = $legal_matter->toArray();
		$rels = $data['data']['users'];
		$data['data']['users'] = [];

		foreach ($rels as $rel) {
			$data2 = $rel['user'];
			$data2['email'] = $rel['email'];
			$data2['user_type'] = $rel['user_type'];
			$data['data']['users'][] = $data2;
		}

		return $this->response->array($data);

	}

}
