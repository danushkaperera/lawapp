<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\QuickReferenceGuide;

class QRGController extends BaseController
{
   
    public function __construct() 
    {
        $this->middleware('auth:api');
    }
     
    
    public function all()
    {
        $data = [];
        $data['data'] = QuickReferenceGuide::where('publish','1')->orderBy('priority', 'asc')->get() ? : [];
        return $this->response->array($data);
    }
}
