<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Requests\CreateLegalAlertRequest;
use App\Http\Requests\UpdateLegalAlertRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LegalAlertType;
use App\LegalAlert;
use App\User;
use Event;
use App\Events\SendPushNotification;

class LegalAlertController extends Controller
{

 public function __construct() 
    {
      $this->middleware('auth:admin', ['except' => ['login', 'login_request'] ]);
      
    }
      

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $legal_alerts = LegalAlert::all();
        
     //   return view('web.admin.legal-alerts-manager')->with($legal_alerts);
         return view('web.admin.legal-alerts-manager');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

         $legal_alert_type = array(''=>'select'); 
         $arrr = LegalAlertType::all();
      
        for ($i = 0; $i < count($arrr); $i++) {
            $legal_alert_type[$arrr[$i]->id] = $arrr[$i]->type;
        }
      
       $legal_alerts = new LegalAlert;
        return view('web.admin.create-legal-alert',['alert'=>$legal_alerts,'types' => $legal_alert_type ]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateLegalAlertRequest $request)
    {  //dd($request);
        $inputs = $request->all();
        
        LegalAlert::create($inputs);
        Event::fire(new SendPushNotification('You Have A New Legal Alert',1)); 
        $users = User::all(['device', 'push_id']);
       
        
        
        return view('web.admin.legal-alerts-manager');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $legal_alert = LegalAlert::find($id);
        $legal_alert->delete();
        // redirect
      // Session::flash('message', 'Successfully deleted the lawyer!');
       return redirect('web/legal-alert');
     //   return Redirect::to('web/legal-alert');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $legal_alert_type = array(''=>'select'); 
         $arrr = LegalAlertType::all();
      
        for ($i = 0; $i < count($arrr); $i++) {
            $legal_alert_type[$arrr[$i]->id] = $arrr[$i]->type;
        }

        $legal_alert = LegalAlert::find($id);
        
        return view('web.admin.edit-legal-alert')->with(['alert'=>$legal_alert, 'types' => $legal_alert_type ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateLegalAlertRequest $request, $id)
    {   //dd($request);
        $legal_alert = LegalAlert::find($id);        
        $inputs = $request->all();        
        $legal_alert->update($inputs);
       //Event::fire(new SendPushNotification('Updated A Legal Alert',1));
        return redirect('web/legal-alert');
     //  return view('web.admin.legal-alerts-manager');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
   
         public function destroy($id)
    {
        // delete
        $legal_alert = LegalAlert::find($id);
        $legal_alert->delete();
        // redirect
        Session::flash('message', 'Successfully deleted the lawyer!');
        return Redirect::to('web/legal-alert');
       
    }
    
     public function legalAlertRequestAll()
    {
        $legal_alert = LegalAlert::orderBy('id', 'desc')->get();
        return response()->json($legal_alert);
    }
    
   
}
