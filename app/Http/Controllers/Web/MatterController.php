<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\LegalMatter;
use App\FirmUser;
use App\Lawyer;
use App\LegalMatterNote;
use App\CorporateUser;
use App\LegalMatterUser;
use App\Http\Requests;
use App\Http\Requests\CreateMatterRequest;
use App\Http\Controllers\Controller;
use DB;
use Event;
use App\Events\SendPushNotification;

class MatterController extends Controller
{

 public function __construct() 
    {
    //  $this->middleware('auth:admin', ['except' => ['login', 'login_request'] ]);
      
    }

     public function index()
    {
       
        $legal_maters = LegalMatter::all();
        return view('web.admin.matter-manager');
      //return view('web.admin.update-matter');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $lawyers = array(''=>'select'); 
      $arrr =Lawyer::all();
      
        for ($i = 0; $i < count($arrr); $i++) {
            $lawyers[$arrr[$i]->full_name] = $arrr[$i]->full_name;
        }
        
        $firmusers = array(''=>'select');
        $arr = FirmUser::all();
       
        for ($i = 0; $i < count($arr); $i++) {
            $firmusers[$arr[$i]->id] = $arr[$i]->name;
        }
         $corporateuser =array(''=>'select');
          $arry = CorporateUser::all();
          for ($j = 0; $j < count($arry); $j++) {
            $corporateuser[$arry[$j]->company] = $arry[$j]->company;
        }
     
     
        return view('web.admin.create-matter', ['leadlawyer'=>$firmusers, 'lawyer'=>$lawyers, 'companyname'=>$corporateuser]);
    }
    
    

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
       //
        dd($request);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
      public function changeStatus($id)
    {
       //
        dd($id);
    }
     
    public function show($id)
    {  //dd($id);
      //  $legal_maters = LegalMatter::find($id);
      //  return view('web.admin.create-legal-matter')->with($legal_maters);
        
    }
 public function close($id){
     //dd($id);
     $legal_maters = LegalMatter::find($id);
        $legal_maters->update(['closed'=>'1']);
        
 }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        
    
        $firmusers = array(''=>'select');
        $arr = FirmUser::all();
       
        for ($i = 0; $i < count($arr); $i++) {
            $firmusers[$arr[$i]->id] = $arr[$i]->name;
        }
        
        $corporateuser =array(''=>'select');
        $arry = CorporateUser::all();
        for ($j = 0; $j < count($arry); $j++) {
            $corporateuser[$arry[$j]->company] = $arry[$j]->company;
        }
        
     
        $legal_matter = LegalMatter::find($id);
          $firmuserselect = FirmUser::find($id);
        return view('web.admin.edit-matter', ['leadlawyer'=>$firmusers,'selectedlawyer'=>$firmuserselect, 'matter'=>$legal_matter , 'companyname'=>$corporateuser]);
     
    }
    
    

      public function requestsAll()
        {
             $legal_maters = LegalMatter::with(['type'])->where('closed','0')->get();
            return response()->json($legal_maters);
        }
        
        
        
         public function corporateUsersAll(Request $request)
        {
            
     //   $corporateuser = CorporateUser::where('company',$request->represent_company)->get(['id','name']);
        // $lawyers = FirmUser::all(['id','name']);
         $corporateuser = CorporateUser::with(['users'])->where('company',$request->represent_company)->get();
            
            $data = array();
            
            foreach($corporateuser as $e)
            {
                $data[] = ['id'=>$e->users[0]->id, 'name'=>$e->name ];
            }

            return response()->json(['users'=>$data ]);
        }
        
        public function firmUsersAll(Request $request)
        {
            $lawyers = FirmUser::with(['users'])->get();
            
            $data = array();
            
            foreach($lawyers as $e)
            {
                $data[] = ['id'=>$e->users[0]->id, 'name'=>$e->name ];
            }

            return response()->json(['lawyers'=>$data ]);
            // echo "hello";
        //   $lawyers = FirmUser::all();
           
        //     return response()->json(['lawyers'=>$lawyers ]);
        }
        
        
    public function getMetter($id)
    {
        $legal_mater = LegalMatter::find($id);
        
        $data = [];
        
        if($legal_mater)
        {
        
            $legalMatterUsers = $legal_mater->legalMatterUsers()->with(['user.user'])->get();
            
            //dd($legalMatterUsers->toArray());
            
            $clients = [];
            $lawyers = [];
            
            foreach($legalMatterUsers as $u)
            {
                if($u->user->user_type == "App\CorporateUser")
                {
                    $clients[] = ['id'=>$u->user_id, "name"=> $u->user->user->name];
                }
                else 
                {
                    
                    $lawyers[] = ['id'=>$u->user_id, "name"=> $u->user->user->name];
                }
            }
            
            $data = $legal_mater->toArray();
            
            $data['clients'] = $clients;
            $data['lawyers'] = $lawyers;
            
        }
        
        
        return response()->json(['data'=>$data]);
        
      
        
    }
        
   
    public function update(Request $request, $id)
    {
         
       
        $inputs = $request->only(
          "name",
          "type_id" ,
          "lead_lawyer_id",
          "represent_company");
         
         $inputs = array_add($inputs, "updated_date", date('Y-m-d H:i:s'));
         $inputs = array_add($inputs, "closed", '0');
         
         $legal_matter = LegalMatter::find($id);
         
         if($legal_matter)
         {
            $legal_matter->update($inputs);
        
            $ids = $request->input('user_ids');
        
            $data = [];
            
            foreach($ids as $id)
            {
               $data[] = array('legal_matter_id'=>$legal_matter->id, 'user_id'=>$id);
               Event::fire(new SendPushNotification('Updated a Legal Matter Note',1,$id));
            }

            $legal_matter->legalMatterUsers()->delete();
            
          
            $legalMatterUser = new LegalMatterUser();
            $legalMatterUser->insert($data);
            //LegalMatterUser::insert($data);
            
          
            
         }

        // $legal_maters = LegalMatter::find($id);
        
        // $inputs = $request->all();
        
        // $legal_maters->update($inputs);

        // return view('web.admin.matter-manager')->with($legal_maters);
    }
    
    public function submitUsersAll(Request $request)
    {//dd($request);
       $inputs = $request->only(
          "name",
          "type_id" ,
          "lead_lawyer_id",
          "represent_company");
         // dd($inputs);
         $inputs = array_add($inputs, "updated_date", date('Y-m-d H:i:s'));
         $inputs = array_add($inputs, "closed", '0');
         
         $legal_matter = LegalMatter::create($inputs);
         
         if($legal_matter)
         {
            $ids = $request->input('user_ids');
            $legalMatterUser = new LegalMatterUser();
            $data = [];
            
            foreach($ids as $id)
            {
               $data[] = array('legal_matter_id'=>$legal_matter->id, 'user_id'=>$id);
                Event::fire(new SendPushNotification('You Have A New Legal Matter Note',1,$id));
            //LegalMatterUser::insert($data);
             }
            $legalMatterUser->insert($data);
//dd( $data);
 
            // $legalMatterUser = new LegalMatterUser(['legal_matter_id'=>$legal_matter->id]);

            // $legalMatter = LegalMatter::find($legal_matter->id);
            // $legalMatterUser->save();
        //    $legalMatter->legalMatterUsers()->save($legalMatterUser);


         }
         
        
        


    }

   //radio button filter lawyers
    
public function lawyerRequestAll(Request $request){
  
      $divitionLawyer = FirmUser::where('division_id',$request->type_id)->get(['id','name']);
         return response()->json(['users'=>$divitionLawyer]);
}





}
