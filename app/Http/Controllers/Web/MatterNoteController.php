<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\CreateMatterNoteRequest ;
use App\User;
use App\LegalMatter;
use App\FirmUser;
use App\Lawyer;
use App\LegalMatterNote;
use App\CorporateUser;
use App\Http\Requests;
use App\LegalMatterUser;
use App\Http\Controllers\Controller;
use Event;
use App\Events\SendPushNotification;
class MatterNoteController extends Controller
{
    //
public function __construct() 
    {
      $this->middleware('auth:admin', ['except' => ['login', 'login_request'] ]);
      
    }
  


      public function index()
    {
        //
    }


    public function create()
    {
        //
    }



    public function store(CreateMatterNoteRequest $request)
    {
        
        
    
     $input = $request->except('name','company','type_id','id');
    
     $legal_matter_note = LegalMatterNote::create($input);
     if($legal_matter_note)
         {
        $nextdate = $request->input('next_date');
        $legal_maters = LegalMatter::find($request->input('legal_matter_id'));
        $leadLawyerData = $legal_maters->lead_lawyer()->first();

        $userLeadLawyer = User::where('user_id','=',$leadLawyerData->id)->where('user_type','=','App\FirmUser')->first();
       if($userLeadLawyer->id){

 	Event::fire(new SendPushNotification('You Have A New Legal Matter Note',1,$userLeadLawyer->id));

}
        $legal=  $legal_maters->update(['next_date' => $nextdate]);
        $legalMatterUsers = LegalMatterUser::where('legal_matter_id','=',$legal_maters->id);

        foreach ($legalMatterUsers as $key => $legalMatterUser) {
     Event::fire(new SendPushNotification('You Have A New Legal Matter Note',1,$legalMatterUser->user_id));
        }
  
      return redirect('web/matter-management');
      
         }
     
        
    }



    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        
        $corporateuser = CorporateUser::find($id);
        $legal_matter_note = new LegalMatterNote;
        $legal_matter =LegalMatter::where('id',$id)->first();
        return view('web.admin.create-matter-note', ['matternote'=>$legal_matter_note,'matter'=>$legal_matter,'lawyer'=>$legal_matter->name, 'companyname'=>$corporateuser]);
     
    }
public function viewupdateNote($id)
    {
       // dd($id);
         $lawyer = Lawyer::find($id);
            $legal_matter =LegalMatter::find($id);
        $legal_matter_note = LegalMatterNote::where('legal_matter_id','=',$id )->get();
         return view('web.admin.update-matter',['legal_matter_note'=>$legal_matter_note,'matter'=>$legal_matter,'lawyers'=>$lawyer]);
    
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
