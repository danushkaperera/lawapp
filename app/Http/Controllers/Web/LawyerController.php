<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Requests\CreateLawyerRequest;
use App\Http\Requests\UpdateLawyerRequest;
use App\LawyerDivision;
use App\Lawyer;
use App\User;
use App\FirmUser;
use Redirect;
use DB;
use App\Http\Controllers\Controller;

class LawyerController extends Controller
{

 public function __construct() 
    {
      $this->middleware('auth:admin', ['except' => ['login', 'login_request'] ]);
      
    }
  

      public function index()
    {
        $lawyer = Lawyer::all();
      //  $division=LawyerDivision::all();
      // return view('web.admin.lawyer.lawyer-directory')->with($lawyer);
        return view('web.admin.lawyer.lawyer-directory');
    }

    
    public function create()
    { 
        $lawyer = new Lawyer;
    //   $lawyer->email = "";
        $divisions = array('Select');
        
        $arr = LawyerDivision::all();
        
        for ($i = 0; $i < count($arr); $i++) {
            $divisions[$arr[$i]->id] = $arr[$i]->division;
        }
            

         return view('web.admin.lawyer.create-lawyer-user')->with(['lawyrs'=>$lawyer, 'divisions'=>$divisions]);
    }

    
    public function store(CreateLawyerRequest $request)
    { ///dd($request);
        $inputs = $request->all();
             
        if ($request->hasFile('image')) {
            $inputs = array_add($inputs, "image", $request->file('image'));
        }
        // Lawyer::create($inputs);
        $lawyer = Lawyer::create($inputs);

        
        $user_inputs = [];
        $user_inputs['name'] = $inputs['full_name'];
        $user_inputs['designation'] = $inputs['designation'];
        $user_inputs['division_id'] = $inputs['division_id'];
        $user_inputs['phone'] = $inputs['direct_phone'];
        $user_inputs['status'] = 'active';
        
        $firm_user = FirmUser::create($user_inputs);
        $firm_user->users()->save(User::create($request->only('email','password')));
        
        return redirect('web/lawyers');
       
    }

    
    
    
    public function edit($id)
    {

       $divisions = array();
        $arr = LawyerDivision::all();
        for ($i = 0; $i < count($arr); $i++) {
            $divisions[$arr[$i]->id] = $arr[$i]->division;
        }
          $lawyer = Lawyer::find($id);
       //  $userpas= User::find($id);
          return view('web.admin.lawyer.edit-lawyer-user')->with(['lawyar'=>$lawyer, 'divisions'=>$divisions ]);

    }

    public function update(UpdateLawyerRequest $request,$id)
    {
           
        $lawyers = Lawyer::find($id);
        $inputs = $request->all();
        if ($request->hasFile('image')) {
            $inputs = array_add($inputs, "image", $request->file('image'));
        }
        
           $lawyers->update($inputs);
         
         
       $user_inputs['name'] = $lawyers->full_name;
        $user_inputs['designation'] = $lawyers->designation;
        $user_inputs['division_id'] = $lawyers->division_id;
        $user_inputs['phone'] = $lawyers->direct_phone;
        $user_inputs['email'] = $lawyers->email;
        $user_inputs['password'] = $lawyers->password;
        $user_inputs['status'] = 'active';
       // $firm_user=FirmUser::where('lawyer_id',$lawyers->id)->update($user_inputs);

     ///  $login_details = $request->only('email','password');
        
         if($user_inputs['password'] == ""|| $user_inputs['password'] == "xxxxx")
         {
          unset($user_inputs['password']);
         }      

     $fuser=   DB::table('firm_users')
     ->join('users', 'firm_users.id', '=', 'users.user_id')
     ->where('lawyer_id',$lawyers->id)
     ->update($user_inputs);
     
         
            return redirect('web/lawyers');
    }
    
    public function lawyersAll()
    {
    
        $lawyers = Lawyer::with('division')->orderBy('id', 'desc')->get();
        return response()->json($lawyers);
    }
    
    public function destroy($id)
    {
          $lawyers = Lawyer::find($id);
       
        $user_inputs = [];
        $user_inputs['name'] = $lawyers->full_name;
        $user_inputs['designation'] = $lawyers->designation;
        $user_inputs['division_id'] = $lawyers->division_id;
        $user_inputs['phone'] = $lawyers->direct_phone;
         $user_input=$lawyers->email;
     
        $lawyers->delete();
        
        $firm_user = FirmUser::where($user_inputs)->first();
  
        $firm_user->delete();

      $firm= $firm_user->users()->first();
       $firm->delete();
        
        

       //  return view('web.admin.lawyer.lawyer-directory');
     //  Session::flash('message', 'Successfully deleted the lawyer!');
       return Redirect::to('web/lawyers');
       
    }
  public function createAccounts()
    {
        $lawyers = [];
        
        $lawyersAll = Lawyer::all();
        
        foreach ($lawyersAll as $lawyer) {
            
            $user_inputs = [];
            $user_inputs['name'] = $lawyer->full_name;
            $user_inputs['designation'] = $lawyer->designation;
            $user_inputs['division_id'] = $lawyer->division_id;
            $user_inputs['phone'] = $lawyer->direct_phone;
            
            $firm_user = FirmUser::where($user_inputs)->first();
            
            if($firm_user == null)
            {
                $user_inputs['status'] = 'active';
                $firm_user = FirmUser::create($user_inputs);
                $firm_user->users()->save(User::create(['email'=>$lawyer->email,'password'=>'1234']));
                $lawyers[] = $firm_user;
            }
            else if($firm_user->user == null)
            {
                
                $user = User::where(['email'=>$lawyer->email])->first();
                if($user != null && $user->user_type == "App\FirmUser")
                {
                    $user->delete();
                }
                
                $firm_user->users()->save(User::create(['email'=>$lawyer->email,'password'=>'1234']));
                $lawyers[] = $firm_user;
            }
            
        }
        
        
        return response()->json($lawyers);
    }

}
