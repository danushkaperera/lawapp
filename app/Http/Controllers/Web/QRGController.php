<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Requests\CreateQuickReferenceRequest;
use App\Http\Requests\UpdateqrgRequest;
use App\QuickReferenceGuide;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Event;
use App\Events\SendPushNotification;

class QRGController extends Controller
{

 public function __construct() 
    {
      $this->middleware('auth:admin', ['except' => ['login', 'login_request'] ]);
      
    }
  

      /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
    
        $quick_reference_guide = QuickReferenceGuide::all();
        
     //   return view('web.admin.quick-reference-guide')->with($quick_reference_guide);
       return view('web.admin.quick-reference-guide');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        $quick_reference_guide = new QuickReferenceGuide;

        return view('web.admin.create-qrg', ['qrg'=>$quick_reference_guide]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateQuickReferenceRequest $request)
    {
        //
        $inputs =$request->all();
        
        QuickReferenceGuide::create($inputs);
     //   Event::fire(new SendPushNotification('You Have A New Quick Reference Guide',1)); 
        return redirect('web/quick-reference-guide');
     
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
          $quick_reference_guide = QuickReferenceGuide::find($id);
        $quick_reference_guide->delete();
        // redirect
       // Session::flash('message', 'Successfully deleted the lawyer!');
           return redirect('web/quick-reference-guide');
     //   return Redirect::to('web/quick-reference-guide');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      //
          $quick_reference_guide = QuickReferenceGuide::find($id);
          return view('web.admin.edit-qrg')->with(['qrg'=>$quick_reference_guide]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateqrgRequest $request, $id)
    {
        
        //
         $quick_reference_guide = QuickReferenceGuide::find($id);
         
         $inputs = $request->all();
         
         $quick_reference_guide->update($inputs);
         //Event::fire(new SendPushNotification('Updated A Quick Reference Guide',1)); 
         return redirect('web/quick-reference-guide');
       //  return view('web/quick-reference-guide');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
         public function destroy($id)
    {
        // delete
        $quick_reference_guide = QuickReferenceGuide::find($id);
        $quick_reference_guide->delete();
        // redirect
        Session::flash('message', 'Successfully deleted the lawyer!');
        return Redirect::to('web/quick-reference-guide');
       
    }
    
      public function qrgRequestAll()
    {
        $quick_reference_guide = QuickReferenceGuide::orderBy('priority', 'asc')->get();
        return response()->json($quick_reference_guide);
    }
}
