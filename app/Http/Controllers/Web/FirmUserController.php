<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Requests\CreateFirmUserRequest;
use App\Http\Requests\UpdateFirmUserRequest;

use App\Http\Controllers\Controller;

use App\AccountRequest;
use App\FirmUser;
use App\User;



class FirmUserController extends Controller
{
public function __construct() 
    {
      $this->middleware('auth:admin', ['except' => ['login', 'login_request'] ]);
      
    }
  
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $firm_users = FirmUser::all();
        
        return view('web.admin.client-directory')->with($firm_users);
       //  return view('web.admin.client-directory');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('web.admin.create-user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateFirmUserRequest $request)
    {
        $inputs = $request->all();
   
        $firm_user = FirmUser::create($inputs);
        
        $firm_user->users()->save(User::create($request->only('email','password')));
        
        
        
        //return view('');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $firm_user = FirmUser::find($id);
        
        return view('')->with($firm_user);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $firm_user = FirmUser::find($id);
        
        return view('')->with($firm_user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateFirmUserRequest $request, $id)
    {
        $firm_user = FirmUser::find($id);
        
        $inputs = $request->all();
        
        $firm_user->update($firm_user);
        
        $login_details = $request->only('email','password');
        
        if($login_details['password'] == "")
        {
         unset($login_details['password']);
        }
        
        $firm_user->users()->first()->update($login_details);


        return view('')->with($firm_user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $firm_user = FirmUser::find($id);
        $firm_user->delete();

        return view('');
    }
    
}
