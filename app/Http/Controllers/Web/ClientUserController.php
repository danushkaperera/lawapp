<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Requests\CreateCorporateUserRequest;
use App\Http\Requests\UpdateCorporateUserRequest;
use Session;
use App\Http\Controllers\Controller;

use App\AccountRequest;
use App\CorporateUser;
use App\User;


class ClientUserController extends Controller
{
     public function __construct() 
    {
      $this->middleware('auth:admin', ['except' => ['login', 'login_request'] ]);
      
    }
  
    
     /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $corporate_users = CorporateUser::all();
        //return view('web.admin.client.client-directory')->with(['clients'=>$corporate_users]);
      return view('web.admin.client.client-directory');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    { 
        
      //return view('web.admin.client.create-client-user');
      //  return view('web.admin.client.create-client-user',['clients'=>$corporate_users]);
       //return view('web.admin.client.create-client-user',['client'=>$account_request]);
        //$corporate_user = CorporateUser::with('users')->find(1); 
        $corporate_user = new CorporateUser;
       return view('web.admin.client.create-client-user',['client'=>$corporate_user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CreateCorporateUserRequest $request)
    { //dd($request);
        $inputs = $request->all();
   
        $corporate_user = CorporateUser::create($inputs);
        
        $corporate_user->users()->save(User::create($request->only('email','password')));
        
        $account_request = AccountRequest::find($request->input('id'));
      
       if($account_request){
            $account_request->update(['status'=>'approved']);
        }
        
        
        
        
        return redirect('web/clients');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
      //dd($id);
        
         $corporate_user = CorporateUser::find($id);
         $corporate_user->delete();
         Session::flash('message', 'Successfully deleted the lawyer!');
       return redirect('web/clients');
        
        // $corporate_user = CorporateUser::find($id);
        
        // return view('')->with($corporate_user);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $corporate_user = CorporateUser::with('users')->find($id); 
       
        return view('web.admin.client.edit-client-user')->with(['client'=>$corporate_user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateCorporateUserRequest $request, $id)
    {
        $corporate_user = CorporateUser::find($id);
        
        $inputs = $request->all();
        
       
        
        $corporate_user->update($inputs);
        
        $login_details = $request->only('email','password');
        
        if($login_details['password'] == "")
        {
         unset($login_details['password']);
        }
        
        $corporate_user->users()->first()->update($login_details);

       // return view('web.admin.client.edit-client-user')->with(['client'=>$corporate_user]);
        return redirect('web/clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        
        dd($id);
         $corporate_user = CorporateUser::find($id);
         $corporate_user->delete();
         Session::flash('message', 'Successfully deleted the lawyer!');
       return redirect('web/clients');
      //   return view('');
    }
    

    
    public function accountIndex()
    {
        return view('web.admin.account-requests');
    }
    
    public function accountRequestAll()
    {
        $account_requests = AccountRequest::orderBy('id', 'desc')->where(['status'=> 'pending'])->get();

        return response()->json($account_requests);
    }
    
    
    public function accountDeclined($id)
    {
        $account_request = AccountRequest::find($id);
        $account_request->update(['status'=>'declined']);
            return redirect('web/client/requests');

       //  return response()->json($account_request);
    }
        
        
    public function accountApproved($id)
    {
        $account_request = AccountRequest::find($id);
        $account_request_id = $id;
        
        return view('web.admin.client.create-client-user',['client'=>$account_request, 'account_request_id'=>$account_request_id]);
    }
    
    public function clientRequestAll()
    {
        $corporate_users = CorporateUser::orderBy('id', 'desc')->get();
        return response()->json($corporate_users);
    }
}