<?php



/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('api/cases','CaseController@all');

//'middleware' => 'auth:api',


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

$api = app('Dingo\Api\Routing\Router');


$api->version('v1', function ($api) {
    
    $basePath = "App\Http\Controllers\Api\\";
    
    $api->post('/login', $basePath.'UserController@login');
    $api->get('/login_verify', $basePath.'UserController@loginVerify');
    $api->post('/recover_password', $basePath.'UserController@recoverPassword');
    $api->post('/request_account', $basePath.'UserController@requestAccount');
    $api->post('/get_updates', $basePath.'UserController@getUpdates');
    $api->get('/get_profile', $basePath.'UserController@getProfile');
    $api->get('/lawyers', $basePath.'LawyerController@all');
    $api->get('/lawyers_android', $basePath.'LawyerController@all_android');
    $api->get('lawyer/all_divisions', $basePath.'LawyerController@getAllDivisions');
    $api->get('/lawyer/{id}', $basePath.'LawyerController@item')->where(array('id' => '[0-9]+'));
    $api->get('/lawyer/search', $basePath.'LawyerController@search');
    $api->post('/lawyer/request_appoinment', $basePath.'AppointmentRequestController@create');
    $api->get('/qrg', $basePath.'QRGController@all');
    $api->get('/legal_alterts', $basePath.'LegalAltertController@all');
    $api->post('/company/setup', $basePath.'ConpanyController@setup');
    $api->get('/cases', $basePath.'CaseController@all');
    $api->get('/case/{id}', $basePath.'CaseController@item')->where(array('id' => '[0-9]+'));
    
    $api->get('/testpush', $basePath.'UserController@testPush');
    $api->get('/view', $basePath.'UserController@views');


});



Route::get('/', array('uses' => 'Web\AdminController@index'));
//web
$web = "Web\\";
Route::get('web/dashboard', [
    'as' => 'dashboard', 'uses' => $web.'AdminController@dashboard'
]);
Route::get('web/dashboard2', [
    'as' => 'dashboard', 'uses' => $web.'AdminController@dashboard2'
]);
Route::get('web/login', array( 'as' => 'login_page', 'uses' => $web.'AdminController@login'));
Route::post('web/login', array('uses' => $web.'AdminController@login_request'));
Route::get('web/logout', array( 'as' => 'logout_page', 'uses' => $web.'AdminController@logout'));

Route::resource('web/quick-reference-guide', $web.'QRGController');
Route::get('web/quick-reference-guides/request-all', [
    'as' => 'qrg_all_json', 'uses' => $web.'QRGController@qrgRequestAll'
]);


Route::resource('web/legal-alert', $web.'LegalAlertController');
Route::get('web/legal-alerts/request-all', [
    'as' => 'legal-alert_all_json', 'uses' => $web.'LegalAlertController@legalAlertRequestAll'
]);

Route::delete('web/legal-alert/{id}', [
    'as' => 'deletepage', 'uses' => $web.'LegalAlertController@destroy'
]);


Route::resource('web/lawyers', $web.'LawyerController', ['except' => ['destroy']]);
Route::get('web/lawyer/lawyers-all', [
    'as' => 'lawyers_all_json', 'uses' => $web.'LawyerController@lawyersAll'
]);
Route::get('web/lawyers/delete/{id}', [
    'as' => 'delete_lawyers', 'uses' => $web.'LawyerController@destroy'
]);
// this is radio button filter lawyers
Route::post('web/matter-management/request-lawyers', [
    'as' => 'firm_requests_json', 'uses' => $web.'MatterController@lawyerRequestAll'
]);

Route::get('web/lawyer/create', [
    'as' => 'lawyers_all_json', 'uses' => $web.'LawyerController@createAccounts'
]);


Route::resource('web/clients', $web.'ClientUserController');
Route::get('web/client/clients-all', [
    'as' => 'clients_all_json', 'uses' => $web.'ClientUserController@clientRequestAll'
]);
Route::get('web/client/requests', [
    'as' => 'account_requests_all_page', 'uses' => $web.'ClientUserController@accountIndex'
]);
Route::get('web/client/requests-all', [
    'as' => 'account_requests_all_json', 'uses' => $web.'ClientUserController@accountRequestAll'
]);
Route::get('web/client/requests-declined/{id}', [
    'as' => 'account_requests_declined', 'uses' => $web.'ClientUserController@accountDeclined'
]);
Route::get('web/client/create/{id}', [
    'as' => 'account_requests_approved', 'uses' => $web.'ClientUserController@accountApproved'
]);

Route::delete('web/client/{id}', [
    'as' => 'deletepage', 'uses' => $web.'ClientUserController@destroy'
]);

Route::resource('web/matter-management', $web.'MatterController');
Route::post('web/matter-management/request-users-all', [
    'as' => 'corporate_users_requests_json', 'uses' => $web.'MatterController@corporateUsersAll'
]);
Route::post('web/matter-management/request-lawyers-all', [
    'as' => 'firm_users_requests_json', 'uses' => $web.'MatterController@firmUsersAll'
]);
Route::post('web/matter-management/submit-users', [
    'as' => 'corporate_users_requests_json', 'uses' => $web.'MatterController@submitUsersAll'
]);
Route::post('web/matter-management/{id}/updates-users', [
    'as' => 'corporate_users_requests_json', 'uses' => $web.'MatterController@update'
]);
Route::post('web/matter-management/submit-users-all', [
    'as' => 'corporate_users_requests_json', 'uses' => $web.'MatterController@corporateUsersAll'
]);
Route::post('web/matter-management/{id}/close', [
    'as' => 'close matter', 'uses' => $web.'MatterController@close'
]);
Route::post('web/matter-management/change-status', [
    'as' => 'angular request', 'uses' => $web.'MatterController@changeStatus'
]);

Route::resource('web/matter-note', $web.'MatterNoteController');
Route::get('web/matter-note/{id}/viewnote', [
    'as' => 'update_view_note', 'uses' => $web.'MatterNoteController@viewupdateNote'
]);
Route::get('web/matter/requests-all', [
    'as' => 'account_requests_all_json', 'uses' => $web.'MatterController@requestsAll'
]);
Route::get('web/matter-management/{id}/getMetter', [
    'as' => 'get_matter_by_id', 'uses' => $web.'MatterController@getMetter'
]);
