<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LawyerDivision extends Model
{
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'division',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    /**
     * Get the Lawyers for this LawyerDivision.
     */
    public function lawyers()
    {
        return $this->hasMany('App\Lawyer', 'division_id');
    }
}
