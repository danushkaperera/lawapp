<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountRequest extends Model
{
    
    // public static $STATUS_NEW = 'new';
    // public static $STATUS_PROCESSED = 'processed';
    
    public static $status = [
        'Pending'=>'pending',
        'Approved'=>'approved',
        'Declined'=>'declined'
        ];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',	  
        'company',	 
        'email',	  
        'designation',	  
        'phone',	  
        'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    

  
    
}
