<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorporateUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',	  
        'company',	  
        'department',	  
        'designation',	  
        'phone',	  
        'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    /**
     * Get the Designation of this CorporateUser
     */
    public function designation()
    {
        return $this->belongsTo('App\Designation');
    }
    
    /**
     * Get all of the user of CorporateUser type.
     */
    public function users()
    {
        return $this->morphMany('App\User', 'user');
    }
}
