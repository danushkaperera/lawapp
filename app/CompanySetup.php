<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySetup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',	  
        'address',	  

    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    /**
     * Get the CompanySetupDirectors for this CompanySetup.
     */
    public function companySetupDirectors()
    {
        return $this->hasMany('App\CompanySetupDirector');
    }
    
    
    /**
     * Get the CompanySetupShareholders for this CompanySetup.
     */
    public function companySetupShareholders()
    {
        return $this->hasMany('App\CompanySetupShareholder');
    }
    
}
