<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentRequestUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'appointment_request_id', 'lawyer_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    /**
     * Get the AppointmentRequest that own this User.
     */
    public function appointmentRequest()
    {
        return $this->belongsTo('App\AppointmentRequest');
    }
    
    /**
     * Get the lawyers of AppointmentRequest
     */
    public function lawyer()
    {
        return $this->belongsTo('App\Lawyer');
    }
    
}
