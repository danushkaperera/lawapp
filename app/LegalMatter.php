<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalMatter extends Model
{
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'type_id',
        'lead_lawyer_id',
        'represent_company',	  
        'next_date',	  
        'updated_date',	  
        'closed',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       'created_at','updated_at'
    ];
    
    protected $casts = [
        'closed' => 'boolean',
    ];
    
    
    /**
     * Get the LegalMatterNotes for this LegalMatter.
     */
    public function legalMatterNotes()
    {
        return $this->hasMany('App\LegalMatterNote');
    }
    
    /**
     * Get the LegalMatterUsers for the legalMatter.
     */
    public function legalMatterUsers()
    {
        return $this->hasMany('App\LegalMatterUser','legal_matter_id','id');
    } 
    
    public function lead_lawyer()
    {
        return $this->belongsTo('App\FirmUser','lead_lawyer_id','id');
    }
    
    /**
     * Get the LegalMatterType for the legalMatter.
     */
    public function type()
    {
        return $this->belongsTo('App\LegalMatterType');
    }
    
    
    /**
     * Get all of the Users for the legalMatter.
     */
    public function users()
    {
        return $this->hasManyThrough('App\User', 'App\LegalMatterUser', 'legal_matter_id', 'id');
    }
    
    
}
