<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentRequest extends Model
{
    protected $softDelete = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'datetime', 'subject', 'email', 'phone', 'company', 
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       'deleted_at'
    ];
    
     /**
     * Get the Users for the AppointmentRequest.
     */
    public function appointmentUsers()
    {
        return $this->hasMany('App\AppointmentRequestUser');
    }
    
    /**
     * Get all of the Lawyers for the AppointmentRequest.
     */
    public function lawyers()
    {
        return $this->hasManyThrough('App\Lawyer', 'App\AppointmentRequestUser', 'appointment_request_id', 'id');
    }
}
