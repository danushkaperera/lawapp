<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Getupdates extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',	  
        'legal_alert',	  
        'quick_reference',	  
        'track_matter'
    ];

    public $timestamps = false;

}
