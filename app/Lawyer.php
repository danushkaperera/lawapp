<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;


class Lawyer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',  
        'designation',	
        'email',	 
        'division_id', 
        'direct_phone',
        'image',
        'head',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'division_id', 
       
    ];
    
    protected $casts = [
        'head' => 'boolean',
    ];
    
    /**
     * Get the LawyerDivision of this Lawyer
     */
    public function division()
    {
        return $this->belongsTo('App\LawyerDivision');
    }
    
    public function getImageAttribute()
    {
        if( !isset($this->attributes['image']) || $this->attributes['image'] == "")
            return "";
            
        return asset($this->attributes['image']);
    }
    
    
    public function setImageAttribute($image)
    {
        if(is_string($image))
        {
            $this->attributes['image'] = $image;
        }
        else
        {
            if ($image->isValid()) {
             
            $photoDir = 'images/lawyers';

            $extension = $image->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;
            $image->move($photoDir, $fileName);

            if ( isset($this->attributes['image']) &&  $this->attributes['image'] != null) {
                File::Delete( $this->attributes['image'] );
            }

            $this->attributes['image'] = $photoDir . "/". $fileName;
        }
        }
    }
    
    public function setFullNameAttribute($name)
    {
        
        $this->attributes['full_name'] = ucwords($name);
       
    }
    
    /**
     * Get the Designation of this Lawyer
     */
    //public function designation()
    //{
    //    return $this->belongsTo('App\Designation');
    //}
    
    /**
     * Get the AppointmentRequestUser records associated with the user.
     */
    public function appointmentRequestUser()
    {
        return $this->hasMany('App\AppointmentRequestUser');
    }
    
    /**
     * Get all of the AppointmentRequest for the Lawyer.
     */
    public function appointmentRequests()
    {
        return $this->hasManyThrough('App\AppointmentRequest', 'App\AppointmentRequestUser', 'appointment_request_id', 'id' );
    }
}

