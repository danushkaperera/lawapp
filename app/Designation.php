<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'designation',
        'client',
        'lawyer',
        'firm'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'client',
        'lawyer',
        'firm'
    ];
    
    /**
     * Get the Lawyers for this Designation.
     */
    public function lawyers()
    {
        return $this->hasMany('App\Lawyer');
    }
    
    // /**
    //  * Get the CorporateUsers for this Designation.
    //  */
    // public function corporateUsers()
    // {
    //     return $this->hasMany('App\CorporateUser');
    // }
    
    // /**
    //  * Get the FirmUsers for this Designation.
    //  */
    // public function firmUsers()
    // {
    //     return $this->hasMany('App\FirmUser');
    // }
    
    // /**
    //  * Get the AccountRequests for this Designation.
    //  */
    // public function accountRequests()
    // {
    //     return $this->hasMany('App\AccountRequest');
    // }
    
    
}
