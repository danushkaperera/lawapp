<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalMatterUser extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'legal_matter_id', 'user_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       'legal_matter_id'
    ];
    
     /**
     * Get the LegalMatter that own this user.
     */
    public function legalMatter()
    {
        return $this->belongsTo('App\LegalMatter','legal_matter_id','id');
    }
    
    /**
     * Get the user of this LegalMatterUser
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
