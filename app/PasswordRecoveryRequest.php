<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordRecoveryRequest extends Model
{
    protected $softDelete = true;
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       'deleted_at'
    ];
    
    
    
    
    /**
     * Get the user requested password recovery
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
