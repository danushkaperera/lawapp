<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalMatterNote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'legal_matter_id',	  
        'date',	  
        'note',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       'legal_matter_id','created_at','updated_at'
    ];
    
     /**
     * Get the LegalMatter that own this note.
     */
    public function legalMatter()
    {
        return $this->belongsTo('App\LegalMatter');
    }
}
