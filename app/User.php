<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject as JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    
    protected $softDelete = true;
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'device', 'push_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'deleted_at',
    ];
    
    
    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     *
     * @return mixed
     */
    public function getJWTIdentifier(){
        return $this->getKey(); 
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT
     *
     * @return array
     */
    public function getJWTCustomClaims(){
        return [];
    }
    
    
    /**
     * Hashing password in user models.
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make( $password ); 

    }
    
    /**
     * Get all of the owning user models.
     */
    public function user()
    {
        return $this->morphTo();
    }
    
    /**
     * Get the PasswordRecoveryRequest record associated with the user.
     */
    public function passwordRecoveryRequests()
    {
        return $this->hasOne('App\PasswordRecoveryRequests');
    }
    
    
    
    /**
     * Get the legalMatterUser records associated with the user.
     */
    public function legalMatterUsers()
    {
        return $this->hasMany('App\LegalMatterUser');
    }
    
    /**
     * Get all of the LegalMatters for the User.
     */
    public function legalMatters()
    {
        return $this->hasManyThrough('App\LegalMatter', 'App\LegalMatterUser', 'legal_matter_id', 'id' );
    }


     /**
     * Get all of the LegalMatters for the User.
     */
    public function deviceDetect()
    {
       return $this->hasMany('App\DeviceDetect');
    }
    
}
