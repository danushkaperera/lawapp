<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySetupShareholder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_setups_id',	  
        'nic_country',	  
        'nic_id',	  
        'residentail_address_local',	  
        'residentail_address_foregin',	  
        'current_occupation',	  

    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    /**
     * Get the CompanySetup that own this CompanySetupShareholder.
     */
    public function companySetup()
    {
        return $this->belongsTo('App\CompanySetup');
    }
}
