<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceDetect extends Model
{
    
    protected $table = 'device_detect';
     /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'device_name',
        'device_id'
    ];





     /**
     * Get the user of this Devicedetect
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
