<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendPushNotification extends Event
{
    use SerializesModels;
    public $message;
    public $badges;
    public $user_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message='',$badges=0,$user_id=null)
    {
       $this->message = $message;
       $this->badges = $badges;
       $this->user_id = $user_id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
