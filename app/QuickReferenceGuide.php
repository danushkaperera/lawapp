<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuickReferenceGuide extends Model
{
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'publish','priority',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       'publish',
    ];
}
