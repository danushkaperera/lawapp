<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirmUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',	  
        'division_id',	  
        'designation',	  
        'phone',	  
        'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     protected $hidden = [
        'division_id', 
       
    ];
    
    public function division()
    {
        return $this->belongsTo('App\LawyerDivision');
    }
    /**
     * Get the Designation of this FirmUser
     */
    // public function designation()
    // {
    //     return $this->belongsTo('App\Designation');
    // }
    
    /**
     * Get all of the user of CorporateUser type.
     */
    public function users()
    {
        return $this->morphMany('App\User', 'user');
    }
}
