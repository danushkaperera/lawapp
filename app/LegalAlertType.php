<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalAlertType extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    /**
     * Get the legalMatters for the type.
     */
    public function legalAlert()
    {
        return $this->hasMany('App\LegalAlert');
    }
}
