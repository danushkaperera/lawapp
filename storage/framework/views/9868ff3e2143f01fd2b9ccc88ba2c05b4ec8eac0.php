<html ng-app="myApp">
<head>
    <title>LawApp - <?php echo $__env->yieldContent('title'); ?></title>
    <?php echo $__env->make('web.partials._header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
<?php $__env->startSection('sidebar'); ?>
<?php echo $__env->yieldSection(); ?>

<div class="container">
    <?php echo $__env->yieldContent('content'); ?>
</div>
</body>
<?php $__env->startSection('footer'); ?>
    <?php echo $__env->make('web.partials._footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
</html>