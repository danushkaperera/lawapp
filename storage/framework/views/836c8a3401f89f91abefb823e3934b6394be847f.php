 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', 'Create Quick Reference Guide'); ?>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="content">
<h2>Create Quick Reference Guide</h2>
 <button type="button" class="btn btn-sm btn-warning" id="backbtnqrg"  data-toggle="modal" >Go Back</button>
<?php echo e(Form::open(array('url' => 'web/quick-reference-guide'))); ?>

      
       
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   <?php echo e(Form::label('title', 'Title',  ['class' => 'control-label'])); ?></div>
                <div class="col-sm-9">
                     <?php echo e(Form::input('title', 'title',$qrg->title,['class' => 'form-control'])); ?>

                
               <?php if($errors->has('title')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('title')); ?></strong>
                        </span>
                    <?php endif; ?>  
                </div>
            </div>
        </div>
    </div>
   
  
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>     <?php echo e(Form::label('body', 'Notes:',  ['class' => 'control-label'])); ?></div>
                <div class="col-sm-9">
                    <?php echo Form::textarea('body',$qrg->body, ['class' => 'form-control']); ?>

                  
               
                 <?php if($errors->has('body')): ?>
                       <span class="error-block">
                            <strong><?php echo e($errors->first('body')); ?></strong>
                        </span>
                    <?php endif; ?>
 </div>
            </div>
        </div>
    </div>
  
         
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>    <?php echo e(Form::label('created_at', ' Publish Date ')); ?></div>
                <div class="col-sm-9">
                  <?php echo e(Form::input('text', 'created_at', null,['id' => 'datepicker','class' => 'form-control datepicker','data-date-format'=>'y-m-d'])); ?>

                  
               
                   <?php if($errors->has('created_at')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('created_at')); ?></strong>
                        </span>
                    <?php endif; ?> </div>
            </div>
        </div>
    </div>
  
  
   
  <button type="submit" class="btn btn-primary">Submit</button>
<?php echo Form::close(); ?>


</div>
  <script>
  $('.datepicker').datepicker({
    dateFormat: 'yy-mm-dd',
   
});
  </script>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>