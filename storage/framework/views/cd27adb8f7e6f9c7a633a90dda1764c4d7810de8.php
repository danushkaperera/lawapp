 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', 'Create User'); ?>

<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>
<style>
#status, input[type="radio"]{
margin: 0px 0 0 !important;

}
</style>
<?php $__env->startSection('content'); ?>
<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <h1>Add New User</h1>
           <button type="button" class="btn btn-sm btn-warning" id="backbtnclient"  data-toggle="modal" >Go Back</button>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2>Personal Details</h2>
        </div>
    </div>
    <!--<?php echo Form::open(array('url' => 'web/clients','files' => true )); ?>-->
        <?php echo Form::model($client, [
        'method' => 'PUT',
        'route' => array('web.clients.update', $client->id),
        'files' => true
        ]); ?>



    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('status', 'Status',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::label('active','Active',array('id'=>'','class'=>''))); ?>

                    <?php echo e(Form::radio('status', 'active', 'true', ['class' => 'radio-inline'])); ?>

                    <?php echo e(Form::label('deactive','Deactive',array('id'=>'','class'=>''))); ?>

                    <?php echo e(Form::radio('status', 'deactive', 'false', ['class' => 'radio-inline'])); ?>

                    <?php if($errors->has('status')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('status')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>              
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('full_name', 'Full Name:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'name',$client->full_name, ['class' => 'form-control'])); ?>

                    <?php if($errors->has('name')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('name')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('designation', 'Designation:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'designation',$client->designation,['class' => 'form-control'])); ?>

                    <?php if($errors->has('designation')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('designation')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('company', 'Company:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'company',$client->company,['class' => 'form-control'])); ?>

                    <?php if($errors->has('company')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('company')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div>
    <div class="row">
        <div class="col-sm-12">
            <h2>Contact Details</h2>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required"></span><?php echo e(Form::label('department', 'Department:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'department',$client->department,['class' => 'form-control'])); ?>

                    <?php if($errors->has('department')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('department')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div>  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('direct_phone_number', 'Direct Phone Number:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'phone',$client->phone,['class' => 'form-control'])); ?>

                    <?php if($errors->has('phone')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('phone')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div>  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('email', 'Email Address:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'email',$client->users()->first()->email,['class' => 'form-control'])); ?>

                    <?php if($errors->has('email')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div> 
   <div class="row">
    <div class="col-sm-12">
     <h2>Login Details</h2>
    </div>
   </div>
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('email', 'User Email:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'email',$client->users()->first()->email,['class' => 'form-control'])); ?>

                </div>
            </div>
        </div>
   </div> 
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required"></span><?php echo e(Form::label('password', 'Password:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('password', 'password',$client->password,['class' => 'form-control'])); ?>

                    <?php if($errors->has('password')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('password')); ?></strong>
                        </span>
                    <?php endif; ?>
                   
                </div>
            </div>
        </div>
   </div>
 <br>
 <div class="row">
  <div class="col-sm-12">
    <?php echo e(Form::submit('Submit',array('class' => 'btn btn-submit'))); ?>

  </div>
 </div>
   <?php echo Form::close(); ?>

    <!--<?php if($errors->any()): ?>-->
    <!--  <div class="row">-->
    <!--      <div class="col-sm-12">-->
    <!--        <ul class="alert alert-danger" >-->
    <!--          <?php foreach($errors->all() as $errors): ?>-->
    <!--          <li><?php echo e($errors); ?></li>-->
    <!--          <?php endforeach; ?>-->
    <!--          </ul>-->
    <!--      </div>-->
    <!--  </div>-->
    <!-- <?php endif; ?>-->

  
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>