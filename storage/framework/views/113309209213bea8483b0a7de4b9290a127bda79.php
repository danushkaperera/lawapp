 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', 'Edit Matter'); ?>
 <?php echo $__env->make('web.layouts.loading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="content">
<h2>Edit Matter</h2>
<button type="button" class="btn btn-sm btn-warning" id="backbtnmatter"  data-toggle="modal" >Go Back</button>
 <div ng-controller="editmatterCtrl">
     <form  ng-submit="submitdata()"  method="post">
    
      
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span> <?php echo Form::label('name', 'Matter Title:'); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'name', $matter->name,['class' => 'form-control', 'ng-model' => 'data.name' ])); ?>

                    
                        <span class="error-block">
                            <strong><%  data.name_err %></strong>
                        </span>
                        
                       
                </div>
            </div>
        </div>
    </div>
      
          <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   <?php echo Form::label('', 'Type of Matter:'); ?></div>
                <div class="col-sm-9">
                    <!--<input type="radio" ng-model="formdata.type_id" name="type_id" value="1">-->
                   <input type="radio"  name="type_id" ng-model="data.type_id" ng-value="1" ng-checked="<% data.type_id == 1 %>" /><?php echo Form::label('type_id', 'Incorporation of a Company'); ?>

                   <input type="radio"  name="type_id" ng-model="data.type_id" ng-value="2" ng-checked="<% data.type_id == 2 %>"  /><?php echo Form::label('type_id', 'Litigation'); ?>

                   <input type="radio"  name="type_id" ng-model="data.type_id" ng-value="3" ng-checked="<% data.type_id == 3 %>"   /> <?php echo Form::label('type_id', 'IP Registration'); ?>

                  
                    <?php if($errors->has('type_id')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('type_id')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   <?php echo Form::label('lead_lawyer_id', 'Lead Lawyer:'); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::select('lead_lawyer_id', $leadlawyer, $matter->lead_lawyer_id, ['class' => 'form-control' , 'ng-model' => 'data.lead_lawyer_id', ])); ?>

                    <?php if($errors->has('lead_lawyer_id')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('lead_lawyer_id')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>    <?php echo Form::label('represent_company', 'Represented Company:'); ?></div>
                <div class="col-sm-9">
                       <?php echo e(Form::select('represent_company', $companyname, $matter->represent_company,['class' => 'form-control','ng-change'=>'leaveChange()','id'=>'repeatSelect',  'data-ng-model' => 'data.represent_company'])); ?>

                    <?php if($errors->has('represent_company')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('represent_company')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    
    <br>
    <br>
    
     <div class="row">
       <div class="col-sm-12">
        <div class="form-group col-sm-12">
            <div class="col-sm-3 " ><h4>Select Users</h4></div>
            <div class="col-sm-9">
          <select id="asd"class="form-control" ng-model="nameselect" ng-options="row as row.name for row in rowCollection5 " ng-change="lawyerName()"  >                  
          </select>
                    <span class="error-block">
                            <strong><%  data.clients_err %></strong>
                        </span>
          </div>
        </div>
      </div>
</div>
  <div class="row">
     <div class="col-sm-12">
        <div class="form-group col-sm-12">
       <div class="col-sm-3"></div>
         <div class="col-sm-9">
      <table st-table="rowCollection1" class="table table-striped">
            <thead>
            <tr>
                
                <th st-sort="full_name">Selected Client Names</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="row1 in data.clients">
                <td  ng-model="data.clients" ><%row1.name%></td>
                <td>
                <button type="button" ng-click="removeItem1(row1)" class="btn btn-sm btn-danger">close</button>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5" class="text-center">
                    <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="7"></div>
                </td>
            </tr>
            </tfoot>
        </table>
        </div> 
   </div> 
 </div>
</div> 
      
        <br/>
      
  <div class="row">
    <div class="col-sm-12">
        <div class="form-group col-sm-12">
            <div class="col-sm-3 " ><h4>Select Lawyers</h4></div>
            <div class="col-sm-9">
          <select id="asd" class="form-control" ng-model="selectlawyer" ng-options="row as row.name for row in rowCollection4 " ng-change="usersSelect()"  >               
          </select>

                       <span class="error-block">
                            <strong><%  data.lawyers_err %></strong>
                        </span>
          </div>
        </div>
      </div>
      </div>

      <div class="row">
       <div class="col-sm-12">
        <div class="form-group col-sm-12">
       <div class="col-sm-3"></div>
    <div class="col-sm-9">
        <table st-table="rowCollection2" class="table table-striped">
            <thead>
            <tr>
               
                <th st-sort="full_name">Selected Lawyer Name</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="row in data.lawyers">
                <td ng-model="data.lawyers"><%row.name%></td>
                <td>
                <button type="button" ng-click="removelawyer(row)" class="btn btn-sm btn-danger">close</button>
                </td>
            </tr>
            </tbody>
            <tfoot>
            
            </tfoot>
        </table>
    </div>
      </div> 
 </div>
    </div>
 <br/>
        
        
        
     <button type="button" class="btn btn-sm btn-danger"  ng-click="close()" data-toggle="modal" >Close Matter</button>
    
     <?php echo e(Form::submit('SAVE',array('class' => 'btn navbar-right btn-default'))); ?>

      
    </form>
    </div>
 </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
    
    <?php echo Html::script('js/edit-matter.js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>