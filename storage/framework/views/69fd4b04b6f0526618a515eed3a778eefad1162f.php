 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', ' matter management'); ?>

<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="content">
   
 
    
<h2> Matter Updates</h2>
  <a href="<?php echo e(URL::previous()); ?>" class="btn btn-sm btn-warning" >Go Back</a>
 <?php echo Form::open(array('url' => 'web/matter-management/update')); ?>

 
 <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span> <?php echo Form::label('name', 'Matter Title:'); ?></div>
                <div class="col-sm-9">
                     <?php echo e(Form::input('text', 'name', $matter->name,['class' => 'form-control','readonly'=>'readonly'])); ?>

                    <?php if($errors->has('name')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('name')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
 <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   <?php echo Form::label('', 'Type of Matter:'); ?></div>
                <div class="col-sm-9">
                    <!--<input type="radio" ng-model="formdata.type_id" name="type_id" value="1">-->
                   <input type="radio"  name="type_id" value="5" id="myradio"<?php echo e(($matter->type_id)=="1" ? 'checked='.'"'.'checked'.'"' : ''); ?> /><?php echo Form::label('type_id', 'Incorporation of a Company'); ?>

                   <input type="radio"  name="type_id" value="5" id="myradio2" <?php echo e(($matter->type_id)=="2" ? 'checked='.'"'.'checked'.'"' : ''); ?> /><?php echo Form::label('type_id', 'Litigation'); ?>

                   <input type="radio"  name="type_id" value="5" id="myradio3" <?php echo e(($matter->type_id)=="3" ? 'checked='.'"'.'checked'.'"' : ''); ?> /> <?php echo Form::label('type_id', 'IP Registration'); ?>

                  
                    <?php if($errors->has('type_id')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('type_id')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  
 
  <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
               
                <div class="col-sm-6">
          
               <ol>
<?php if(count($legal_matter_note)): ?>
  

                    <?php foreach($legal_matter_note as $legal_matter_notes): ?>

 
                    
                        <li>  <?php echo e(Form::hidden('id', $legal_matter_notes->id)); ?>

                        <?php echo e(Form::text('text',$legal_matter_notes->updated_at->format('Y-m-d'), null ,['class' => 'form-control ','id' => 'datepicker'])); ?> 
                               <button type="button" class="btn btn-sm btn-primary"  id="myBtn" data-toggle="modal" data-target="#<?php echo e($legal_matter_notes->id); ?>">VIEW</button>
                        
                                         
                                            <!-- Modal -->
                            <div class="modal fade" id="<?php echo e($legal_matter_notes->id); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"> <?php echo Form::label('note', 'View Note:'); ?></h4>
                                  </div>
                                  <div class="modal-body">
                                          <?php echo e(Form::textarea('note', $legal_matter_notes->note,['class' => 'form-control'])); ?>

                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                       </li> 
                        <?php endforeach; ?>
<?php else: ?>
   There is no any updates!
<?php endif; ?>
                    
               </ol>
          </div>
        </div>
    </div>
   
   

   
<?php echo Form::close(); ?>

</div>
<script>
document.getElementById("myradio").disabled = true;
document.getElementById("myradio2").disabled = true;
document.getElementById("myradio3").disabled = true;
</script>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
   <?php echo Html::script('js/update-view.js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>