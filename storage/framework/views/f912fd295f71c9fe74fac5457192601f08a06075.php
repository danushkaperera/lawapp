 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', 'Create Matter'); ?>
  <?php echo $__env->make('web.layouts.loading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>
 
<?php $__env->startSection('content'); ?>
<div class="content">
<h2>Create a New Matter</h2>

 
  <button type="button" class="btn btn-sm btn-warning" id="backbtnmatter"  data-toggle="modal" >Go Back</button>
 <div ng-controller="matterCtrl">
    
  <!--<button type="button" class="btn btn-default" ng-click="open()">Large modal</button>-->
     <form ng-submit="submitdata()"    method="post">
    
       
        <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   <?php echo Form::label('name', 'Matter Title:'); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'name',null,['class' => 'form-control' ,'ng-model'=>'formdata.name'])); ?>

                   
                        <span class="error-block">
                            <strong><% formdata.err %></strong>
                        </span>
                  
                </div>
            </div>
        </div>
    </div>
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
               <div class="col-sm-3"><span class="error_required">*</span>   <?php echo Form::label('', 'Type of Matter:'); ?></div>
                <div class="col-sm-9" ng-init="matter_type=1">
                
                    <?php echo e(Form::radio('type_id', '1', true,['ng-checked'=>'matter_type', 'ng-model'=>'matter_type', 'ng-change'=>'mater_select(matter_type)'])); ?><?php echo Form::label('type_id', 'Incorporation of a Company'); ?>&nbsp&nbsp&nbsp
                   <?php echo e(Form::radio('type_id', '2', false, ['ng-model'=>'matter_type', 'ng-change'=>'mater_select(matter_type)'])); ?><?php echo Form::label('type_id', 'Litigation'); ?>&nbsp&nbsp&nbsp
                    <?php echo e(Form::radio('type_id', '3', false, ['ng-model'=>'matter_type', 'ng-change'=>'mater_select(matter_type)'])); ?> <?php echo Form::label('type_id', 'IP Registration'); ?>

                        </br>
                        <span class="error-block">
                            <strong><% matter_type.err %></strong>
                        </span

                </div>
            </div>
        </div>
    </div> </div>
  
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   <?php echo Form::label('lead_lawyer_id', 'Lead Lawyer:'); ?></div>
                <div class="col-sm-9">
                    
                    <select class="form-control" ng-model="lawyername.name" ng-options="lawyerRow as lawyerRow.name for lawyerRow in devional_lawyers" >
          </select>
                    <!--<?php echo e(Form::select('lead_lawyer_id', $leadlawyer, null, ['class' => 'form-control' , 'ng-model'=>'lawyername.name' ])); ?>-->
                     <span class="error-block">
                            <strong><% lawyername.err %></strong>
                        </span>
                </div>
            </div>
        </div>
    </div>

   <h4>Client Users</h4>
<hr>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>    <?php echo Form::label('represent_company', 'Represented Company:'); ?></div>
                <div class="col-sm-9">
    <?php echo e(Form::select('represent_company', $companyname, null,['class' => 'form-control','ng-change'=>'leaveChange()', 'id'=>'repeatSelect', 'ng-model'=>'represent_company.name'])); ?>

                    <span class="error-block">
                            <strong><% represent_company.err %></strong>
                        </span>
                </div>
            </div>
        </div>
    </div>



    <div class="form-group">
        <div class="col-sm-12">
            <div class="col-sm-3 " ><span class="error_required">*</span> <label>Select Users</label></div>
            <div class="col-sm-9">
          <select id="asd"class="form-control" ng-model="nameselect.name" ng-options="row as row.name for row in rowCollection" ng-change="usersSelect()"  >
          </select>
                     <span class="error-block">
                            <strong><% nameselect.err %></strong>
                        </span>

          </div>
        </div>
      </div>
     
      
      
    <div class="row">
        <div class="col-sm-5">
            <table st-table="rowCollection1" class="table table-striped">
            <thead>
            <tr>
                <th st-sort="name"><label>Full Name</label></th>
            </tr>
            </thead>
            <tbody>
                <tr ng-repeat="row1 in rowCollection1"  >
                    <td ng-model="nameselect.name" name="companyuser" ng-cloak><%row1.name %></td>
                    <td>
                        <button type="button" ng-click="removeItem1(row1)" class="btn btn-sm btn-danger">close
    	           </td>
            </tr>
            </tbody>
            <tfoot>
           
            </tfoot>
        </table>
       				
  
  </div>
  </div>
  
  
      
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span> <?php echo Form::label('full_name', 'Select Lawyers:'); ?></div>
                <div class="col-sm-9">
          <select id="asd"class="form-control" ng-model="lawyer_name"  ng-options="row as row.name for row in rowCollection4" ng-change="lawyerSelect()"  ></select>
                      <span class="error-block">
                            <strong><% lawyer_name.err %></strong>
                        </span>
                </div>
            </div>
        </div>
    </div>
  
  
    <div class="row">
        <div class="col-sm-5">
            <table st-table="rowCollection2" class="table table-striped">
            <thead>
            <tr>
                <th st-sort="full_name"><label>Lawyer Name</label></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="rows in rowCollection2">
                <td ng-model="lawyer" name="lawyername" ng-cloak><% rows.name %></td>
                <td>
                    <button type="button" ng-click="removeItem(rows)" class="btn btn-sm btn-danger">close
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5" class="text-center">
                    <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="7"></div>
                </td>
            </tr>
            </tfoot>
        </table>
  
  </div>
  </div>
  
  
      <button type="submit" class="btn btn-default" >CREATE</button>
    </form>
     <script type="text/ng-template" id="myModalContent.html">
        <div class="modal-header">
            <h3 class="modal-title">Confirm Message</h3>
        </div>
        <div class="modal-body">
          <h4>Are you sure you want to change selected company ?</h4>
           
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
        </div>
    </script>
    
    
    </div>
 </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
    <?php echo Html::script('js/create-matterlist.js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>