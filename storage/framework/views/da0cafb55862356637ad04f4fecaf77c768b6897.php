 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', 'Edit Legal Alert'); ?>

<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="content">
<h2>Edit Legal Alert</h2>
  <button type="button" class="btn btn-sm btn-warning" id="backbtnlegalalert"  data-toggle="modal" >Go Back</button>
 
   <?php echo Form::model($alert, [
    'method' => 'PUT',
    'route' => array('web.legal-alert.update', $alert->id),
    'files' => true  
    ]); ?>  
    
  <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>     <?php echo e(Form::label('title', 'Title',  ['class' => 'control-label'])); ?></div>
                <div class="col-sm-9">
                      <?php echo e(Form::input('title', 'title',$alert->title,['class' => 'form-control'])); ?>

                    <?php if($errors->has('title')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('title')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  
 <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   <?php echo Form::label('type_id', 'Legal Alert Type:'); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::select('type_id',  $types,$alert->type_id, ['class' => 'form-control'  ])); ?>

                    <?php if($errors->has('type_id')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('type_id')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>     <?php echo e(Form::label('message', 'Notes',  ['class' => 'control-label'])); ?></div>
                <div class="col-sm-9">
                     <?php echo e(Form::textarea('message', $alert->message,['class' => 'form-control'])); ?>

                    <?php if($errors->has('message')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('message')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  
 
 <br>
  <button type="submit" class="btn btn-default">Submit</button>
<?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>