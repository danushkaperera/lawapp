 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', 'Create Quick Reference Guide'); ?>

<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="content">
<h2>Edit Quick Reference Guide</h2>
<button type="button" class="btn btn-sm btn-warning" id="backbtnqrg"  data-toggle="modal" >Go Back</button>

   <?php echo Form::model($qrg, [
    'method' => 'PUT',
    'route' => array('web.quick-reference-guide.update', $qrg->id),
    'files' => true  
    ]); ?>

    
     
  <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>     <?php echo e(Form::label('title', 'Title',  ['class' => 'control-label'])); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('title', 'title',$qrg->title,['class' => 'form-control'])); ?>

                    <?php if($errors->has('title')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('title')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  
   <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>  <?php echo e(Form::label('body', 'Notes:',  ['class' => 'control-label'])); ?></div>
                <div class="col-sm-9">
                 <?php echo Form::textarea('body', $qrg->body, ['class' => 'form-control']); ?>

                    <?php if($errors->has('body')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('body')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  
         
     <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>    <?php echo e(Form::label('created_at', ' Publish Date ')); ?></div>
                <div class="col-sm-9">
                  <?php echo e(Form::input('text', 'created_at', $qrg->created_at->format('Y-m-d'),['id' => 'datepicker','readonly'=>'readonly','class' => 'form-control datepicker','data-date-format'=>'y-m-d'])); ?>

            </div>
        </div>
    </div>
  
 <br>
  <button type="submit" class="btn btn-primary">Submit</button>
<?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>