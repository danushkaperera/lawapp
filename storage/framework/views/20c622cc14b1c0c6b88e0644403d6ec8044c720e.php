 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', 'Create User'); ?>

<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <h1>Add New User</h1>
            <button type="button" class="btn btn-sm btn-warning" id="backbtnlawyer"  data-toggle="modal" >Go Back</button>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2>Personal Details</h2>
        </div>
        
    </div>
    <?php echo Form::open(array('url' => 'web/lawyers','files' => true )); ?>


 <br>
 
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('full_name', 'Full Name:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'full_name', $lawyrs->full_name,['class' => 'form-control'])); ?>

                    <?php if($errors->has('full_name')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('full_name')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required"></span><?php echo e(Form::label('designation', 'Is A Key Contact:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                  <?php echo e(Form::select('head', ['0' => 'No', '1' => 'Yes' ])); ?>

                    <?php if($errors->has('head')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('head')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('designation', 'Designation:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'designation',$lawyrs->designation,['class' => 'form-control'])); ?>

                    <?php if($errors->has('designation')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('designation')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required"></span><?php echo e(Form::label('division_id', 'Division:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">  
   <?php echo e(Form::select('division_id', $divisions, null, ['class' => 'form-control']
)); ?>

     <?php if($errors->has('division_id')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('division_id')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
   
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
            <div class="col-sm-3"><span class="error_required"></span><?php echo e(Form::label('image', 'Upload Image:',array('class' => 'control-label'))); ?></div>
            <div class="col-sm-9"><?php echo e(Form::file('image',$lawyrs->image, ['class' => 'field'])); ?>

                   
                    <?php if($errors->has('image')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('image')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div>

   
    <div class="row">
        <div class="col-sm-12">
            <h2>Contact Details</h2>
        </div>
    </div>
  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('direct_phone', 'Direct Phone Number:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'direct_phone',null,['class' => 'form-control' ,'placeholder'=>'(071) 333 3333'])); ?>

                    <?php if($errors->has('direct_phone')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('direct_phone')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div>  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('email', 'Email Address:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'email',null,['class' => 'form-control'])); ?>

                    <?php if($errors->has('email')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div> 
   <div class="row">
    <div class="col-sm-12">
     <h2>Login Details</h2>
    </div>
   </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('email', 'Login Email:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'email',null,['class' => 'form-control'])); ?>

                    <?php if($errors->has('email')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div> 
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('password', 'Password:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('password', 'password',null,['class' => 'form-control'])); ?>

                    <?php if($errors->has('password')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('password')); ?></strong>
                        </span>
                    <?php endif; ?>
                    <?php echo e(Form::input('hidden', 'status','active',['class' => 'form-control'])); ?>

                </div>
            </div>
        </div>
   </div>
 <br>
 </div>
 
  <?php echo e(Form::submit('Submit',array('class' => 'btn btn-submit'))); ?>

    </br> </br> </br>
    
   <?php echo Form::close(); ?>

   
  
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>