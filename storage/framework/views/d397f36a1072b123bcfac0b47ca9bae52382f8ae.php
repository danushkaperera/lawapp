 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', 'Create User'); ?>

<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="content">
    <div class="row">
        <div class="col-sm-12">
            <h1>Add New User</h1>
            <a href="<?php echo e(URL::previous()); ?>" class="btn btn-sm btn-warning" >Go Back</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2>Personal Details</h2>
        </div>
    </div>
    <?php echo Form::open(array('url' => 'web/clients','files' => true )); ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('full_name', 'Full Name:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'name',$client->name,['class' => 'form-control'])); ?>

                    <?php if($errors->has('name')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('name')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('designation', 'Designation:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'designation',$client->designation,['class' => 'form-control'])); ?>

                    <?php if($errors->has('designation')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('designation')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><?php echo e(Form::label('division', 'Division:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'division',$client->division,['class' => 'form-control'])); ?>

                    <?php if($errors->has('division')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('division')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('company', 'Company:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'company',$client->company,['class' => 'form-control'])); ?>

                    <?php if($errors->has('company')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('company')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div>
   
    <div class="row">
        <div class="col-sm-12">
            <h2>Contact Details</h2>
        </div>
    </div>
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required"></span><?php echo e(Form::label('department', 'Department:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'department',null,['class' => 'form-control'])); ?>

                    <?php if($errors->has('department')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('department')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div>  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('direct_phone_number', 'Direct Phone Number:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'phone',$client->phone,['class' => 'form-control'])); ?>

                    <?php if($errors->has('phone')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('phone')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div>  
   <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('email', 'Email Address:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'email',$client->email,['class' => 'form-control'])); ?>

                    <?php if($errors->has('email')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div> 
   <div class="row">
    <div class="col-sm-12">
     <h2>Login Details</h2>
    </div>
   </div>
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('email', 'User Name:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('text', 'email',$client->email,['class' => 'form-control'])); ?>

                </div>
            </div>
        </div>
   </div> 
  <div class="row">
       <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span><?php echo e(Form::label('password', 'Password:',array('class' => 'control-label'))); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::input('password', 'password',null,['class' => 'form-control'])); ?>

                    <?php if($errors->has('password')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('password')); ?></strong>
                        </span>
                    <?php endif; ?>
                    <?php echo e(Form::input('hidden', 'id',$client->id,['class' => 'form-control'])); ?>

                      <?php echo e(Form::input('hidden', 'status','active',['class' => 'form-control'])); ?>

                     
                </div>
            </div>
        </div>
   </div>
 <br>
 <div class="row">
  <div class="col-sm-12">
    <?php echo e(Form::submit('Submit',array('class' => 'btn btn-submit'))); ?>

  </div>
 </div>
   <?php echo Form::close(); ?>


  
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>