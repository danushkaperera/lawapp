<?php $__env->startSection('title', 'Dashboard'); ?>

<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>


  <nav class="navbar navbar-default">
        <div class="container">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo e(url('web/dashboard')); ?>">Home</a></li>
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                        <a href="<?php echo e(url('web/logout')); ?>"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                           
                        </li>
                </ul>
            </div>
        </div>
    </nav>
<?php $__env->startSection('content'); ?>
    
    
   
    <div class="row">
                            <h2> Dashboard </h2>
        <div class="col-sm-12">
            <div class="col-sm-4">
                <a href="<?php echo e(url('web/legal-alert')); ?>" class="btn dashboard btn-default">Legal Alerts Manager</a>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo e(url('web/quick-reference-guide')); ?>" class="btn dashboard btn-default">Quick Reference Guide Manager</a>
            </div>
            <div class="col-sm-4">
                 <a href="<?php echo e(url('web/lawyers')); ?>" class="btn dashboard  btn-default">Lawyer Manager</a>
            </div>
         </div>  
          
       
         <div class="col-sm-12">
            <div class="col-sm-4">
                <a href="<?php echo e(url('web/client/requests')); ?>" class="btn dashboard btn-default">Client Request</a>
            </div>
           
             <div class="col-sm-4">
                <a href="<?php echo e(url('web/matter-management')); ?>" class="btn dashboard btn-default">Matter Manager</a>
            </div>
             <div class="col-sm-4">
                <a href="<?php echo e(url('web/clients')); ?>" class="btn dashboard btn-default">Client Manager</a>
            </div>
         </div>
          
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
    <?php echo Html::script('js/login.js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>