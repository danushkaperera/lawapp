 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', 'Quick reference guid Manager'); ?>
 <?php echo $__env->make('web.layouts.loading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>
<script data-require="jquery@*" data-semver="2.0.3" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
<script data-require="bootstrap@*" data-semver="3.1.1" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

<?php $__env->startSection('content'); ?>


 <div class="row">
        <div class="col-sm-12">            
                <div class="col-sm-9">
                  <h2>Quick Reference Guide Manager </h2> 
                  <button type="button" class="btn btn-sm btn-warning" id="btnhome"  data-toggle="modal" >Go Back</button>
                </div>
                <div class="col-sm-3">
                  </br>
                  <a href="<?php echo e(url('web/quick-reference-guide/create')); ?>" class="btn btn-primary">Create New</a>
                </div>
         </div>
</div>




    <?php echo Form::open(array('url' => 'foo/bar')); ?>

    <div ng-controller="qrguidCtrl">
        <table st-table="rowCollection" class="table table-striped">
            <thead>
            <tr>
                <th st-sort="id">Id</th>
                <th st-sort="title">Title</th>
                <th st-sort="updated_at">Last Updated</th>
                 <th st-sort="#"></th>
            </tr>
            <tr><th>                   
                </th>
                <th class="col-sm-6">
                    <input st-search="'title'" ng-model="search" placeholder="search by title" class="input-sm form-control" type="search"/>
                </th>
               
                
            </tr>
            </thead>
            <tbody>
            <tr dir-paginate="row in rowCollection| filter:search|itemsPerPage:15">
                 <td><%row.id %></td>
                <td><%row.title | uppercase%></td>
                <td><%row.updated_at.slice(0,10)%></td>
                
                <td>
                <a href="<?php echo e(url('web/quick-reference-guide')); ?>/<%row.id%>/edit" class="btn btn-xs btn-primary">Edit</a>
                </td>
                <td>
                <a data-href="<?php echo e(url('web/quick-reference-guide')); ?>/<%row.id%>"  data-toggle="modal" data-target="#confirm-delete"   class="btn btn-xs btn-delete">Delete</a>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5" class="text-center">
                    <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="7"></div>
                </td>
            </tr>
            </tfoot>
        </table>
                 <div class="text-center">
                          	
            		 <dir-pagination-controls
            		 max-sizes="5"
            		 on-page-change="pageChangeHandler(newPageNumber)"
            		 direction-links="true"
            		 boundary-links="true" >
            		</dir-pagination-controls>
            		      
            </div>


    
    </div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">         
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>            
                <div class="modal-body">
                    <p>Are you sure you want to delete this record? </p>
                    
                    <p class="debug-url"></p>
                </div>                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
    
    
    
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
          //  $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        });
    </script>


    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
    <?php echo Html::script('js/quick-reference-guid.js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>