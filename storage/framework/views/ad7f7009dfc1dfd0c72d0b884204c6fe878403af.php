<?php $__env->startSection('title', 'Dashboard'); ?>

  <nav class="navbar navbar-default">
        <div class="container">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo e(url('web/dashboard')); ?>">Home</a></li>
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                      

                           <a href="<?php echo e(url('web/logout')); ?>"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                            
                            
                        </li>
                </ul>
            </div>
        </div>
    </nav>

<?php $__env->startSection('footer'); ?>
    @parent
    <?php echo Html::script('js/login.js'); ?>

<?php $__env->stopSection(); ?>