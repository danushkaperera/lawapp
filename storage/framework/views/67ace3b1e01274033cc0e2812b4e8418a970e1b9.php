 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', 'Account Request '); ?>
 <?php echo $__env->make('web.layouts.loading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>
<style>

</style>
<?php $__env->startSection('content'); ?>
<h2>Client Request </h2>  
<button type="button" class="btn btn-sm btn-warning" id="btnhome"  data-toggle="modal" >Go Back</button>
    <?php echo Form::open(array('url' => 'foo/bar')); ?>

    <div ng-controller="accRequestCtrl">
        <table st-table="rowCollection" class="table table-striped">
            <thead>
            <tr>
                <th st-sort="name">Name</th>
                <th st-sort="designation">Designation</th>
                <th st-sort="company">Company</th>
                <th st-sort="email">Email</th>
                <th st-sort="phone">Phone</th>
                <th st-sort="status"></th>
                <th st-sort="id">Status</th>
            </tr>
            <tr> 
                <th>
            <input st-search="'name'" ng-model="search "search placeholder="search by name" class="input-sm form-control" type="search"/>
                </th>
                
            </tr>
            </thead>
            <tbody>
            <tr class="<%row.status%>" dir-paginate="row in rowCollection|itemsPerPage:15|filter:search">
                <td><%row.name | uppercase%></td>
                <td><%row.designation%></td>
                <td><%row.company%></td>
                <td><a ng-href="mailto:<%row.email%>">email</a></td>
                <td><%row.phone%></td>
                <td ><%row.status%></td>
                <td><a href="<?php echo e(url('web/client/create/')); ?>/<%row.id%>"  class="btn btn-submit">Approve</a>
                <a href="<?php echo e(url('web/client/requests-declined')); ?>/<%row.id%>" ng-click="declineUser(row)" class="btn btn-submit">Decline</a>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5" class="text-center">
                    <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="20"></div>
                </td>
            </tr>
            </tfoot>
        </table>
             <div class="text-center">
                          	
            		 <dir-pagination-controls
            		 max-sizes="5"
            		 on-page-change="pageChangeHandler(newPageNumber)"
            		 direction-links="true"
            		 boundary-links="true" >
            		</dir-pagination-controls>
            		      
            </div>

    </div>
    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
    <?php echo Html::script('js/account-requests.js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>