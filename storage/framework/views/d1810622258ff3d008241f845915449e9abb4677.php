<!DOCTYPE html>
<html>
    <body>
        <div style="padding-left:15px">
        <p>Dear <?php echo $data['name']; ?>,</p>
    
        <p>Your request for an account on the FJG Direct mobile application has been received by one of our administrators. They will verify your request and an account will be created and notified to you shortly. </p>
      
        <p>Your patience and understanding in this matter is appreciated. </p>
     
        <p>Thank you <br/>
        FJ&G De Saram </p>
        <br>
        </div>
    </body>
</html>