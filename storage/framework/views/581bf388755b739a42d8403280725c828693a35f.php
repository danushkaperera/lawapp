 <?php echo $__env->make('web.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('title', 'Create Legal Alert'); ?>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<?php $__env->startSection('sidebar'); ?>
    @parent
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="content">
<h2>Create Legal Alert</h2>
   <button type="button" class="btn btn-sm btn-warning" id="backbtnlegalalert"  data-toggle="modal" >Go Back</button>
 <?php echo e(Form::open(array('url' => 'web/legal-alert'))); ?>

      <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>  <?php echo e(Form::label('title', 'Title')); ?></div>
                <div class="col-sm-9">
                       <?php echo e(Form::input('title', 'title',$alert->title,['class' => 'form-control'])); ?>

                    <?php if($errors->has('title')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('title')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  
 <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>   <?php echo Form::label('type_id', 'Legal Alert Type:'); ?></div>
                <div class="col-sm-9">
                    <?php echo e(Form::select('type_id',  $types,null, ['class' => 'form-control' , 'ng-model' => 'data.type', ])); ?>

                    <?php if($errors->has('type_id')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('type_id')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <div class="col-sm-3"><span class="error_required">*</span>  <?php echo e(Form::label('message', 'Note')); ?></div>
                <div class="col-sm-9">
                        <?php echo e(Form::textarea('message', $alert->message,['class' => 'form-control'])); ?>

                    <?php if($errors->has('message')): ?>
                        <span class="error-block">
                            <strong><?php echo e($errors->first('message')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
  
  
 <br>
  <button type="submit" class="btn btn-default">Submit</button>
<?php echo Form::close(); ?>

</div>
 <script>

   $('.datepicker').datepicker({
    dateFormat: 'yy-mm-dd',
   
});
  </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    @parent
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>